# Sorrow & Bliss larp support web

## Design

### Basics

- Frontend is React application using Next.JS, MaterialUI 5, React Final Form, ...
    - Next.JS is used more as a router; SSR is not very useful in this kind of app
- Backend is Node.JS application (in the same repository as the frontend)
- GraphQL is used as means of BE<=>FE communication
    - See `schema.graphql` for schema description
    - Only file upload / download are standalone REST endpoints
    - `graphql-request` and `react-query` are used on frontend instead of Apollo due to easier cache management
-  Sqlite3 is used as database
    - It does not require DB server, so both prod and dev environment are simplified  
    - Can run in memory-only mode which si very useful during automated tests
    - Allows for advanced SQL queries like `AND NOT EXISTS` which is not available / complicated in document databases
    - Tradeoff is more complicated containerization of app should we need it (we currently don't)
    - DB is stored in `data/db` file
- Uploaded files are stored in configured Google Drive folder using Google Drive API
- Application is internationalized using `next-i18next`, but for 100% internationalization we would probably also need to split character names (in the DB) per language.
- GraphQL mutations and their parameters are logged to server output for debugging purposes. Results are not logged since that would be more complicated.  

### Security & Roles

- Calls to API are authorized using JWT token in the `Authorization` HTTP header
- Since we cannot set the `Authorization` header for file display / download, short-lived JWT token is prepared for each file by BE which holds info about downloaded file and sent as a parameter. 
- Client stores login tokens in browser local storage
    - Token storage is independent for player and admin, so user can be logged in both as a player and an admin at the same time
    - Token is kept in storage even when it is expired, so we can redirect user to "his" section 
    - See also *Sections and redirects* below
- GraphQL schema has three sections
    - Operations under the `public` section can be accessed without authorization
    - Operations under the `player` section require player token
    - Operations under the `admin` section require admin token
  
#### Admins
- Administrators are identified using Google account via Google Auth API integration
- Application contains list of allowed admin e-mails. On first login with each e-mail the unique account id is stored and then checked on subsequent logins so that e-mail reuse does not pose security problem.
- Created token has long expiration for convenience, however it is checked on every backend call that associated email still has admin access.  

#### Players
- Players log in using passwords that are generated for each player character when the run is created
- Those passwords cannot be chaged (except by direct DB update) and are stored in clear text in the database
- That is not very secure, however
    - The passwords are good for just one run (~3 months)
    - Usecase does not require high security; the protection is here mainly to fend off random people who whould stumble on the web
    - It is very convenient
        - No password change / password recovery procedures needed
        - Admin can login as a player when the need arises without impersonification infrastructure
- Player token has long expiration, however
    - On every page change it is checked whether the run is still active
    - When letter is created / updated or file uploaded, it is checked whether the run is still active
        - So even when user circumvents the check on page render, he can no longer create data in the run
    - When run is deleted, operations with the token will fail or return no data

### Sections and redirects

- Web has sections for players (dashboard and outgoing letters) and admins (dashboard, letters, npcs, runs and admin accounts)
- Player section is under the `/player` URLs
- Admin section is under the `/admin` URLs
- When user accesses root of the web (the `/` URL), "smart" redirect is executed
    - When there is admin token stored (even expired), user is redirected to admin section
    - Otherwise user is redirected to player section
    - When accessing respective section without valid token, section login is displayed
    - User can be logged in simultaneously in both roles, but generally he will be logged in only in one of them, so this will automagically redirect him to "his" section 
    
## Configuration

### Configuration file
Stored in `.env.prod` file for production, `.env.dev` for development (distinguished by NODE_ENV)

Values:

| Value | Meaning |
| ----- | ------- |
| DATABASE_FILE | Location of sqlite3 database file |
| GOOGLE_BASE_FOLDER_ID | ID of base folder on Google Drive where to create run directories. The ID is visible in the browser as last component of the URL. Full access to this folder must be granted to service account specified in the `sorrow-and-bliss-drive-key.json` configuration file. | 
| JWT_SECRET | Secret to encode JWT authorization tokens used for admin / player login |
| JWT_EXPIRATION | Expiration of JWT authorization tokens used for admin / player login |
| DOWNLOAD_JWT_SECRET | Secret to encode JWT tokens used for file download |
| DOWNLOAD_JWT_EXPIRATION | Expiration of JWT tokens used for file download |
| LETTER_AGE_PENDING_WARN | Days after which letter shows with warning in unanswered letters |
| LETTER_AGE_PENDING_ERROR | Days after which letter shows with error in unanswered letters |
| LETTER_AGE_LONG_UNCONFIRMED | Days after which is unconfirmed letter (in delivery) shown in admin dashboard "long unconfirmed" list |
| LETTER_AGE_LONG_UNANSWERED | Days after which is unanswered letter shown in admin dashboard "long unanswered" list |

### Drive client configuration

Stored in the `sorrow-and-bliss-drive-key.json` file. Obtained from Google Cloud web console when creating the service account.

## Compiling and running (yarn targets)

| Command | What it does |
| ------- | ------------ |
| yarn dev | Run Next.JS in development mode |
| yarn build | Build production version of application |
| yarn start | Run production version of application (`yarn build` must be run prior to this) |
| yarn test | Run automatic tests (BE services are covered quite well, other parts are mostly uncovered) |
| yarn gen:schema | Regenerate Typescript definitions (`typescript-operations.ts`) from GraphQL schema and queries / mutations. Used during development when some of that changes.  |
| yarn verify | Check project - compile, lint and run tests |

## DB tables description

### AdminAccount

Stores Google accounts that have admin access

| Field | Type | Meaning |
| ----- | ---- | ------- |
| id | INT | Primary key |
| email | TEXT | Email the account is bound to |
| sub | TEXT | Google *sub* identifier that uniquely identifies the account. Stored after the first login. |

### Character

Stores info about characters, both PC and NPC

| Field | Type | Meaning |
| ----- | ---- | ------- |
| id | INT | Primary key |
| name | TEXT | Character name |
| isNPC | INT | 0 = this is PC, 1 = this is NPC |
| ordinal | INT | Used to order initially created NPCs |

### GameRun

Information about larp runs

| Field | Type | Meaning |
| ----- | ---- | ------- |
| id | INT | Primary key |
| name | TEXT | Run name |
| isActive | INT | Whether the run is currently active (0 / 1) |
| created | INT | Millisecond TS when the run was created (for ordering) | 
| passwords | TEXT | JSON of characters and their passwords for this run |
| directoryId | TEXT | Google Driver ID of folder where files for this run will be stored | 

### Letter

Information about letters

| Field | Type | Meaning |
| ----- | ---- | ------- |
| id | INT | Primary key |
| runId | INT | Run this letter belongs to |
| creatorId | INT  | ID of letter creator character |
| senderId | INT  | ID of letter sender. Same as creatorId for regular letters, NULL for anonymous letters. |
| recipientId | INT  | ID of letter recipient |
| serialNumber | INT  | Letter serial number |
| sentDate | TEXT  | Letter sent date in YYYY-MM-DD format |
| receivedDate | TEXT  | Letter received date in YYYY-MM-DD format (after reception is confirmed) |
| orgComment | TEXT | Organizer / admin comment for the letter |
| fileExtensions | TEXT | List of extensions of files stored with this letter separated by dots ('.') |
| isEndOfLine | INT | Recipient will not answer to this letter (0/1) |
| sentDateTime | INT | UNIXTIME for sentDate midnight |
| receivedDateTime | INT | UNIXTIME for receivedDate midnight |
