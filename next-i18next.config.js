const path = require('path')

module.exports = {
    i18n: {
        defaultLocale: 'cs',
        locales: ['cs', 'en'],
    },
    react: { useSuspense: false },
    localePath: path.resolve('./public/locales'),
}
