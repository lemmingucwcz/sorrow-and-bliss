import { useEffect } from 'react'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { CredentialsService } from '../src/client/services/CredentialsService'
import { loadServerSideTranslations } from '../src/utils/ssrLocale'
import AppLoadingPaper from '../src/client/components/common/AppLoadingPaper'

interface Props {}
interface InitialProps {}

const IndexPage: NextPage<Props, InitialProps> = () => {
    const router = useRouter()

    useEffect(() => {
        // Determine section to go to
        const adminCredentials = CredentialsService.getAdminCredentials()
        if (adminCredentials) {
            // Go to admin section
            router.push({ pathname: '/admin' })
            return
        }
        // Go to player section
        router.push({ pathname: '/player' })
    }, [router])

    return <AppLoadingPaper />
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common'])

export default IndexPage
