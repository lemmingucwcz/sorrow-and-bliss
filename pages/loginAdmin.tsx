import React from 'react'
import { NextPage } from 'next'
import { useTranslation } from 'next-i18next'
import { Box } from '@mui/material'
import TopBar from '../src/client/components/common/TopBar/TopBar'
import { loadServerSideTranslations } from '../src/utils/ssrLocale'
import AdminLoginContent from '../src/client/components/login/AdminLoginContent/AdminLoginContent'

interface Props {}
interface InitialProps {}

const LoginAdminPage: NextPage<Props, InitialProps> = () => {
    const { t } = useTranslation('admin')

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <TopBar>{t('LoginPage.title')}</TopBar>
                <AdminLoginContent />
            </Box>
        </>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'admin'])

export default LoginAdminPage
