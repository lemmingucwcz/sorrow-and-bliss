import { NextPage } from 'next'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { Box } from '@mui/material'
import PlayerLoginContent from 'src/client/components/login/PlayerLoginContent/PlayerLoginContent'
import TopBar from 'src/client/components/common/TopBar/TopBar'
import { loadServerSideTranslations } from '../src/utils/ssrLocale'

interface Props {}
interface InitialProps {}

const LoginPlayerPage: NextPage<Props, InitialProps> = () => {
    const { t } = useTranslation('player')

    return (
        <>
            <TopBar>{t('LoginPage.title')}</TopBar>
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '20px' }}>
                <PlayerLoginContent />
            </Box>
        </>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'player'])

export default LoginPlayerPage
