import * as React from 'react'
import App, { AppInitialProps, AppContext } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import { QueryClient, QueryClientProvider } from 'react-query'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { red } from '@mui/material/colors'
import { Box, CssBaseline } from '@mui/material'
import Head from 'next/head'
import ConfirmDialogProvider from '../src/client/components/common/ConfrmDialogProvider/ConfirmDialogProvider'
import FeedbackProvider from '../src/client/components/common/FeedbackProvider/FeedbackProvider'

const queryClient = new QueryClient()

// Create a theme instance.
const theme = createTheme({
    palette: {
        primary: {
            main: '#01579b',
        },
        secondary: {
            main: '#fdd835',
        },
        error: {
            main: red.A400,
        },
    },
})

class WebApp extends App {
    static async getInitialProps({ Component, ctx }: AppContext): Promise<AppInitialProps> {
        const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}

        return { pageProps }
    }

    componentDidMount() {
        // Remove styles sent from server - we have generated out own
        document.getElementById('server-side-styles')?.remove()
    }

    componentWillUnmount() {}

    render() {
        const { Component, pageProps } = this.props

        return (
            <>
                <Head>
                    <meta name="viewport" content="initial-scale=1, width=device-width" />
                </Head>
                <QueryClientProvider client={queryClient}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline />
                        <ConfirmDialogProvider>
                            <FeedbackProvider>
                                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                                <Box sx={{ minHeight: '100vh' }}>
                                    <Component {...pageProps} />
                                </Box>
                            </FeedbackProvider>
                        </ConfirmDialogProvider>
                    </ThemeProvider>
                </QueryClientProvider>
            </>
        )
    }
}

export default appWithTranslation(WebApp)
