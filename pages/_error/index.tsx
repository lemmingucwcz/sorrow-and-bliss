import * as React from 'react'
import { NextPage } from 'next'
import { useTranslation, WithTranslation } from 'next-i18next'
import { createUseStyles } from 'react-jss'
import Link from 'next/link'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

interface ErrorPageProps extends WithTranslation {
    statusCode?: number
}

const useStyles = createUseStyles({
    row: {
        padding: '100px 0 120px',
        fontSize: '1.5rem',
    },
    link: {
        fontSize: '1rem',
    },
})

const Error: NextPage<ErrorPageProps, {}> = ({ statusCode }) => {
    const classes = useStyles()
    const { t } = useTranslation('common')

    return (
        <div className={classes.row}>
            {!statusCode || statusCode === 404 ? t('Error.notFound') : t('Error.otherError', { statusCode })}
            <br />
            <br />
            <Link href={'/'}>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a className={classes.link}>{t('Error.toHomepage')}</a>
            </Link>
        </div>
    )
}

Error.getInitialProps = async ({ locale, res, err }) => {
    let statusCode

    if (res) {
        ;({ statusCode } = res)
    } else if (err) {
        ;({ statusCode } = err)
    }

    return {
        ...(await serverSideTranslations(locale || '', ['common', 'player'])),
        statusCode,
    }
}

export default Error
