import { NextApiRequest, NextApiResponse } from 'next'

const healthCheckHandler = (req: NextApiRequest, res: NextApiResponse): void => {
    res.status(200).json({ status: 'OK' })
}

export default healthCheckHandler
