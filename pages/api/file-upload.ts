import { NextApiRequest, NextApiResponse } from 'next'
import { decodeRequestToken } from '../../src/utils/requestToken'
import { PlayerLetterService } from '../../src/server/services/letterService/PlayerLetterService'
import { getDatabase } from '../../src/server/db/apiDatabase'
import { GoogleDriveFileService } from '../../src/server/services/fileService/GoogleDriveFileService'
import formidable, { Fields, Files } from 'formidable'
import { AdminToken, PlayerToken } from '../../src/server/services/tokenService/tokenService'
import { OrgLetterService } from '../../src/server/services/letterService/OrgLetterService'
import * as fs from 'fs'
import { getFileExtension } from '../../src/utils/fileUtils'

const parseRequest = (req: NextApiRequest): Promise<{ readonly fields: Fields; readonly files: Files }> =>
    new Promise((resolve, reject) => {
        const form = new formidable.IncomingForm({ keepExtensions: true })
        form.parse(req, (err, fields, files) => {
            if (err) {
                reject(err)
            }
            resolve({ fields, files })
        })
    })

const fileUploadHandler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { fields, files } = await parseRequest(req)

    const { letterId, fileNo, runId } = fields
    const file = files.file

    if (
        typeof letterId !== 'string' ||
        typeof fileNo !== 'string' ||
        typeof runId !== 'string' ||
        Array.isArray(file)
    ) {
        res.status(400)
        res.send('Bad Request')
        return
    }

    const auth = decodeRequestToken(req)
    if (!auth) {
        // No valid token
        res.status(403)
        res.send('Invalid token')
        return
    }

    const fileStream = fs.createReadStream(file.filepath)

    const db = await getDatabase()

    const pAuth = auth as PlayerToken
    const aAuth = auth as AdminToken
    if (pAuth.characterId) {
        const letter = await PlayerLetterService.playerGetLetterById(db, pAuth, parseInt(letterId))

        if (!letter) {
            // Letter not found
            res.status(404)
            res.send('Letter not found')
            return
        }

        await PlayerLetterService.playerSaveLetterFile(db, GoogleDriveFileService, pAuth, {
            serialNumber: letter.serialNumber,
            sentDate: letter.sentDate,
            recipientId: letter.recipientId,
            fileContents: fileStream,
            isAnonymous: !letter.senderId,
            fileNo: parseInt(fileNo),
            fileExtension: getFileExtension(file.originalFilename || ''),
            mimeType: file.mimetype ?? undefined,
        })
    }
    if (aAuth.email) {
        const letter = await OrgLetterService.orgGetLetterById(db, parseInt(runId), parseInt(letterId))

        if (!letter) {
            // Letter not found
            res.status(404)
            res.send('Letter not found')
            return
        }

        await OrgLetterService.orgSaveLetterFile(db, GoogleDriveFileService, parseInt(runId), {
            creatorId: letter.creatorId || 0,
            fileNo: parseInt(fileNo),
            fileContents: fileStream,
            isAnonymous: !letter.senderId,
            recipientId: letter.recipientId,
            sentDate: letter.sentDate,
            serialNumber: letter.serialNumber,
            fileExtension: getFileExtension(file.originalFilename || ''),
            mimeType: file.mimetype ?? undefined,
        })
    }

    fileStream.close()
    fs.unlinkSync(file.filepath)

    res.status(201)
    res.send('Created')
}

export const config = {
    api: {
        bodyParser: false,
    },
}

export default fileUploadHandler
