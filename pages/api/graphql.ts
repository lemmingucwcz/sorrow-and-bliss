import { ApolloServer } from 'apollo-server-micro'
import { MicroRequest } from 'apollo-server-micro/dist/types'
import { ServerResponse } from 'http'
import resolvers from 'src/server/resolvers'
// eslint-disable-next-line import/no-extraneous-dependencies
import { GraphQLRequestContext } from 'apollo-server-types'
import { ResolverContext } from 'src/server/resolvers/context'
import { AdminToken, PlayerToken, TokenService } from 'src/server/services/tokenService/tokenService'
import { getDatabase } from 'src/server/db/apiDatabase'

// Make sure ENV variables are loaded
require('src/utils/env')

const typeDefs = require('src/graphql/schema.graphql')

const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
    plugins: [
        {
            async requestDidStart(context: GraphQLRequestContext<ResolverContext>): Promise<void> {
                // Put database to context
                context.context.db = await getDatabase()

                // Check auth and decode token if present
                const authorization = context.request.http?.headers.get('Authorization')
                if (authorization && authorization.startsWith('Bearer ')) {
                    const token = authorization.substring(7)
                    const decoded = TokenService.verifyToken(token)
                    if ((decoded as AdminToken).email) {
                        context.context.adminEmail = (decoded as AdminToken).email
                    } else {
                        context.context.playerToken = decoded as PlayerToken
                    }
                }
            },
        },
    ],
})

export const config = {
    api: {
        bodyParser: false,
    },
}

let handler: (req: MicroRequest, res: ServerResponse) => Promise<void>
apolloServer.start().then(() => {
    handler = apolloServer.createHandler({ path: '/api/graphql' })
})

const grahpqlHandler = async (req: MicroRequest, res: ServerResponse) => {
    if (!handler) {
        console.log('Delaying request because apollo is starting')
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(grahpqlHandler(req, res))
            }, 200)
        })
    }

    return handler(req, res)
}

export default grahpqlHandler
