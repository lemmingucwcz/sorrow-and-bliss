import { NextApiRequest, NextApiResponse } from 'next'
import { TokenService } from '../../../../src/server/services/tokenService/tokenService'
import { GoogleDriveFileService } from '../../../../src/server/services/fileService/GoogleDriveFileService'

const fileDownloadHandler = async (req: NextApiRequest, res: NextApiResponse) => {
    const token = TokenService.verifyDownloadToken(req.query.token as string)
    if (!token) {
        res.status(400)
        res.send('Token not found or invalid')
    }

    const file = await GoogleDriveFileService.readFile(token.directoryId, token.fileName)
    if (!file) {
        res.status(404)
        res.send('File not found')
        return
    }

    const { contentType, stream } = file

    res.status(200)
    res.setHeader('Content-type', contentType)
    res.send(stream)
}

export default fileDownloadHandler
