import React from 'react'
import { NextPage } from 'next'
import { loadServerSideTranslations } from '../../src/utils/ssrLocale'
import AdminTopBar from 'src/client/components/admin/AdminTopBar/AdminTopBar'
import AdminBottomNavigation from '../../src/client/components/admin/AdminBottomNavigation/AdminBottomNavigation'
import AdminDashboardContent from '../../src/client/components/admin/AdminDashboardContent/AdminDashboardContent'
import CredentialsProvider from '../../src/client/components/common/CredentialsProvider/CredentialsProvider'

interface Props {}
interface InitialProps {}

const AdminDashboardPage: NextPage<Props, InitialProps> = () => {
    return (
        <CredentialsProvider pathType="admin">
            <AdminTopBar />
            <AdminDashboardContent />
            <AdminBottomNavigation />
        </CredentialsProvider>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'admin'])

export default AdminDashboardPage
