import React from 'react'
import { NextPage } from 'next'
import { loadServerSideTranslations } from '../../src/utils/ssrLocale'
import AdminTopBar from 'src/client/components/admin/AdminTopBar/AdminTopBar'
import AdminBottomNavigation from 'src/client/components/admin/AdminBottomNavigation/AdminBottomNavigation'
import AdminRunsContent from 'src/client/components/admin/AdminRunsContent/AdminRunsContent'
import CredentialsProvider from '../../src/client/components/common/CredentialsProvider/CredentialsProvider'

interface Props {}
interface InitialProps {}

const AdminRunsPage: NextPage<Props, InitialProps> = () => {
    return (
        <CredentialsProvider pathType="admin">
            <AdminTopBar />
            <AdminRunsContent />
            <AdminBottomNavigation />
        </CredentialsProvider>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'admin'])

export default AdminRunsPage
