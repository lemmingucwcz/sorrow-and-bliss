import React from 'react'
import { NextPage } from 'next'
import PlayerTopBar from '../../src/client/components/player/PlayerTopBar/PlayerTopBar'
import { loadServerSideTranslations } from 'src/utils/ssrLocale'
import PlayerBottomNavigation from 'src/client/components/player/PlayerBottomNavigation/PlayerBottomNavigation'
import PlayerSentContent from 'src/client/components/player/PlayerSentContent/PlayerSentContent'
import CredentialsProvider from '../../src/client/components/common/CredentialsProvider/CredentialsProvider'

interface Props {}
interface InitialProps {}

const SentPlayerPage: NextPage<Props, InitialProps> = () => {
    return (
        <CredentialsProvider pathType="player">
            <PlayerTopBar />
            <PlayerSentContent />
            <PlayerBottomNavigation />
        </CredentialsProvider>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'player'])

export default SentPlayerPage
