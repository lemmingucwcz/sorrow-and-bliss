import React from 'react'
import { NextPage } from 'next'
import PlayerTopBar from '../../src/client/components/player/PlayerTopBar/PlayerTopBar'
import PlayerInfoContent from 'src/client/components/player/PlayerInfoContent/PlayerInfoContent'
import { loadServerSideTranslations } from 'src/utils/ssrLocale'
import PlayerBottomNavigation from 'src/client/components/player/PlayerBottomNavigation/PlayerBottomNavigation'
import CredentialsProvider from '../../src/client/components/common/CredentialsProvider/CredentialsProvider'

interface Props {}
interface InitialProps {}

const PlayerDashboardPage: NextPage<Props, InitialProps> = () => {
    return (
        <CredentialsProvider pathType="player">
            <PlayerTopBar />
            <PlayerInfoContent />
            <PlayerBottomNavigation />
        </CredentialsProvider>
    )
}

export const getServerSideProps = async ({ locale }: { locale?: string }) =>
    loadServerSideTranslations(locale, ['common', 'player'])

export default PlayerDashboardPage
