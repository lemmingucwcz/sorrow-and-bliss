const withPlugins = require('next-compose-plugins')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: ['client', 'server', 'both'].includes(process.env.BUNDLE_ANALYZE),
})
const nextRuntimeDotenv = require('next-runtime-dotenv')
const graphql = require('next-plugin-graphql')
const { i18n } = require('./next-i18next.config')

const withConfig = nextRuntimeDotenv({
    path: process.env.NODE_ENV === 'production' ? '.env.prod' : '.env.dev',
    public: [
        'LETTER_AGE_PENDING_WARN',
        'LETTER_AGE_PENDING_ERROR',
        'LETTER_AGE_LONG_UNCONFIRMED',
        'LETTER_AGE_LONG_UNANSWERED',
    ],
})

const nextConfig = {
    publicRuntimeConfig: {
        LETTER_AGE_PENDING_WARN: process.env.LETTER_AGE_PENDING_WARN,
        LETTER_AGE_PENDING_ERROR: process.env.LETTER_AGE_PENDING_ERROR,
        LETTER_AGE_LONG_UNCONFIRMED: process.env.LETTER_AGE_LONG_UNCONFIRMED,
        LETTER_AGE_LONG_UNANSWERED: process.env.LETTER_AGE_LONG_UNANSWERED,
    },
    i18n,
    webpack: config => {
        config.resolve.fallback = { fs: false, path: false }

        return config
    },
}

module.exports = withConfig(withPlugins([withBundleAnalyzer, graphql], nextConfig))
