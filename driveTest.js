const { google } = require('googleapis')
const { Readable } = require('stream')

const SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive.file',
]

/**
 * Lists the names and IDs of up to 10 files.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listFiles(drive) {
    drive.files.list(
        {
            pageSize: 10,
            fields: 'nextPageToken, files(*)',
            // q: "mimeType = 'application/vnd.google-apps.folder' and '1nQKtbSnrigpg-fqQW38rfoZ-4eBtmqve' in parents",
            // q: "'1Op3d64C3i_g-0gcxzOBEm_x0lTfDMSdE' in parents",
        },
        (err, res) => {
            if (err) return console.log('The API returned an error: ' + err)
            const files = res.data.files
            if (files.length) {
                console.log('Files:')
                files.map(file => {
                    // console.log(JSON.stringify(file, null, 2))
                    console.log(`${file.name} (${file.id}) (${file}) [${file.parents}]`)
                })
            } else {
                console.log('No files found.')
            }
        },
    )
}

function downloadFile(fileId, drive) {
    drive.files.get(
        {
            fileId: fileId,
            alt: 'media',
        },
        { responseType: 'stream' },
        (err, res) => {
            if (err) {
                console.error(err)
                return
            }

            res.data
                .on('data', function (d) {
                    console.log(d.toString())
                })
                .on('end', function () {
                    console.log('==== Done ====')
                })
        },
    )
}

function createDirectory(parentId, name, drive) {
    const fileMetadata = {
        name,
        mimeType: 'application/vnd.google-apps.folder',
        parents: [parentId],
    }
    drive.files.create(
        {
            resource: fileMetadata,
            fields: 'id',
        },
        function (err, file) {
            if (err) {
                // Handle error
                console.error(err)
            } else {
                console.log('Folder Id: ', file.data.id)
            }
        },
    )
}

function createFile(drive, parentId, name, mimeType, data) {
    const body = new Readable()
    body.push(data)
    body.push(null)
    const fileMetadata = {
        name,
        parents: [parentId],
    }
    const media = {
        mimeType,
        body,
    }
    drive.files.create(
        {
            resource: fileMetadata,
            media: media,
            fields: 'id',
        },
        function (err, file) {
            if (err) {
                // Handle error
                console.error(err)
            } else {
                console.log('File Id: ', file.data.id)
            }
        },
    )
}

function deleteFiles(drive, fileIds) {
    fileIds.forEach(fileId => {
        drive.files.delete(
            {
                fileId,
            },
            {},
        )
    })
}

function getServiceAccountAuth() {
    const auth = new google.auth.GoogleAuth({
        keyFile: 'sorrow-and-bliss-drive-key.json',
        scopes: SCOPES,
    })

    return auth
}

const auth = getServiceAccountAuth()
const drive = google.drive({ version: 'v3', auth })
// downloadFile('13LxASI6_BCLPqWqJ92USMG7ldDF-Y4jR', drive)
// createDirectory('1nQKtbSnrigpg-fqQW38rfoZ-4eBtmqve', '3. běh', drive)
// createFile(drive, '1nQKtbSnrigpg-fqQW38rfoZ-4eBtmqve', 'test.txt', 'text/plain', 'Hello world, this is a test file')
// deleteFiles(drive, ['1rST-S9qR8CC_dw2qXkMR3lxAdjURinW3'])

listFiles(drive)
