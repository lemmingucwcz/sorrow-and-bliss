declare namespace NodeJS {
    interface ProcessEnv {
        readonly JWT_SECRET: string
        readonly JWT_EXPIRATION: string
        readonly DATABASE_FILE: string
        readonly GOOGLE_BASE_FOLDER_ID: string
        readonly DOWNLOAD_JWT_SECRET: string
        readonly DOWNLOAD_JWT_EXPIRATION: string
        readonly LETTER_AGE_PENDING_WARN: string
        readonly LETTER_AGE_PENDING_ERROR: string
        readonly LETTER_AGE_LONG_UNCONFIRMED: string
        readonly LETTER_AGE_LONG_UNANSWERED: string
    }
}

declare namespace jest {
    interface Options {
        media?: string
        modifier?: string
        supports?: string
    }

    interface Matchers<R> {
        toHaveStyleRule(property: string, value?: Value, options?: Options): R
    }
}
