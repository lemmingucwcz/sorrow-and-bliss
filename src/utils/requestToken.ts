import { NextApiRequest } from 'next'
import { TokenService } from '../server/services/tokenService/tokenService'

export const decodeRequestToken = (req: NextApiRequest) => {
    const authorization = req.headers.authorization
    if (typeof authorization !== 'string') {
        // No authorization
        return null
    }

    const token = authorization.substring(7)
    const decoded = TokenService.verifyToken(token)
    if (!decoded) {
        // Not a valid player token
        return null
    }

    return decoded
}
