export class InvalidValueError extends Error {
    parameterName: string

    value: unknown

    constructor(parameterName: string, value: unknown) {
        super(`Invalid value of parameter ${parameterName}`)
        this.parameterName = parameterName
        this.value = value
    }
}
