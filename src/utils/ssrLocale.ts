import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export const loadServerSideTranslations = async (locale: string | undefined, namespaces: string[]) => ({
    props: { ...(await serverSideTranslations(locale || 'cs', namespaces)) },
})
