require('dotenv').config({ path: process.env.NODE_ENV === 'production' ? '.env.prod' : '.env.dev' })

/**
 * This file just loads env, export something so that it is a module
 */
// eslint-disable-next-line import/no-anonymous-default-export
export default 0
