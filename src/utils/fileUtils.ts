/**
 * Get file's extension
 */
export const getFileExtension = (name: string) => {
    const li = name.lastIndexOf('.')
    return li < 0 ? '' : name.substring(li + 1)
}

/**
 * Get string of extensions of files separated by dots
 */
export const getFileExtensions = (files: File[]) => files.map(file => getFileExtension(file.name)).join('.')
