import { Database } from 'sqlite3'
import { DB } from '../db'

export const setupRuns = async (db: Database) => {
    await DB.run(
        db,
        'CREATE TABLE GameRun (id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL, isActive INTEGER NOT NULL, created INTEGER NOT NULL, passwords TEXT NOT NULL, directoryId TEXT NOT NULL)',
        {},
    )
}
