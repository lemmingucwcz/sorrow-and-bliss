import { Database } from 'sqlite3'
import { DB } from '../db'

const tableName = 'AdminAccount'

const initialAdmins = ['michal.kara@gmail.com', 'skokanova.klara@gmail.com']

export const setupAdminAccounts = async (db: Database) => {
    await DB.run(db, `CREATE TABLE ${tableName} (id INTEGER NOT NULL PRIMARY KEY, email TEXT NOT NULL, sub TEXT)`, {})

    const stmt = db.prepare(`INSERT INTO ${tableName}(email) VALUES($email)`)
    initialAdmins.forEach(email => stmt.run({ $email: email }))
    await DB.finalize(stmt)
}
