import { Database } from 'sqlite3'
import { DB } from '../db'

const pcCharacters = [
    'Fitzwilliam Clairmont',
    'Cullodina Clairmontová',
    'Alec Clairmont',
    'Athenais Clairmontová',
    'Dorothea Snowleyová',
    'Anne Everlyová',
    'Edward Reed',
    'Frederick Tattersal',
    'Nathaniel Seymore',
    'Duncan Dupree',
    'Theresa Clare',
    'Bathsheba Snowleyová',
]

const ordinaryNpcs = [
    'Callan MacKinnon',
    'Cuthbert Cocke',
    'Edna Hancocková',
    'Elfrida Seymorová',
    'Fanny Westwoodová',
    'Gavin Young',
    'Geordie Aspic',
    'Gytha Boatwrighteová',
    'Heywood Shelley',
    'Jeremiah Herman',
    'Jonathan Harlowe',
    'Kendall Hathewaye',
    'Lachlan Dùghlas',
    'Murtagh MacKinnon',
    'Norma Winterbottomová',
    'Starla Tattersalová',
    'Theodor Thornton',
    'William Wilbeforce',
    'William Godwin',
    'Mary Jane Godwinová',
    'Marcello Lante della Rovere',
    'Giuseppe Marius Dorius Pamphili',
    'Jean-Joël Beaubois',
    'Madam Sibyla',
    'James Gillray',
    'Paní Merrihewová',
    'James Barry',
    'Psí závody',
]

export const setupCharacters = async (db: Database) => {
    await DB.run(
        db,
        'CREATE TABLE Character (id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL, isNPC INTEGER NOT NULL, ordinal INTEGER NOT NULL DEFAULT 99)',
        {},
    )

    const stmt1 = db.prepare('INSERT INTO Character(name, isNPC) VALUES($name, 0)')
    pcCharacters.forEach(name => stmt1.run({ $name: name }))
    await DB.finalize(stmt1)

    const stmt2 = db.prepare('INSERT INTO Character(name, ordinal, isNPC) VALUES($name, $ordinal, 1)')

    stmt2.run({ $name: 'The Daily Dispatch', $ordinal: 1 })

    stmt2.run({ $name: 'Policie', $ordinal: 2 })

    ordinaryNpcs.forEach(name => stmt2.run({ $name: name, $ordinal: 20 }))
    await DB.finalize(stmt2)
}
