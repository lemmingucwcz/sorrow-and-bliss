import { Database } from 'sqlite3'
import { setupRuns } from './gameRun'
import { setupCharacters } from './character'
import { setupLetters } from './letter'
import { setupAdminAccounts } from './adminAccount'

/**
 * Setup all tables in the database
 */
export const setupDatabase = async (db: Database) => {
    await setupRuns(db)
    await setupCharacters(db)
    await setupLetters(db)
    await setupAdminAccounts(db)
}
