import { Database } from 'sqlite3'
import { DB } from '../db'

export const setupLetters = async (db: Database) => {
    await DB.run(
        db,
        `CREATE TABLE Letter (` +
            `id INTEGER NOT NULL PRIMARY KEY, runId INTEGER NOT NULL, creatorId INTEGER NOT NULL, senderId INTEGER, ` +
            `recipientId INTEGER NOT NULL, serialNumber INTEGER NOT NULL, sentDate TEXT NOT NULL, receivedDate TEXT, orgComment TEXT, ` +
            `fileExtensions TEXT, isEndOfLine INTEGER NOT NULL, sentDateTime INTEGER NOT NULL, receivedDateTime INTEGER` +
            `)`,
        {},
    )
    await DB.run(db, 'CREATE INDEX Letter_rrs ON Letter(runId, recipientId, senderId)', {})
    await DB.run(db, 'CREATE INDEX Letter_rc ON Letter(runId, creatorId)', {})
}
