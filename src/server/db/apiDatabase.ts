import { Database } from 'sqlite3'
import { DB } from './db'
import { setupDatabase } from './setup'

let db: Database | null = null

/**
 * Provide cached database to be used by API calls
 */
export const getDatabase = async () => {
    if (db) {
        // We already have DB initialized
        return db
    }

    // Connect
    db = await DB.connect(process.env.DATABASE_FILE, 'rdwr')

    // Check schema is created
    let created = 0
    try {
        created =
            (await DB.get<{ created: number }>(db, 'SELECT 1 AS created FROM Character LIMIT 1', {}))?.created || 0
    } catch {}

    // Setup database if needed
    if (!created) {
        await setupDatabase(db)
    }

    return db
}
