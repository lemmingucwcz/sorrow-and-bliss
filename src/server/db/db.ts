import {
    Database as IDatabase,
    OPEN_READONLY,
    OPEN_READWRITE,
    RunResult,
    sqlite3 as Isqlite3,
    Statement,
} from 'sqlite3'

const sqlite3 = require('sqlite3').verbose()

type TParams = { [key: string]: string | number | undefined }

/**
 * Promisify callback-oriented sqlite3 interface
 */
export const DB = {
    connect: async (fileName: string, mode: 'rdonly' | 'rdwr' = 'rdonly'): Promise<IDatabase> =>
        new Promise((respond, reject) => {
            const db = new (sqlite3 as Isqlite3).Database(
                fileName,
                mode === 'rdwr' ? OPEN_READWRITE : OPEN_READONLY,
                err => {
                    if (err) {
                        reject(err)
                    }

                    respond(db)
                },
            )
        }),

    memory: async (): Promise<IDatabase> =>
        new Promise((respond, reject) => {
            const db = new (sqlite3 as Isqlite3).Database(':memory:', err => {
                if (err) {
                    reject(err)
                }

                respond(db)
            })
        }),

    close: async (db: IDatabase) =>
        new Promise<void>((respond, reject) => {
            db.close(err => {
                if (err) {
                    reject(err)
                }

                respond()
            })
        }),

    run: async (db: IDatabase, sql: string, params: TParams) =>
        new Promise<RunResult>((respond, reject) => {
            db.run(sql, params, function (err) {
                if (err) {
                    reject(err)
                }

                respond(this)
            })
        }),

    get: async <T>(db: IDatabase, sql: string, params: TParams) =>
        new Promise<T | null>((respond, reject) => {
            db.get(sql, params, (err, row) => {
                if (err) {
                    reject(err)
                }

                respond(row)
            })
        }),

    all: async <T>(db: IDatabase, sql: string, params: TParams) =>
        new Promise<T[]>((respond, reject) => {
            db.all(sql, params, (err, rows) => {
                if (err) {
                    reject(err)
                }

                respond(rows)
            })
        }),

    finalize: async (statement: Statement) =>
        new Promise<void>((respond, reject) => {
            statement.finalize(err => {
                if (err) {
                    reject(err)
                }

                respond()
            })
        }),
}
