import { DB } from 'src/server/db/db'
import { GameRunService } from '../GameRunService'
import { setupDatabase } from '../../../db/setup'
import { CharacterService } from '../../characterService/CharacterService'
import { MockFileService } from '../../fileService/MockFileService'
import { OrgLetterService } from '../../letterService/OrgLetterService'

// Insert initial test data
const setupDb = async () => {
    const db = await DB.memory()
    await setupDatabase(db)

    const stmt = db.prepare(
        "INSERT INTO GameRun(name, isActive, created, passwords, directoryId) VALUES($name, $isActive, $created, '[]', 'some_id')",
    )
    stmt.run({
        $name: 'Run 1',
        $isActive: 0,
        $created: 1,
    })
    stmt.run({
        $name: 'Run 2',
        $isActive: 1,
        $created: 2,
    })
    stmt.run({
        $name: 'Run 3',
        $isActive: 1,
        $created: 3,
    })
    await DB.finalize(stmt)
    return db
}

describe('GameRunService', () => {
    test('listRuns()', async () => {
        const db = await setupDb()
        const runs = await GameRunService.listRuns(db)
        expect(runs).toHaveLength(3)
        expect(runs[0].name).toBe('Run 3')
        expect(runs[1].name).toBe('Run 2')
        expect(runs[2].name).toBe('Run 1')
        expect(runs[1].isActive).toBe(true)
        expect(runs[2].isActive).toBe(false)
    })

    test('listActiveRuns()', async () => {
        const db = await setupDb()
        const runs = await GameRunService.listActiveRuns(db)
        expect(runs).toHaveLength(2)
        expect(runs[0].name).toBe('Run 3')
        expect(runs[1].name).toBe('Run 2')
    })

    describe('getById()', () => {
        it('should return existing run by id', async () => {
            const db = await setupDb()
            const runs = await GameRunService.listActiveRuns(db)

            const run = await GameRunService.getRunById(db, runs[0].id)

            expect(run?.name).toBe(runs[0].name)
        })

        it('should return null when run does not exist', async () => {
            const db = await setupDb()
            const run = await GameRunService.getRunById(db, 0)

            expect(run).toBeNull()
        })
    })

    test('createRun()', async () => {
        const db = await setupDb()

        await GameRunService.createRun(db, MockFileService, { name: 'New Run' })

        const runs = await GameRunService.listRuns(db)
        expect(runs).toHaveLength(4)
        expect(runs[0].name).toBe('New Run')
        expect(runs[0].isActive).toBe(false)
    })

    test('updateRun()', async () => {
        const db = await setupDb()

        const runs = await GameRunService.listRuns(db)

        await GameRunService.updateRun(db, MockFileService, runs[0].id, { name: 'New Run' })

        const updatedRuns = await GameRunService.listRuns(db)
        expect(updatedRuns[0].name).toBe('New Run')
    })

    test('activateRun()', async () => {
        const db = await setupDb()

        const runs = await GameRunService.listRuns(db)
        expect(runs[2].isActive).toBe(false)

        await GameRunService.activateRun(db, runs[2].id)

        const updatedRuns = await GameRunService.listRuns(db)
        expect(updatedRuns[2].isActive).toBe(true)
    })

    test('deactivateRun()', async () => {
        const db = await setupDb()

        const runs = await GameRunService.listRuns(db)
        expect(runs[0].isActive).toBe(true)

        await GameRunService.deactivateRun(db, runs[0].id)

        const updatedRuns = await GameRunService.listRuns(db)
        expect(updatedRuns[0].isActive).toBe(false)
    })

    test('deleteRun()', async () => {
        const db = await setupDb()

        const runs = await GameRunService.listRuns(db)
        expect(runs).toHaveLength(3)
        const testRunId = runs[0].id

        // Add letters
        const letter1Id = (
            await OrgLetterService.orgCreateLetter(db, testRunId, {
                serialNumber: 1,
                sentDate: '2021-01-01',
                isAnonymous: false,
                creatorId: 1,
                recipientId: 2,
            })
        ).lastID
        const letter2Id = (
            await OrgLetterService.orgCreateLetter(db, runs[1].id, {
                serialNumber: 1,
                sentDate: '2021-01-01',
                isAnonymous: false,
                creatorId: 1,
                recipientId: 2,
            })
        ).lastID

        await GameRunService.deleteRun(db, MockFileService, testRunId)

        // Check run is deleted
        const updatedRuns = await GameRunService.listRuns(db)
        expect(updatedRuns).toHaveLength(2)

        // Check run letter is deleted
        const letter1 = await OrgLetterService.orgGetLetterById(db, testRunId, letter1Id)
        expect(letter1).toBeNull()

        // Check other run letter is not deleted
        const letter2 = await OrgLetterService.orgGetLetterById(db, runs[1].id, letter2Id)
        expect(letter2).not.toBeNull()
    })

    test('getRunPasswords()', async () => {
        const db = await setupDb()

        // Create new run
        const runId = (await GameRunService.createRun(db, MockFileService, { name: 'Test run' })).lastID

        const passwords = await GameRunService.getRunPasswords(db, runId)
        expect(passwords).toHaveLength(12)

        const anne = passwords.find(p => p.name === 'Anne Everlyová')
        expect(anne).toBeDefined()
        if (anne) {
            expect(anne.password).toHaveLength(12)
        }
    })

    describe('checkRunPassword()', () => {
        it('should return true when correct password is provided', async () => {
            const db = await setupDb()

            // Create new run
            const runId = (await GameRunService.createRun(db, MockFileService, { name: 'Test run' })).lastID

            // Find anne's password
            const passwords = await GameRunService.getRunPasswords(db, runId)
            const anne = passwords.find(p => p.name === 'Anne Everlyová')

            // Check
            expect(anne).toBeDefined()
            if (anne) {
                const valid = await GameRunService.checkRunPassword(db, {
                    runId,
                    characterId: anne.id,
                    password: anne.password,
                })
                expect(valid).toBe(true)
            }
        })

        test('should return false when correct password is provided', async () => {
            const db = await setupDb()

            // Create new run
            const runId = (await GameRunService.createRun(db, MockFileService, { name: 'Test run' })).lastID

            // Get character
            const characters = await CharacterService.listCharacters(db)

            // Check
            const valid = await GameRunService.checkRunPassword(db, {
                runId,
                characterId: characters[0].id,
                password: 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            })
            expect(valid).toBe(false)
        })
        test('should return false when invalid character is is provided', async () => {
            const db = await setupDb()

            // Create new run
            const runId = (await GameRunService.createRun(db, MockFileService, { name: 'Test run' })).lastID

            // Check
            const valid = await GameRunService.checkRunPassword(db, {
                runId,
                characterId: 999999999,
                password: 'abc',
            })
            expect(valid).toBe(false)
        })
    })
})
