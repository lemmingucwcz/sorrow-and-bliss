import {
    CreateGameRunRequest,
    GameRun,
    GameRunDB,
    RunCharacterPassword,
    UpdateGameRunRequest,
} from '../../model/GameRun'
import { Database } from 'sqlite3'
import { DB } from 'src/server/db/db'
import { CharacterService } from '../characterService/CharacterService'
import { GPW } from './gpw'
import { IFileService } from '../fileService/IFileService'
import { tableName as lettersTableName } from '../letterService/utils'

const runMapper = (run: GameRunDB): GameRun => ({ id: run.id, name: run.name, isActive: !!run.isActive } as GameRun)

interface CheckRunPasswordRequest {
    readonly runId: number
    readonly characterId: number
    readonly password: string
}

export const GameRunService = {
    listRuns: async (db: Database): Promise<GameRun[]> => {
        return DB.all<GameRunDB>(db, 'SELECT id, name, isActive FROM GameRun ORDER BY created DESC', {}).then(rows => {
            return rows.map(runMapper)
        })
    },

    listActiveRuns: async (db: Database): Promise<GameRun[]> => {
        return DB.all<GameRunDB>(
            db,
            'SELECT id, name, isActive FROM GameRun WHERE isActive=1 ORDER BY created DESC',
            {},
        ).then(rows => {
            return rows.map(runMapper)
        })
    },

    getRunDbById: async (db: Database, id: number): Promise<GameRunDB | null> =>
        DB.get<GameRunDB>(db, 'SELECT * FROM GameRun WHERE id=$id', { $id: id }),

    getRunById: async (db: Database, id: number): Promise<GameRun | null> =>
        GameRunService.getRunDbById(db, id).then(run => (run ? runMapper(run) : null)),

    createRun: async (db: Database, fileService: IFileService, request: CreateGameRunRequest) => {
        // Generate passwords for all PCs
        const characters = await CharacterService.listCharacters(db)
        const passwords = characters
            .filter(char => !char.isNPC)
            .map(char => ({ id: char.id, name: char.name, password: GPW.pronounceable(12) } as RunCharacterPassword))
        const directoryId = await fileService.createDirectory(request.name)

        return DB.run(
            db,
            'INSERT INTO GameRun(name, isActive, created, passwords, directoryId) VALUES($name, $isActive, $ts, $passwords, $directoryId)',
            {
                $name: request.name,
                $isActive: 0,
                $ts: new Date().getTime() / 1000,
                $passwords: JSON.stringify(passwords),
                $directoryId: directoryId,
            },
        )
    },

    getRunPasswords: async (db: Database, id: number): Promise<RunCharacterPassword[]> =>
        DB.get<GameRunDB>(db, 'SELECT passwords FROM GameRun WHERE id=$id', { $id: id }).then(run =>
            JSON.parse(run?.passwords || '[]'),
        ),

    checkRunPassword: async (db: Database, request: CheckRunPasswordRequest) => {
        const passwords = await GameRunService.getRunPasswords(db, request.runId)
        const char = passwords.find(ch => ch.id === request.characterId)
        return !!char && char.password === request.password
    },

    updateRun: async (db: Database, fileService: IFileService, id: number, request: UpdateGameRunRequest) => {
        const run = await GameRunService.getRunDbById(db, id)
        if (!run) {
            return { changes: 0 }
        }

        const res = await DB.run(db, 'UPDATE GameRun SET name=$name WHERE id=$id', { $name: request.name, $id: id })

        // Rename directory
        await fileService.renameDirectory(run.directoryId, request.name)

        return res
    },

    activateRun: async (db: Database, id: number) =>
        DB.run(db, 'UPDATE GameRun SET isActive=1 WHERE id=$id', { $id: id }),

    deactivateRun: async (db: Database, id: number) =>
        DB.run(db, 'UPDATE GameRun SET isActive=0 WHERE id=$id', { $id: id }),

    deleteRun: async (db: Database, fileService: IFileService, id: number) => {
        // Delete directory first
        const run = await GameRunService.getRunDbById(db, id)
        if (!run) {
            return { changes: 0 }
        }
        await fileService.deleteDirectory(run.directoryId)

        // Delete run letters
        await DB.run(db, `DELETE FROM ${lettersTableName} WHERE runId=$runId`, { $runId: id })

        // Delete run
        return DB.run(db, 'DELETE FROM GameRun WHERE id=$id', { $id: id })
    },
}
