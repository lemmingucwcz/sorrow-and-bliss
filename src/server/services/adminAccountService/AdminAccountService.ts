import { Database } from 'sqlite3'
import { DB } from 'src/server/db/db'
import { AdminAccount, AdminAccountDB } from '../../model/AdminAccount'

const tableName = 'AdminAccount'

const mapper = (account: AdminAccountDB): AdminAccount => account

export const AdminAccountService = {
    listAdminAccounts: async (db: Database) =>
        DB.all<AdminAccountDB>(db, `SELECT id, email FROM ${tableName} ORDER BY email`, {}).then(rows =>
            rows.map(mapper),
        ),

    addAdminAccount: async (db: Database, email: string) =>
        DB.run(db, `INSERT INTO ${tableName}(email) VALUES($email)`, { $email: email }),

    deleteAdminAccount: async (db: Database, id: number) =>
        DB.run(db, `DELETE FROM ${tableName} WHERE id=$id`, { $id: id }),

    isEmailAdmin: async (db: Database, email: string, sub?: string) => {
        const row = await DB.get<Pick<AdminAccountDB, 'sub'>>(db, `SELECT sub FROM ${tableName} WHERE email=$email`, {
            $email: email,
        })

        if (!row) {
            // Email does not exist
            return false
        }

        if (!sub) {
            // Sub not provided by the caller - just confirm email exists
            return true
        }

        if (row.sub) {
            // Check that sub is OK
            return row.sub === sub
        }

        // Logged in for the first time - log sub
        await DB.run(db, `UPDATE ${tableName} SET sub=$sub WHERE email=$email`, {
            $email: email,
            $sub: sub,
        })

        // OK
        return true
    },
}
