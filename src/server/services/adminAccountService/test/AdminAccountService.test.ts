import { DB } from '../../../db/db'
import { setupDatabase } from '../../../db/setup'
import { AdminAccountService } from '../AdminAccountService'

const setupDb = async () => {
    const db = await DB.memory()
    await setupDatabase(db)
    return db
}
describe('AdminAccountService', () => {
    test('listAdminAccounts()', async () => {
        const db = await setupDb()

        // Get list
        const list = await AdminAccountService.listAdminAccounts(db)

        // Check
        expect(list).toHaveLength(2)
        expect(list[0].email).toBe('michal.kara@gmail.com')
        expect(list[1].email).toBe('skokanova.klara@gmail.com')
    })

    test('addAdminAccount()', async () => {
        const db = await setupDb()

        // Add account
        await AdminAccountService.addAdminAccount(db, 'test@test.cz')

        // Check
        const list = await AdminAccountService.listAdminAccounts(db)
        const testAccount = list.find(a => a.email === 'test@test.cz')
        expect(testAccount).toBeDefined()
    })

    test('deleteAdminAccount()', async () => {
        const db = await setupDb()

        // Delete account
        const list = await AdminAccountService.listAdminAccounts(db)
        await AdminAccountService.deleteAdminAccount(db, list[0].id)

        // Check
        const list2 = await AdminAccountService.listAdminAccounts(db)
        const testAccount = list2.find(a => a.email === list[0].email)
        expect(testAccount).not.toBeDefined()
    })

    describe('isEmailAdmin()', () => {
        it('should return true when existing account is called with the same sub', async () => {
            const db = await setupDb()

            // Check 1
            const res1 = await AdminAccountService.isEmailAdmin(db, 'michal.kara@gmail.com', 'sub1')
            expect(res1).toBe(true)

            // Check 2
            const res2 = await AdminAccountService.isEmailAdmin(db, 'michal.kara@gmail.com', 'sub1')
            expect(res2).toBe(true)
        })

        it('should return false when existing account is called with different subs', async () => {
            const db = await setupDb()

            // Check 1
            const res1 = await AdminAccountService.isEmailAdmin(db, 'michal.kara@gmail.com', 'sub1')
            expect(res1).toBe(true)

            // Check 2
            const res2 = await AdminAccountService.isEmailAdmin(db, 'michal.kara@gmail.com', 'sub2')
            expect(res2).toBe(false)
        })

        it('should return false when account is not admin', async () => {
            const db = await setupDb()

            // Check
            const res = await AdminAccountService.isEmailAdmin(db, 'abc@def.cz', 'sub1')
            expect(res).toBe(false)
        })
    })
})
