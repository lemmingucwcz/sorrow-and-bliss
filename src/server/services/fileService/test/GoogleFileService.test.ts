/**
 * Tests for google drive service
 * Those modify data on real Google Drive
 * For development purposes only, not for regular running!
 */
import { GoogleDriveFileService } from '../GoogleDriveFileService'
import { Readable } from 'stream'

require('dotenv').config({ path: '.env.dev' })

describe('GoogleDriveService', () => {
    test.skip('createDirectory()', async () => {
        const id = await GoogleDriveFileService.createDirectory('Test directory 7')

        expect(id).toBeDefined()
        expect(id.length).toBeGreaterThan(0)
    })

    test.skip('storeFile()', async () => {
        const body = new Readable()
        body.push('Test content')
        body.push(null)

        const id = await GoogleDriveFileService.storeFile('1nQKtbSnrigpg-fqQW38rfoZ-4eBtmqve', 'Test file', body)
        expect(id.length).toBeGreaterThan(0)
    })
})
