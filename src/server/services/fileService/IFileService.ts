import { Stream } from 'stream'

export interface IFileService {
    readonly storeFile: (
        directoryId: string,
        fileName: string,
        fileContents: Stream,
        mimeType?: string,
    ) => Promise<string>
    readonly readFile: (
        directoryId: string,
        fileName: string,
    ) => Promise<{ readonly contentType: string; readonly stream: Stream } | null>
    readonly deleteFile: (directoryId: string, fileName: string) => Promise<number>
    readonly createDirectory: (name: string) => Promise<string>
    readonly deleteDirectory: (directoryId: string) => Promise<void>
    readonly renameFile: (directoryId: string, oldFileName: string, newFileName: string) => Promise<void>
    readonly renameDirectory: (directoryId: string, newName: string) => Promise<void>
}
