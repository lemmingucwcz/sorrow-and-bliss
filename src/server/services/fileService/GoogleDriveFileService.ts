import { IFileService } from './IFileService'
import { Stream } from 'stream'
import { google, drive_v3 } from 'googleapis'
import Params$Resource$Files$Create = drive_v3.Params$Resource$Files$Create

let drive: drive_v3.Drive

const SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive.file',
]

const getDrive = () => {
    if (drive) {
        return drive
    }

    const auth = new google.auth.GoogleAuth({
        keyFile: 'sorrow-and-bliss-drive-key.json',
        scopes: SCOPES,
    })

    drive = google.drive({ version: 'v3', auth })
    return drive
}

const resolveFile = async (d: drive_v3.Drive, directoryId: string, fileName: string) => {
    // Find file first
    const files = await d.files.list({
        pageSize: 10,
        fields: 'nextPageToken, files(id, mimeType)',
        q: `'${directoryId}' in parents and name = '${fileName.replace("'", "\\'")}'`,
    })

    const fls = files.data.files || []
    if (fls.length === 0) {
        return null
    }

    return { id: fls[0].id || '', mimeType: fls[0].mimeType || '' }
}

export const GoogleDriveFileService: IFileService = {
    storeFile: async (directoryId: string, fileName: string, fileContents: Stream, mimeType?: string) => {
        const d = getDrive()

        const fileMetadata = {
            name: fileName,
            parents: [directoryId],
        }
        const media = {
            body: fileContents,
            mimeType,
        }
        return d.files
            .create(
                {
                    requestBody: fileMetadata,
                    media: media,
                    fields: 'id',
                },
                {},
            )
            .then(file => {
                const { id } = file.data
                if (!id) {
                    throw new Error('Created file ID is null!')
                }
                return id
            })
    },

    readFile: async (directoryId: string, fileName: string) => {
        const d = getDrive()

        const file = await resolveFile(d, directoryId, fileName)
        if (!file) {
            return null
        }

        const res = await d.files.get(
            {
                fileId: file.id,
                alt: 'media',
            },
            { responseType: 'stream' },
        )

        return {
            contentType: file.mimeType,
            stream: res.data,
        }
    },

    deleteFile: async (directoryId: string, fileName: string) => {
        const d = getDrive()

        const file = await resolveFile(d, directoryId, fileName)
        if (!file) {
            return 0
        }

        await d.files.delete(
            {
                fileId: file.id,
            },
            {},
        )
        return 1
    },

    createDirectory: async (name: string) => {
        const d = getDrive()

        const fileMetadata: Params$Resource$Files$Create['requestBody'] = {
            name,
            mimeType: 'application/vnd.google-apps.folder',
            parents: [process.env.GOOGLE_BASE_FOLDER_ID],
        }

        const directoryId = await d.files
            .create({
                requestBody: fileMetadata,
                fields: 'id',
            })
            .then(file => {
                const { id } = file.data
                if (!id) {
                    throw new Error('No id for created directory')
                }
                return id
            })

        return directoryId
    },

    deleteDirectory: async directoryId => {
        const d = getDrive()

        await d.files.delete(
            {
                fileId: directoryId,
            },
            {},
        )
    },

    renameFile: async (directoryId: string, oldFileName: string, newFileName: string) => {
        const d = getDrive()

        const file = await resolveFile(d, directoryId, oldFileName)
        if (!file) {
            return
        }

        await d.files.update({
            fileId: file.id,
            requestBody: {
                name: newFileName,
            },
            fields: 'id',
        })
    },

    renameDirectory: async (directoryId: string, newName: string) => {
        const d = getDrive()
        await d.files.update({
            fileId: directoryId,
            requestBody: {
                name: newName,
            },
            fields: 'id',
        })
    },
}
