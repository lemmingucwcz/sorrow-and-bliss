import { IFileService } from './IFileService'
import { Readable } from 'stream'

let storedFiles: string[] = []

/**
 * Mocks file service - just stores names of files it has stored
 */
export const MockFileService: IFileService & { reset: () => void } = {
    readFile: async (directory, fileName) => {
        const path = `${directory}/${fileName}`
        const file = storedFiles.find(p => p === path)

        if (!file) {
            return null
        }

        const stream = new Readable()
        stream.push(fileName)
        stream.push(null)

        return {
            contentType: 'text/plain',
            stream,
        }
    },

    storeFile: async (directoryId: string, fileName: string) => {
        storedFiles.push(`${directoryId}/${fileName}`)
        return 'some_id'
    },

    deleteFile: async (directoryId: string, fileName: string) => {
        const path = `${directoryId}/${fileName}`
        storedFiles = storedFiles.filter(p => p !== path)
        return 1
    },

    renameFile: async (directoryId: string, oldFileName: string, newFileName: string) => {
        const oldPath = `${directoryId}/${oldFileName}`
        const newPath = `${directoryId}/${newFileName}`
        storedFiles = storedFiles.filter(p => p !== oldPath)
        storedFiles.push(newPath)
    },

    createDirectory: async () => {
        return 'some_id'
    },

    deleteDirectory: async () => {},

    renameDirectory: async () => {},

    reset: () => {
        storedFiles = []
    },
}
