import { DB } from '../../../db/db'
import { setupDatabase } from '../../../db/setup'
import { CharacterService } from '../../characterService/CharacterService'
import {
    createOrgGeneralLetters,
    createOrgIncomingLetters,
    createOrgUnansweredLetters,
    createUnifyTestLetters,
} from './orgLetterMock'
import { OrgCreateLetterRequest, OrgLetterService, OrgSaveLetterFileRequest } from '../OrgLetterService'
import {
    addLettersForPendingList,
    createMockForLongLettersList,
    createMockForNextLetterSerial,
    createStream,
} from './mockUtils'
import { MockFileService } from '../../fileService/MockFileService'
import { Letter } from 'src/server/model/Letter'
import { getFileName, getFileNameComponents } from '../utils'
import { Database } from 'sqlite3'
import { LetterDirection, LetterState } from '../../../../graphql/__generated__/typescript-operations'
import { Character } from '../../../model/Character'

const mockRunName = 'Run 1'
const mockDirectoryId = 'mockDirectoryId'

const setupDb = async () => {
    const db = await DB.memory()
    await setupDatabase(db)

    const res = await DB.run(
        db,
        "INSERT INTO GameRun(name, isActive, created, passwords, directoryId) VALUES($name, $isActive, $created, '[]', $directoryId)",
        {
            $name: mockRunName,
            $isActive: 1,
            $created: 1,
            $directoryId: mockDirectoryId,
        },
    )

    const runId = res.lastID

    const characters = await CharacterService.listCharacters(db)

    const npcId = (await CharacterService.createNPC(db, 'NPC')).lastID

    return { db, runId, characters, npcId }
}

const letterToFileRequest = (lrq: OrgCreateLetterRequest, fileNo: number): OrgSaveLetterFileRequest => {
    const frq: OrgSaveLetterFileRequest = {
        creatorId: lrq.creatorId,
        recipientId: lrq.recipientId,
        isAnonymous: lrq.isAnonymous,
        sentDate: lrq.sentDate,
        serialNumber: lrq.serialNumber,
        fileContents: createStream('xxx'),
        fileNo,
        fileExtension: (lrq.fileExtensions || '').split('.')[fileNo - 1],
    }
    return frq
}

const getLetterFile = async (db: Database, letter: Letter, fileNo: number) => {
    const { dir, fileBase } = await getFileNameComponents(db, letter.runId, letter.creatorId || 0, {
        recipientId: letter.recipientId,
        serialNumber: letter.serialNumber,
        isAnonymous: !letter.senderId,
        sentDate: letter.sentDate,
    })
    const fileName = getFileName(fileBase, fileNo, 'dat')
    return MockFileService.readFile(dir, fileName)
}

const extractSerials = (l: Letter[]) => l.map(({ serialNumber }) => serialNumber)

describe('OrgLetterService', () => {
    beforeEach(() => {
        MockFileService.reset()
    })

    describe('orgStats()', () => {
        test('stats for incoming letters', async () => {
            const { db, runId, characters, npcId } = await setupDb()
            await createOrgIncomingLetters(db, runId, characters, npcId)

            const stats = await OrgLetterService.orgStats(db, runId)
            expect(stats).toEqual({
                lettersTotal: 5,
                lettersInTransit: 4,
                lettersWaitingForResponse: 0,
            })
        })

        test('stats for pending list', async () => {
            const { db, runId, characters, npcId } = await setupDb()
            await addLettersForPendingList(db, runId, characters, npcId)

            const stats = await OrgLetterService.orgStats(db, runId)
            expect(stats).toEqual({
                lettersTotal: 13,
                lettersInTransit: 3,
                lettersWaitingForResponse: 3,
            })
        })
    })

    describe('orgListIncomingLetters()', () => {
        test('listing NPC letters', async () => {
            const { db, runId, characters, npcId } = await setupDb()
            await createOrgIncomingLetters(db, runId, characters, npcId)

            // Get incoming letters
            const list = await OrgLetterService.orgListIncomingLetters(db, runId, true)

            // Expect two letters:
            // - incoming unconfirmed regular letter
            // - incoming unconfirmed anonymous letter
            expect(list).toHaveLength(2)
            expect(list[0].serialNumber).toBe(1)
            expect(list[1].serialNumber).toBe(4)
        })

        test('listing PC letters', async () => {
            const { db, runId, characters, npcId } = await setupDb()
            await createOrgIncomingLetters(db, runId, characters, npcId)

            // Get incoming letters
            const list = await OrgLetterService.orgListIncomingLetters(db, runId, false)

            // Expect two letters:
            // - incoming unconfirmed regular letter
            // - incoming unconfirmed regular letter
            expect(list).toHaveLength(2)
            expect(list[0].serialNumber).toBe(3)
            expect(list[1].serialNumber).toBe(5)
        })
    })

    describe('orgListPendingLetters()', () => {
        test('list for NPCs', async () => {
            const { db, runId, characters, npcId } = await setupDb()
            await addLettersForPendingList(db, runId, characters, npcId)

            // Get the list
            const list = await OrgLetterService.orgListPendingLetters(db, runId, true)

            // We expect two letters to show as unanswered
            // - 22 exchange, last letter went to the NPC, unanswered
            // - 31 lone letter to the NPC, unanswered
            expect(list).toHaveLength(2)
            expect(list[0].serialNumber).toBe(22)
            expect(list[1].serialNumber).toBe(31)
        })

        test('list for PCs', async () => {
            const { db, runId, characters } = await setupDb()
            await addLettersForPendingList(db, runId, characters, characters[0].id)

            // Get the list
            const list = await OrgLetterService.orgListPendingLetters(db, runId, false)

            // We expect two letters to show as unanswered
            // - 22 exchange, last letter went to the PC, unanswered
            // - 31 lone letter to the PC, unanswered
            // - 81 lone letter to another PC, unanswered
            expect(list).toHaveLength(3)
            expect(list[0].serialNumber).toBe(22)
            expect(list[1].serialNumber).toBe(31)
            expect(list[2].serialNumber).toBe(81)
        })
    })

    describe('orgListAllLetters()', () => {
        test('list without filter', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const list = await OrgLetterService.orgListAllLetters(db, runId, {})

            // Just check there are all
            expect(list).toHaveLength(6)
        })

        test('limiting list by limit', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const listAll = await OrgLetterService.orgListAllLetters(db, runId, {})

            const listLimited = await OrgLetterService.orgListAllLetters(db, runId, { limit: 5 })
            expect(listLimited).toHaveLength(5)
            expect(extractSerials(listAll).slice(0, 5)).toEqual(extractSerials(listLimited))
        })

        test('getting second page using offset', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const listAll = await OrgLetterService.orgListAllLetters(db, runId, {})

            const listLimited = await OrgLetterService.orgListAllLetters(db, runId, { offset: 5 })
            expect(listLimited).toHaveLength(1)
            expect(extractSerials(listAll).slice(5)).toEqual(extractSerials(listLimited))
        })

        test('list letters concerning char0', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const char0 = characters[0].id
            const list = await OrgLetterService.orgListAllLetters(db, runId, { character1Id: char0.toString() })

            // Check number of letters and they concern char0
            expect(list).toHaveLength(4)
            list.forEach(letter => {
                expect(letter.creatorId === char0 || letter.recipientId === char0)
            })
        })

        test('list letters concerning char0 and char1', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const char0 = characters[0].id
            const char1 = characters[1].id
            const list = await OrgLetterService.orgListAllLetters(db, runId, {
                character1Id: char0.toString(),
                character2Id: char1.toString(),
            })

            // Check number of letters and they concern char0
            expect(list).toHaveLength(3)
            list.forEach(letter => {
                expect(
                    (letter.creatorId === char0 && letter.recipientId === char1) ||
                        (letter.creatorId === char1 && letter.recipientId === char0),
                )
            })
        })

        test('list letters from char0 and char1', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const char0 = characters[0].id
            const char1 = characters[1].id
            const list = await OrgLetterService.orgListAllLetters(db, runId, {
                character1Id: char0.toString(),
                character2Id: char1.toString(),
                letterDirection: LetterDirection.C1C2,
            })

            // Check number of letters and they concern char0
            expect(list).toHaveLength(2)
            list.forEach(letter => {
                expect(letter.creatorId === char0 && letter.recipientId === char1)
            })
        })

        test('list letters from char1 and char0', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)

            const char0 = characters[0].id
            const char1 = characters[1].id
            const list = await OrgLetterService.orgListAllLetters(db, runId, {
                character1Id: char0.toString(),
                character2Id: char1.toString(),
                letterDirection: LetterDirection.C2C1,
            })

            // Check number of letters and they concern char0
            expect(list).toHaveLength(1)
            expect(list[0].creatorId === char1 && list[0].recipientId === char0)
        })

        test('list letters concering char1, in transit', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgGeneralLetters(db, runId, characters)
            const char1 = characters[1].id
            const list = await OrgLetterService.orgListAllLetters(db, runId, {
                character1Id: char1.toString(),
                letterState: LetterState.Unconfirmed,
            })

            // Check number of letters and their properties
            expect(list).toHaveLength(2)
            list.forEach(letter => {
                expect((letter.creatorId === char1 || letter.recipientId === char1) && !letter.receivedDate)
            })
        })

        test('list letters concerning char1, unanswered', async () => {
            const { db, runId, characters } = await setupDb()
            await createOrgUnansweredLetters(db, runId, characters)
            const char1 = characters[1].id

            const list = await OrgLetterService.orgListAllLetters(db, runId, {
                character1Id: char1.toString(),
                letterState: LetterState.Unanswered,
            })

            // Check letters
            const serials = extractSerials(list)
            expect(serials).toEqual([4, 3])
        })
    })

    describe('orgCreateLetter', () => {
        test('creating regular letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const letterId = (
                await OrgLetterService.orgCreateLetter(db, runId, {
                    creatorId: characters[0].id,
                    recipientId: characters[1].id,
                    isAnonymous: false,
                    fileExtensions: 'dat',
                    sentDate: '2021-01-01',
                    serialNumber: 123,
                })
            ).lastID

            // Get and test the letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.serialNumber).toBe(123)
            expect(letter?.senderId).toBe(characters[0].id)
        })

        test('creating anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const letterId = (
                await OrgLetterService.orgCreateLetter(db, runId, {
                    creatorId: characters[0].id,
                    recipientId: characters[1].id,
                    isAnonymous: true,
                    fileExtensions: 'dat',
                    sentDate: '2021-01-01',
                    serialNumber: 644,
                })
            ).lastID

            // Get and test the letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.serialNumber).toBe(644)
            expect(letter?.senderId).toBeNull()
        })
    })

    describe('orgUpdateLetter()', () => {
        test('updating various fields', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                sentDate: '2021-01-01',
                serialNumber: 123,
                fileExtensions: 'dat',
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Update letter
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                orgComment: 'Our comment',
                receivedDate: '2021-01-03',
                sentDate: '2021-01-02',
                isEndOfLine: true,
                serialNumber: 321,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.orgComment).toBe('Our comment')
            expect(letter?.receivedDate).toBe('2021-01-03')
            expect(letter?.sentDate).toBe('2021-01-02')
            expect(letter?.isEndOfLine).toBe(true)
            expect(letter?.serialNumber).toBe(321)

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('changing from regular to anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Update letter
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                isAnonymous: true,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.senderId).toBeNull()

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('changing from anonymous to regular letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: true,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Update letter
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                isAnonymous: false,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.senderId).toBe(characters[0].id)

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('clearing received date', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const letterId = (
                await OrgLetterService.orgCreateLetter(db, runId, {
                    creatorId: characters[0].id,
                    recipientId: characters[1].id,
                    isAnonymous: false,
                    fileExtensions: 'dat',
                    sentDate: '2021-01-01',
                    serialNumber: 123,
                })
            ).lastID

            // Set received date
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                receivedDate: '2021-01-03',
            })

            // Check letter
            const letter1 = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter1?.receivedDate).toBe('2021-01-03')

            // Clear received date
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                receivedDate: '',
            })

            // Check letter
            const letter2 = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter2?.receivedDate).toBeNull()
        })

        test('updating creator and recipient of non-anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Set creator and recipient
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                creatorId: characters[2].id,
                recipientId: characters[3].id,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.creatorId).toBe(characters[2].id)
            expect(letter?.senderId).toBe(characters[2].id)
            expect(letter?.recipientId).toBe(characters[3].id)

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('updating creator and recipient of anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: true,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Set creator
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                creatorId: characters[2].id,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.creatorId).toBe(characters[2].id)
            expect(letter?.senderId).toBeNull()

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('updating creator and recipient of anonymous letter while making it non-anonymous', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const request = {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: true,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            }
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))

            // Set received date
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                creatorId: characters[2].id,
                isAnonymous: false,
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.creatorId).toBe(characters[2].id)
            expect(letter?.senderId).toBe(characters[2].id)

            // Check file is renamed
            if (letter) {
                const file = await getLetterFile(db, letter, 1)
                expect(file).not.toBeNull()
            }
        })

        test('changing number of files', async () => {
            const { db, runId, characters } = await setupDb()

            const request: OrgCreateLetterRequest = {
                sentDate: '2021-01-01',
                serialNumber: 123,
                recipientId: characters[1].id,
                creatorId: characters[0].id,
                isAnonymous: false,
                fileExtensions: 'dat.dat.dat',
            }

            // Create letter
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

            // Upload files
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 1))
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 2))
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, letterToFileRequest(request, 3))

            // Check last file exists
            const file = await MockFileService.readFile(
                mockDirectoryId,
                `${characters[0].name} - ${characters[1].name} 123 0101 - p3.dat`,
            )
            expect(file).not.toBeNull()

            // Set number of files
            await OrgLetterService.orgUpdateLetter(db, MockFileService, runId, {
                letterId,
                fileExtensions: 'dat.dat.dat.dat',
            })

            // Check letter
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.numberOfFiles).toBe(4)

            // Check last file was deleted
            if (letter) {
                const file3 = await getLetterFile(db, letter, 3)
                expect(file3).toBeNull()
            }
        })
    })

    describe('orgConfirmIncomingLetter()', () => {
        const createRequest = (characters: Character[]) =>
            ({
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
                orgComment: 'oldComment',
            } as OrgCreateLetterRequest)

        test('without org comment', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, createRequest(characters))).lastID

            // Confirm
            await OrgLetterService.orgConfirmIncomingLetter(db, runId, letterId, '2021-02-01')

            // Check
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.receivedDate).toBe('2021-02-01')
            expect(letter?.orgComment).toBe('oldComment')
        })

        test('with org comment', async () => {
            const { db, runId, characters } = await setupDb()

            // Create letter
            const letterId = (await OrgLetterService.orgCreateLetter(db, runId, createRequest(characters))).lastID

            // Confirm
            await OrgLetterService.orgConfirmIncomingLetter(db, runId, letterId, '2021-02-01', 'newComment')

            // Check
            const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
            expect(letter?.receivedDate).toBe('2021-02-01')
            expect(letter?.orgComment).toBe('newComment')
        })
    })

    test('orgMarkEndOfLine', async () => {
        const { db, runId, characters } = await setupDb()

        // Create letter
        const letterId = (
            await OrgLetterService.orgCreateLetter(db, runId, {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                fileExtensions: 'dat',
                sentDate: '2021-01-01',
                serialNumber: 123,
            })
        ).lastID

        // Confirm
        await OrgLetterService.orgConfirmIncomingLetter(db, runId, letterId, '2021-02-01')

        // Mark end of line
        await OrgLetterService.orgMarkEndOfLine(db, runId, letterId)

        // Check
        const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
        expect(letter?.isEndOfLine).toBe(true)
    })

    test('orgDeleteLetter()', async () => {
        const { db, runId, characters } = await setupDb()

        const request = {
            creatorId: characters[0].id,
            recipientId: characters[1].id,
            isAnonymous: false,
            fileExtension: 'dat',
            sentDate: '2021-01-01',
            serialNumber: 123,
        }

        // Create files
        await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, {
            ...request,
            creatorId: request.creatorId,
            fileNo: 1,
            fileContents: createStream('1'),
        })
        await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, {
            ...request,
            creatorId: request.creatorId,
            fileNo: 2,
            fileContents: createStream('2'),
        })

        // Create letter
        const letterId = (await OrgLetterService.orgCreateLetter(db, runId, request)).lastID

        // Delete letter
        await OrgLetterService.orgDeleteLetter(db, MockFileService, runId, letterId)

        // Check letter does not exist
        const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
        expect(letter).toBeNull()

        // Check files do not exist
        const file1 = await MockFileService.readFile(
            mockRunName,
            `${characters[0].name} - ${characters[1].name} 123 0101 - p1.dat`,
        )
        expect(file1).toBeNull()
        const file2 = await MockFileService.readFile(
            mockRunName,
            `${characters[0].name} - ${characters[1].name} 123 0101 - p2.dat`,
        )
        expect(file2).toBeNull()
    })

    describe('orgSaveLetterFile()', () => {
        test('Saving file for regular letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: false,
                sentDate: '2021-05-01',
                serialNumber: 123,
                fileNo: 1,
                fileContents: createStream('1'),
                fileExtension: 'dat',
            })
            // Check file
            const file = await MockFileService.readFile(
                mockDirectoryId,
                `${characters[0].name} - ${characters[1].name} 123 0105 - p1.dat`,
            )
            expect(file).not.toBeNull()
        })

        test('Saving file for anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            // Create file
            await OrgLetterService.orgSaveLetterFile(db, MockFileService, runId, {
                creatorId: characters[0].id,
                recipientId: characters[1].id,
                isAnonymous: true,
                sentDate: '2021-05-01',
                serialNumber: 123,
                fileNo: 1,
                fileContents: createStream('1'),
                fileExtension: 'dat',
            })
            // Check file
            const file = await MockFileService.readFile(
                mockDirectoryId,
                `Anonymous - ${characters[1].name} 123 0105 - p1.dat`,
            )
            expect(file).not.toBeNull()
        })
    })

    test('orgUnifyCharacters', async () => {
        const { db, runId, characters, npcId } = await setupDb()

        await createUnifyTestLetters(db, runId, characters, npcId)

        // Get serials of letters of char2
        const char2Serials = (
            await OrgLetterService.orgListAllLetters(db, runId, { character1Id: npcId.toString() })
        ).map(({ serialNumber }) => serialNumber)

        // Replace character 2 with character 3
        await OrgLetterService.orgUnifyCharacters(db, npcId, characters[3].id)

        // Check there are no letters with char2 now
        const list2 = await OrgLetterService.orgListAllLetters(db, runId, { character1Id: npcId.toString() })
        expect(list2).toHaveLength(0)

        // Check all former char2 letters are now belonging to char3
        const list3 = await OrgLetterService.orgListAllLetters(db, runId, { character1Id: characters[3].id.toString() })
        char2Serials.forEach(serial => {
            const letter = list3.find(({ serialNumber }) => serialNumber === serial)
            expect(letter).toBeDefined()
        })

        // Check char2 is deleted
        const char2 = await CharacterService.getCharacterById(db, npcId)
        expect(char2).toBeNull()
    })

    test('orgListPCLongUnconfirmedLetters()', async () => {
        const { db, runId, characters, npcId } = await setupDb()
        await createMockForLongLettersList(db, runId, characters, npcId)

        const res = await OrgLetterService.orgListPCLongUnconfirmedLetters(db, runId, 5)
        expect(extractSerials(res)).toEqual([1])
    })

    test('orgListPCLongPendingLetters()', async () => {
        const { db, runId, characters, npcId } = await setupDb()
        await createMockForLongLettersList(db, runId, characters, npcId)

        const res = await OrgLetterService.orgListPCLongPendingLetters(db, runId, 5)
        expect(extractSerials(res)).toEqual([21])
    })

    test('orgGetNextLetterSerial()', async () => {
        const { db, runId, characters } = await setupDb()
        const char0 = characters[0].id
        const char1 = characters[1].id
        const char2 = characters[2].id

        await createMockForNextLetterSerial(db, runId, characters)

        // No letter line
        const next1 = await OrgLetterService.orgGetNextLetterSerial(db, runId, char0, char2)
        expect(next1).toBe(1)

        // Single letter
        const next2 = await OrgLetterService.orgGetNextLetterSerial(db, runId, char1, char0)
        expect(next2).toBe(2)

        // Two letters
        const next3 = await OrgLetterService.orgGetNextLetterSerial(db, runId, char0, char1)
        expect(next3).toBe(3)
    })

    test('characterLetterCount()', async () => {
        const { db, runId, characters, npcId } = await setupDb()

        await createMockForLongLettersList(db, runId, characters, npcId)

        const count = await OrgLetterService.orgCharacterLetterCount(db, characters[0].id)
        expect(count).toBe(7)
    })
})
