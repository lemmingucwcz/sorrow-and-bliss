import { DB } from '../../../db/db'
import { setupDatabase } from '../../../db/setup'
import { CharacterService } from '../../characterService/CharacterService'
import { PlayerLetterService } from '../PlayerLetterService'
import { MockFileService } from '../../fileService/MockFileService'
import { addLettersForOutgoingList } from './playerLetterMock'
import { addLettersForPendingList, createMockForNextLetterSerial, createStream } from './mockUtils'

const mockRunName = 'Run 1'
const mockDirectoryId = 'mockDirectoryId'
const defaultMimeType = 'application/binary'

const setupDb = async () => {
    const db = await DB.memory()
    await setupDatabase(db)

    const res = await DB.run(
        db,
        "INSERT INTO GameRun(name, isActive, created, passwords, directoryId) VALUES($name, $isActive, $created, '[]', $directoryId)",
        {
            $name: mockRunName,
            $isActive: 1,
            $created: 1,
            $directoryId: mockDirectoryId,
        },
    )

    const runId = res.lastID

    const characters = await CharacterService.listCharacters(db)

    return { db, runId, characters }
}

describe('PlayerLetterService', () => {
    beforeEach(() => {
        MockFileService.reset()
    })

    test('playerStats()', async () => {
        // Setup
        const { db, runId, characters } = await setupDb()
        await addLettersForPendingList(db, runId, characters, characters[0].id)

        const stats = await PlayerLetterService.playerStats(db, { runId, characterId: characters[0].id })
        expect(stats.lettersInTransit).toBe(1)
    })

    test('playerListPendingLetters()', async () => {
        // Setup
        const { db, runId, characters } = await setupDb()
        await addLettersForPendingList(db, runId, characters, characters[0].id)

        // Get the list
        const list = await PlayerLetterService.playerListPendingLetters(db, { runId, characterId: characters[0].id })

        // We expect two letters to show as unanswered
        // - 22 exchange, last letter went to the player, unanswered
        // - 31 lone letter to the player, unanswered
        expect(list).toHaveLength(2)
        expect(list[0].serialNumber).toBe(22)
        expect(list[1].serialNumber).toBe(31)
    })

    describe('playerListOutgoingLetters()', () => {
        it('should return all ongoing letters when no recipient is specified', async () => {
            const { db, runId, characters } = await setupDb()
            await addLettersForOutgoingList(db, runId, characters)

            // Get the list
            const letters = await PlayerLetterService.playerListOutgoingLetters(db, {
                runId,
                characterId: characters[0].id,
            })

            // Check the letters we got
            expect(letters).toHaveLength(2)
            expect(letters[0].serialNumber).toBe(3)
            expect(letters[1].serialNumber).toBe(1)
        })

        it('should return all just letters to given recipient when specified', async () => {
            const { db, runId, characters } = await setupDb()
            await addLettersForOutgoingList(db, runId, characters)

            // Get the list
            const letters = await PlayerLetterService.playerListOutgoingLetters(
                db,
                {
                    runId,
                    characterId: characters[0].id,
                },
                characters[1].id,
            )

            // Check the letters we got
            expect(letters).toHaveLength(1)
            expect(letters[0].serialNumber).toBe(1)
        })
    })

    describe('playerCreateLetter()', () => {
        test('creating non-anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            const auth = {
                runId,
                characterId: characters[0].id,
            }

            // Create letter
            const res = await PlayerLetterService.playerCreateLetter(db, auth, {
                isAnonymous: false,
                sentDate: '2021-09-01',
                recipientId: characters[1].id,
                serialNumber: 12,
                fileExtensions: 'jpg.jpg',
            })

            // Check created letter
            const letter = await PlayerLetterService.playerGetLetterById(db, auth, res.lastID)
            expect(letter?.sentDate).toBe('2021-09-01')
            expect(letter?.recipientId).toBe(characters[1].id)
            expect(letter?.senderId).toBe(characters[0].id)
            expect(letter?.serialNumber).toBe(12)
            expect(letter?.fileExtensions).toBe('jpg.jpg')
        })

        test('creating anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()

            const auth = {
                runId,
                characterId: characters[0].id,
            }

            // Create letter
            const res = await PlayerLetterService.playerCreateLetter(db, auth, {
                isAnonymous: true,
                sentDate: '2021-10-12',
                recipientId: characters[1].id,
                serialNumber: 1,
                fileExtensions: 'jpg.jpg.jpg',
            })

            // Check created letter
            const letter = await PlayerLetterService.playerGetLetterById(db, auth, res.lastID)
            expect(letter?.sentDate).toBe('2021-10-12')
            expect(letter?.recipientId).toBe(characters[1].id)
            expect(letter?.senderId).toBeNull()
            expect(letter?.serialNumber).toBe(1)
            expect(letter?.numberOfFiles).toBe(3)
        })
    })

    test('playerUpdateLetter()', async () => {
        const { db, runId, characters } = await setupDb()

        const auth = {
            runId,
            characterId: characters[0].id,
        }

        // Create non-anonymous letter
        const res = await PlayerLetterService.playerCreateLetter(db, auth, {
            isAnonymous: false,
            sentDate: '2021-08-17',
            recipientId: characters[1].id,
            serialNumber: 12,
            fileExtensions: 'jpg.jpg.jpg',
        })

        const letterId = res.lastID

        // Change it into anonymous letter with different data
        await PlayerLetterService.playerUpdateLetter(db, MockFileService, auth, {
            id: letterId,
            isAnonymous: true,
            serialNumber: 13,
            fileExtensions: 'jpg',
            recipientId: characters[1].id,
            sentDate: '2021-08-16',
        })

        // Get the new letter
        const letter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)

        // Check the letter
        expect(letter?.sentDate).toBe('2021-08-16')
        expect(letter?.recipientId).toBe(characters[1].id)
        expect(letter?.senderId).toBeNull()
        expect(letter?.serialNumber).toBe(13)
        expect(letter?.fileExtensions).toBe('jpg')

        // Check old files are deleted
        const oldFile = await MockFileService.readFile(
            mockRunName,
            `${characters[0].id} - ${characters[1].id} 12 0817 - p1`,
        )
        expect(oldFile).toBeNull()

        // Check new file is there
        const newFile = await MockFileService.readFile(
            mockRunName,
            `${characters[0].id} - ${characters[1].id} 13 0816 - p1`,
        )
        expect(newFile).toBeDefined()
    })

    describe('playerConfirmIncomingLetter()', () => {
        const letterBase = {
            isAnonymous: false,
            sentDate: '2021-08-17',
            serialNumber: 1,
            numberOfFiles: 0,
        }

        test('confirming regular letter', async () => {
            const { db, runId, characters } = await setupDb()
            const senderId = characters[0].id
            const recipientId = characters[1].id

            const auth = {
                runId,
                characterId: senderId,
            }

            // Create letter
            const res = await PlayerLetterService.playerCreateLetter(db, auth, {
                ...letterBase,
                recipientId,
            })

            const letterId = res.lastID

            // Confirm received
            const res2 = await PlayerLetterService.playerConfirmIncomingLetter(
                db,
                { runId, characterId: recipientId },
                {
                    receivedDate: '2021-08-19',
                    sentDate: letterBase.sentDate,
                    serialNumber: letterBase.serialNumber,
                    senderId,
                },
            )

            // Expect change
            expect(res2.changes).toBe(1)

            // Check the letter
            const letter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)
            expect(letter?.receivedDate).toBe('2021-08-19')
        })

        test('confirming anonymous letter', async () => {
            const { db, runId, characters } = await setupDb()
            const senderId = characters[0].id
            const recipientId = characters[1].id

            const auth = {
                runId,
                characterId: senderId,
            }

            // Create letter
            const res = await PlayerLetterService.playerCreateLetter(db, auth, {
                ...letterBase,
                recipientId,
                isAnonymous: true,
            })

            const letterId = res.lastID

            // Confirm received
            const res2 = await PlayerLetterService.playerConfirmIncomingLetter(
                db,
                { runId, characterId: recipientId },
                {
                    receivedDate: '2021-08-19',
                    sentDate: letterBase.sentDate,
                    serialNumber: letterBase.serialNumber,
                    senderId: undefined,
                },
            )

            // Expect change
            expect(res2.changes).toBe(1)

            // Check the letter
            const letter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)
            expect(letter?.receivedDate).toBe('2021-08-19')
        })

        it('should not confirm letter when data are wrong', async () => {
            const { db, runId, characters } = await setupDb()
            const senderId = characters[0].id
            const recipientId = characters[1].id

            const auth = {
                runId,
                characterId: senderId,
            }

            // Create letter
            const res = await PlayerLetterService.playerCreateLetter(db, auth, {
                ...letterBase,
                recipientId,
            })

            const letterId = res.lastID

            // Confirm received
            const res2 = await PlayerLetterService.playerConfirmIncomingLetter(
                db,
                { runId, characterId: recipientId },
                {
                    receivedDate: '2021-08-19',
                    sentDate: letterBase.sentDate,
                    serialNumber: letterBase.serialNumber + 1,
                    senderId,
                },
            )

            // Expect no change
            expect(res2.changes).toBe(0)

            // Check the letter
            const letter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)
            expect(letter?.receivedDate).toBeNull()
        })
    })

    test('playerEndLetterLine()', async () => {
        const { db, runId, characters } = await setupDb()

        const auth = {
            runId,
            characterId: characters[0].id,
        }

        // Create letter
        const res = await PlayerLetterService.playerCreateLetter(db, auth, {
            isAnonymous: false,
            sentDate: '2021-08-17',
            recipientId: characters[1].id,
            serialNumber: 1,
        })

        const letterId = res.lastID

        // End letter line
        await PlayerLetterService.playerEndLetterLine(db, { runId, characterId: characters[1].id }, letterId)

        // Check the letter
        const letter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)
        expect(letter?.isEndOfLine).toBe(true)
    })

    describe('playerSaveLetterFile()', () => {
        test('Saving file from regular sender', async () => {
            const { db, runId, characters } = await setupDb()

            const auth = {
                runId,
                characterId: characters[0].id,
            }

            await PlayerLetterService.playerSaveLetterFile(db, MockFileService, auth, {
                fileNo: 2,
                isAnonymous: false,
                fileContents: createStream('1'),
                recipientId: characters[1].id,
                sentDate: '2021-08-10',
                serialNumber: 3,
                mimeType: 'application/binary',
                fileExtension: 'dat',
            })

            // Check the file
            const file = await MockFileService.readFile(
                mockDirectoryId,
                `${characters[0].name} - ${characters[1].name} 3 1008 - p2.dat`,
            )
            expect(file).not.toBeNull()
        })

        test('Saving file from anonymous sender', async () => {
            const { db, runId, characters } = await setupDb()

            const auth = {
                runId,
                characterId: characters[0].id,
            }

            await PlayerLetterService.playerSaveLetterFile(db, MockFileService, auth, {
                fileNo: 1,
                isAnonymous: true,
                fileContents: createStream('1'),
                recipientId: characters[1].id,
                sentDate: '2021-08-11',
                serialNumber: 7,
                fileExtension: 'dat',
                mimeType: defaultMimeType,
            })

            // Check the file
            const file = await MockFileService.readFile(
                mockDirectoryId,
                `Anonymous - ${characters[1].name} 7 1108 - p1.dat`,
            )
            expect(file).not.toBeNull()
        })
    })

    test('playerDeleteLetterFiles', async () => {
        const { db, runId, characters } = await setupDb()

        const auth = {
            runId,
            characterId: characters[0].id,
        }

        // Save files
        const requestBase = {
            isAnonymous: false,
            recipientId: characters[1].id,
            sentDate: '2021-08-11',
            serialNumber: 7,
            mimeType: defaultMimeType,
            fileExtension: 'dat',
        }
        await PlayerLetterService.playerSaveLetterFile(db, MockFileService, auth, {
            ...requestBase,
            fileNo: 1,
            fileContents: createStream('1'),
        })
        await PlayerLetterService.playerSaveLetterFile(db, MockFileService, auth, {
            ...requestBase,
            fileNo: 2,
            fileContents: createStream('2'),
        })

        // Create letter
        const letterId = (
            await PlayerLetterService.playerCreateLetter(db, auth, { ...requestBase, fileExtensions: 'dat.dat' })
        ).lastID

        // Check the file
        const file1 = await MockFileService.readFile(
            mockDirectoryId,
            `${characters[0].name} - ${characters[1].name} 7 1108 - p1.dat`,
        )
        expect(file1).not.toBeNull()

        // Delete files
        await PlayerLetterService.playerDeleteLetterFiles(db, MockFileService, auth, letterId)

        // Check the file again
        const file2 = await MockFileService.readFile(
            mockDirectoryId,
            `${characters[0].name} - ${characters[1].name} 7 1108 - p1.dat`,
        )
        expect(file2).toBeNull()
    })

    test('playerGetNextLetterSerial()', async () => {
        const { db, runId, characters } = await setupDb()
        const char0 = characters[0].id
        const char1 = characters[1].id
        const char2 = characters[2].id

        await createMockForNextLetterSerial(db, runId, characters)

        // No letter line
        const next1 = await PlayerLetterService.playerGetNextLetterSerial(db, { runId, characterId: char0 }, char2)
        expect(next1).toBe(1)

        // Single letter
        const next2 = await PlayerLetterService.playerGetNextLetterSerial(db, { runId, characterId: char1 }, char0)
        expect(next2).toBe(2)

        // Two letters
        const next3 = await PlayerLetterService.playerGetNextLetterSerial(db, { runId, characterId: char0 }, char1)
        expect(next3).toBe(3)
    })
})
