import { Database } from 'sqlite3'
import { Character } from '../../../model/Character'
import { mockCreateLetter } from './mockUtils'
import { OrgLetterService } from '../OrgLetterService'

export const createOrgIncomingLetters = async (db: Database, runId: number, characters: Character[], npcId: number) => {
    const char0 = characters[0].id
    const char1 = characters[1].id

    // Regular letter to NPC, not confirmed
    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        sentDate: '2021-01-01',
        recipientId: npcId,
        senderId: char0,
    })

    // Regular letter to NPC, confirmed
    await mockCreateLetter(db, {
        runId,
        serialNumber: 2,
        sentDate: '2021-01-02',
        receivedDate: '2021-01-03',
        recipientId: npcId,
        senderId: char0,
    })

    // Redular leter to PC, not confirmed
    await mockCreateLetter(db, {
        runId,
        serialNumber: 3,
        sentDate: '2021-01-04',
        recipientId: char0,
        senderId: npcId,
    })

    // Anonymous letter to NPC, not confirmed
    await mockCreateLetter(db, {
        runId,
        serialNumber: 4,
        isAnonymous: true,
        sentDate: '2021-01-05',
        recipientId: npcId,
        senderId: char0,
    })

    // Regular leter between PCs, not confirmed
    await mockCreateLetter(db, {
        runId,
        serialNumber: 5,
        sentDate: '2021-01-06',
        recipientId: char1,
        senderId: char0,
    })
}

export const createOrgGeneralLetters = async (db: Database, runId: number, characters: Character[]) => {
    const char0 = characters[0].id
    const char1 = characters[1].id
    const char2 = characters[2].id

    // Regular letter from char0 to char1, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        sentDate: '2021-01-01',
        receivedDate: '2021-01-02',
        senderId: char0,
        recipientId: char1,
    })

    // Regular letter from char1 to char0, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 2,
        sentDate: '2021-01-02',
        receivedDate: '2021-01-03',
        senderId: char1,
        recipientId: char0,
    })

    // Regular letter from char0 to char1, in transit
    await mockCreateLetter(db, {
        runId,
        serialNumber: 3,
        sentDate: '2021-01-03',
        senderId: char0,
        recipientId: char1,
    })

    // Regular letter from char1 to char2, in transit
    await mockCreateLetter(db, {
        runId,
        serialNumber: 4,
        sentDate: '2021-01-04',
        senderId: char1,
        recipientId: char2,
    })

    // Regular letter from char2 to char1, anonymous, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 5,
        sentDate: '2021-01-05',
        receivedDate: '2021-01-06',
        senderId: char2,
        recipientId: char1,
        isAnonymous: true,
    })

    // Regular letter from char0 to char2, in transit
    await mockCreateLetter(db, {
        runId,
        serialNumber: 6,
        sentDate: '2021-01-06',
        senderId: char0,
        recipientId: char2,
    })
}

export const createOrgUnansweredLetters = async (db: Database, runId: number, characters: Character[]) => {
    const char0 = characters[0].id
    const char1 = characters[1].id
    const char2 = characters[2].id
    const char3 = characters[3].id
    const char4 = characters[4].id

    // Regular letter from char0 to char1, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        sentDate: '2021-01-01',
        receivedDate: '2021-01-02',
        senderId: char0,
        recipientId: char1,
    })

    // Regular letter from char1 to char0, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 2,
        sentDate: '2021-01-02',
        receivedDate: '2021-01-03',
        senderId: char1,
        recipientId: char0,
    })

    // Regular letter from char0 to char1, delivered
    await mockCreateLetter(db, {
        runId,
        serialNumber: 3,
        sentDate: '2021-01-03',
        receivedDate: '2021-01-04',
        senderId: char0,
        recipientId: char1,
    })

    // Regular letter from char1 to char2, delivered
    await mockCreateLetter(db, {
        runId,
        serialNumber: 4,
        sentDate: '2021-01-04',
        receivedDate: '2021-01-05',
        senderId: char1,
        recipientId: char2,
    })

    // Regular letter from char2 to char1, anonymous, received
    await mockCreateLetter(db, {
        runId,
        serialNumber: 5,
        sentDate: '2021-01-05',
        receivedDate: '2021-01-06',
        senderId: char2,
        recipientId: char1,
        isAnonymous: true,
    })

    // Regular letter from char0 to char2, in transit
    await mockCreateLetter(db, {
        runId,
        serialNumber: 6,
        sentDate: '2021-01-06',
        senderId: char0,
        recipientId: char2,
    })

    // Regular letter from char3 to char1, undelivered
    await mockCreateLetter(db, {
        runId,
        serialNumber: 7,
        sentDate: '2021-01-07',
        senderId: char3,
        recipientId: char1,
    })

    // Regular letter from char4 to char1, delivered and marked as end of line
    const id = await mockCreateLetter(db, {
        runId,
        serialNumber: 8,
        sentDate: '2021-01-08',
        senderId: char4,
        recipientId: char1,
    })
    await OrgLetterService.orgMarkEndOfLine(db, runId, id)
}

export const createUnifyTestLetters = async (db: Database, runId: number, characters: Character[], npcId: number) => {
    const char0 = characters[0].id
    const char1 = characters[1].id
    const char3 = characters[2].id

    // char0 to char1
    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        sentDate: '2021-01-01',
        senderId: char0,
        recipientId: char1,
    })

    // char1 to char0
    await mockCreateLetter(db, {
        runId,
        serialNumber: 2,
        sentDate: '2021-01-01',
        senderId: char1,
        recipientId: char0,
    })

    // char0 to char2
    await mockCreateLetter(db, {
        runId,
        serialNumber: 3,
        sentDate: '2021-01-01',
        senderId: char0,
        recipientId: npcId,
    })

    // char2 to char0
    await mockCreateLetter(db, {
        runId,
        serialNumber: 4,
        sentDate: '2021-01-01',
        senderId: npcId,
        recipientId: char0,
    })

    // char0 to char3
    await mockCreateLetter(db, {
        runId,
        serialNumber: 5,
        sentDate: '2021-01-01',
        senderId: char0,
        recipientId: char3,
    })

    // char3 to char0
    await mockCreateLetter(db, {
        runId,
        serialNumber: 6,
        sentDate: '2021-01-01',
        senderId: char3,
        recipientId: char0,
    })
}
