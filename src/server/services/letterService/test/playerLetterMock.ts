import { Database } from 'sqlite3'
import { Character } from '../../../model/Character'
import { PlayerLetterService } from '../PlayerLetterService'

export const addLettersForOutgoingList = async (db: Database, runId: number, characters: Character[]) => {
    // Note: 0 is current character
    // Note: Serials are used to identify letters

    // Letter from 0 => 1
    await PlayerLetterService.playerCreateLetter(
        db,
        { runId, characterId: characters[0].id },
        {
            sentDate: '2021-01-01',
            recipientId: characters[1].id,
            serialNumber: 1,
            isAnonymous: false,
        },
    )

    // Letter from 1 => 0
    await PlayerLetterService.playerCreateLetter(
        db,
        { runId, characterId: characters[1].id },
        {
            sentDate: '2021-01-02',
            recipientId: characters[0].id,
            serialNumber: 2,
            isAnonymous: false,
        },
    )

    // Anonymous letter from 1 => 2
    await PlayerLetterService.playerCreateLetter(
        db,
        { runId, characterId: characters[0].id },
        {
            sentDate: '2021-01-03',
            recipientId: characters[2].id,
            serialNumber: 3,
            isAnonymous: true,
        },
    )
}
