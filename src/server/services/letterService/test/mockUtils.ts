import { Database } from 'sqlite3'
import { PlayerLetterService } from '../PlayerLetterService'
import { Character } from '../../../model/Character'
import { Readable } from 'stream'

export const mockCreateLetter = async (
    db: Database,
    {
        runId,
        senderId,
        recipientId,
        isAnonymous = false,
        sentDate,
        serialNumber,
        receivedDate,
    }: {
        runId: number
        senderId: number
        recipientId: number
        isAnonymous?: boolean
        sentDate: string
        serialNumber: number
        receivedDate?: string
    },
) => {
    const res = await PlayerLetterService.playerCreateLetter(
        db,
        { runId, characterId: senderId },
        {
            sentDate,
            recipientId,
            serialNumber,
            isAnonymous,
        },
    )

    if (receivedDate) {
        await PlayerLetterService.playerConfirmIncomingLetter(
            db,
            { runId, characterId: recipientId },
            { senderId: isAnonymous ? undefined : senderId, serialNumber, sentDate, receivedDate },
        )
    }

    return res.lastID
}

export const addLettersForPendingList = async (
    db: Database,
    runId: number,
    characters: Character[],
    thisCharacterId: number,
) => {
    // Note: Serials are used to identify letters
    const char1 = characters[1].id
    const char2 = characters[2].id
    const char3 = characters[3].id
    const char4 = characters[4].id
    const char5 = characters[5].id
    const char6 = characters[6].id
    const char7 = characters[7].id
    const char8 = characters[7].id

    /**
     * Line started by 0, answered (three letters total)
     */
    await mockCreateLetter(db, {
        runId,
        senderId: thisCharacterId,
        recipientId: char1,
        sentDate: '2021-01-01',
        receivedDate: '2021-01-02',
        serialNumber: 11,
    })
    await mockCreateLetter(db, {
        runId,
        senderId: char1,
        recipientId: thisCharacterId,
        sentDate: '2021-01-02',
        receivedDate: '2021-01-03',
        serialNumber: 12,
    })
    await mockCreateLetter(db, {
        runId,
        senderId: thisCharacterId,
        recipientId: char1,
        sentDate: '2021-01-03',
        serialNumber: 13,
    })

    /**
     * Line started by 0, unanswered
     */
    await mockCreateLetter(db, {
        runId,
        senderId: thisCharacterId,
        recipientId: char2,
        sentDate: '2021-02-01',
        receivedDate: '2021-02-02',
        serialNumber: 21,
    })
    await mockCreateLetter(db, {
        runId,
        senderId: char2,
        recipientId: thisCharacterId,
        sentDate: '2021-02-02',
        receivedDate: '2021-02-03',
        serialNumber: 22,
    })

    /**
     * Lone letter to 0 (unanswered)
     */
    await mockCreateLetter(db, {
        runId,
        senderId: char3,
        recipientId: thisCharacterId,
        sentDate: '2021-03-01',
        receivedDate: '2021-03-02',
        serialNumber: 31,
    })

    /**
     * Two consecutive letters to 0, answered
     */
    await mockCreateLetter(db, {
        runId,
        senderId: char4,
        recipientId: thisCharacterId,
        sentDate: '2021-04-01',
        receivedDate: '2021-04-02',
        serialNumber: 41,
    })
    await mockCreateLetter(db, {
        runId,
        senderId: char4,
        recipientId: thisCharacterId,
        sentDate: '2021-04-02',
        receivedDate: '2021-04-03',
        serialNumber: 42,
    })
    await mockCreateLetter(db, {
        runId,
        senderId: thisCharacterId,
        recipientId: char4,
        sentDate: '2021-04-04',
        serialNumber: 43,
    })

    /**
     * Anonymous letter to 0
     */
    await mockCreateLetter(db, {
        runId,
        senderId: char5,
        recipientId: thisCharacterId,
        sentDate: '2021-05-01',
        receivedDate: '2021-05-03',
        serialNumber: 51,
        isAnonymous: true,
    })

    /**
     * Letter to 0, marked as end of line
     */
    const letterId = await mockCreateLetter(db, {
        runId,
        senderId: char6,
        recipientId: thisCharacterId,
        sentDate: '2021-06-01',
        receivedDate: '2021-06-03',
        serialNumber: 61,
    })
    await PlayerLetterService.playerEndLetterLine(db, { runId, characterId: thisCharacterId }, letterId)

    /**
     * Letter to 0, undelivered
     */
    await mockCreateLetter(db, {
        runId,
        senderId: char7,
        recipientId: thisCharacterId,
        sentDate: '2021-07-01',
        serialNumber: 71,
    })

    /**
     * Letter from 0 to 8, unanswered
     */
    await mockCreateLetter(db, {
        runId,
        senderId: thisCharacterId,
        recipientId: char8,
        sentDate: '2021-08-01',
        receivedDate: '2021-08-02',
        serialNumber: 81,
    })
}

const fmt2 = (n: number) => (n < 10 ? `0${n}` : n.toString())

const getDateBefore = (now: number, daysBefore: number) => {
    const date = new Date((now - daysBefore * 86400) * 1000)
    return `${date.getFullYear()}-${fmt2(date.getMonth() + 1)}-${fmt2(date.getDate())}`
}

export const createMockForLongLettersList = async (
    db: Database,
    runId: number,
    characters: Character[],
    npcId: number,
) => {
    const now = Math.floor(new Date().getTime() / 1000)

    // Long undelivered letter for PC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        senderId: characters[0].id,
        recipientId: characters[1].id,
        sentDate: getDateBefore(now, 10),
    })

    // Long undelivered letter for NPC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 11,
        senderId: characters[0].id,
        recipientId: npcId,
        sentDate: getDateBefore(now, 10),
    })

    // Long unanswered letter for PC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 21,
        senderId: characters[0].id,
        recipientId: characters[2].id,
        sentDate: getDateBefore(now, 10),
        receivedDate: getDateBefore(now, 9),
    })

    // Long unanswered letter for NPC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 31,
        senderId: characters[1].id,
        recipientId: npcId,
        sentDate: getDateBefore(now, 10),
        receivedDate: getDateBefore(now, 9),
    })

    // Letter for PC received long ago but answered recently
    await mockCreateLetter(db, {
        runId,
        serialNumber: 41,
        senderId: characters[0].id,
        recipientId: characters[3].id,
        sentDate: getDateBefore(now, 10),
        receivedDate: getDateBefore(now, 9),
    })
    await mockCreateLetter(db, {
        runId,
        serialNumber: 42,
        senderId: characters[3].id,
        recipientId: characters[0].id,
        sentDate: getDateBefore(now, 4),
    })

    // Short undelivered letter for PC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 51,
        senderId: characters[0].id,
        recipientId: characters[4].id,
        sentDate: getDateBefore(now, 2),
    })

    // Short unanswered letter for PC
    await mockCreateLetter(db, {
        runId,
        serialNumber: 61,
        senderId: characters[0].id,
        recipientId: characters[5].id,
        sentDate: getDateBefore(now, 10),
        receivedDate: getDateBefore(now, 3),
    })
}

export const createMockForNextLetterSerial = async (db: Database, runId: number, characters: Character[]) => {
    const char0 = characters[0].id
    const char1 = characters[1].id

    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        senderId: char0,
        recipientId: char1,
        sentDate: '2021-01-01',
        receivedDate: '2021-01-02',
    })

    await mockCreateLetter(db, {
        runId,
        serialNumber: 1,
        senderId: char1,
        recipientId: char0,
        sentDate: '2021-02-01',
        receivedDate: '2021-02-02',
    })

    await mockCreateLetter(db, {
        runId,
        serialNumber: 2,
        senderId: char0,
        recipientId: char1,
        sentDate: '2021-03-01',
    })
}

export const createStream = (content: string) => {
    const s = new Readable()
    s.push(content)
    s.push(null)
    return s
}
