import { Letter, LetterDB } from '../../model/Letter'
import { IFileService } from '../fileService/IFileService'
import { Database } from 'sqlite3'
import { GameRunService } from '../gameRunService/GameRunService'
import { InvalidValueError } from '../../../utils/exceptions'
import { CharacterService } from '../characterService/CharacterService'
import { TokenService } from '../tokenService/tokenService'

export const tableName = 'Letter'

export const letterMapper =
    (maskOrgFields: boolean) =>
    (letter: LetterDB): Letter => ({
        id: letter.id,
        runId: letter.runId,
        creatorId: maskOrgFields ? undefined : letter.creatorId,
        senderId: letter.senderId,
        recipientId: letter.recipientId,
        numberOfFiles: letter.fileExtensions ? letter.fileExtensions.split('.').length : 0,
        fileExtensions: letter.fileExtensions || '',
        orgComment: maskOrgFields ? '' : letter.orgComment || '',
        receivedDate: letter.receivedDate,
        sentDate: letter.sentDate,
        serialNumber: letter.serialNumber,
        isEndOfLine: Boolean(letter.isEndOfLine),
    })

export const getFileNameComponents = async (
    db: Database,
    runId: number,
    creatorId: number,
    {
        recipientId,
        sentDate,
        isAnonymous,
        serialNumber,
    }: {
        recipientId: number
        sentDate: string
        isAnonymous: boolean
        serialNumber: number
    },
) => {
    const run = await GameRunService.getRunDbById(db, runId)
    if (!run) {
        throw new InvalidValueError('runId', runId)
    }

    const sender = isAnonymous ? null : await CharacterService.getCharacterById(db, creatorId)

    const recipient = await CharacterService.getCharacterById(db, recipientId)
    if (!recipient) {
        throw new InvalidValueError('recipientId', recipientId)
    }

    const splitDate = sentDate.split('-')

    return {
        dir: run.directoryId,
        fileBase: `${sender ? sender.name : 'Anonymous'} - ${recipient.name} ${serialNumber} ${splitDate[2]}${
            splitDate[1]
        }`,
    }
}

export const getFileName = (fileBase: string, fileNo: number, extension: string) =>
    `${fileBase} - p${fileNo}.${extension}`

type LetterForFile = Pick<Letter, 'runId' | 'serialNumber' | 'senderId' | 'recipientId' | 'sentDate' | 'fileExtensions'>

export const deleteLetterFiles = async (db: Database, fs: IFileService, letter: LetterForFile, creatorId: number) => {
    const { dir: oldDir, fileBase: oldFileBase } = await getFileNameComponents(db, letter.runId, creatorId, {
        serialNumber: letter.serialNumber,
        isAnonymous: !letter.senderId,
        recipientId: letter.recipientId,
        sentDate: letter.sentDate,
    })

    // Delete old files
    if (letter.fileExtensions) {
        const extensions = letter.fileExtensions.split('.')
        for (let i = 0; i < extensions.length; i++) {
            await fs.deleteFile(oldDir, getFileName(oldFileBase, i + 1, extensions[i]))
        }
    }
}

export const renameLetterFiles = async (
    db: Database,
    fs: IFileService,
    oldLetter: LetterForFile,
    oldCreatorId: number,
    newLetter: LetterForFile,
    newCreatorId: number,
) => {
    // Assert extensions are the same
    if (oldLetter.fileExtensions !== newLetter.fileExtensions) {
        throw new Error('Extensions changed when calling renameLetterFiles()')
    }

    const { dir: oldDir, fileBase: oldFileBase } = await getFileNameComponents(db, oldLetter.runId, oldCreatorId, {
        serialNumber: oldLetter.serialNumber,
        isAnonymous: !oldLetter.senderId,
        recipientId: oldLetter.recipientId,
        sentDate: oldLetter.sentDate,
    })

    const { dir: newDir, fileBase: newFileBase } = await getFileNameComponents(db, newLetter.runId, newCreatorId, {
        serialNumber: newLetter.serialNumber,
        isAnonymous: !newLetter.senderId,
        recipientId: newLetter.recipientId,
        sentDate: newLetter.sentDate,
    })

    if (oldDir !== newDir) {
        throw new Error('Directory changed - not supported!')
    }

    if (oldFileBase === newFileBase) {
        // No need to rename files
        return
    }

    // Rename files
    if (oldLetter.fileExtensions) {
        const extensions = oldLetter.fileExtensions.split('.')
        for (let i = 0; i < extensions.length; i++) {
            await fs.renameFile(
                oldDir,
                getFileName(oldFileBase, i + 1, extensions[i]),
                getFileName(newFileBase, i + 1, extensions[i]),
            )
        }
    }
}

export const getTokensForLetterFiles = async (db: Database, letter: Letter | undefined | null) => {
    // Check letter
    if (!letter) {
        throw new Error('Letter does not exist')
    }

    const { dir, fileBase } = await getFileNameComponents(db, letter.runId, letter.senderId || 0, {
        serialNumber: letter.serialNumber,
        isAnonymous: !letter.senderId,
        recipientId: letter.recipientId,
        sentDate: letter.sentDate,
    })

    if (!letter.fileExtensions) {
        return []
    }

    return letter.fileExtensions.split('.').map((extension, i) => {
        const fileName = getFileName(fileBase, i + 1, extension)
        const token = TokenService.createDownloadToken(dir, fileName)
        return { token, fileName }
    })
}
