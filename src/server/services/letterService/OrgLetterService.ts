import { Database } from 'sqlite3'
import { DB } from 'src/server/db/db'
import { Letter, LetterDB } from '../../model/Letter'
import {
    deleteLetterFiles,
    getFileName,
    getFileNameComponents,
    getTokensForLetterFiles,
    letterMapper,
    renameLetterFiles,
    tableName,
} from './utils'
import { InvalidValueError } from '../../../utils/exceptions'
import { IFileService } from '../fileService/IFileService'
import { CharacterService } from '../characterService/CharacterService'
import {
    AdminAllLettersFilter,
    InputMaybe,
    LetterDirection,
    LetterState,
} from '../../../graphql/__generated__/typescript-operations'
import { Stream } from 'stream'

export interface OrgCreateLetterRequest {
    readonly creatorId: number
    readonly recipientId: number
    readonly isAnonymous: boolean
    readonly sentDate: string
    readonly fileExtensions?: string
    readonly serialNumber: number
    readonly orgComment?: string
}

export interface OrgUpdateLetterRequest {
    readonly letterId: number
    readonly creatorId?: number
    readonly recipientId?: number
    readonly sentDate?: string
    readonly receivedDate?: string
    readonly isAnonymous?: boolean
    readonly isEndOfLine?: boolean
    readonly orgComment?: string
    readonly serialNumber?: number
    readonly fileExtensions?: string
}

export interface OrgSaveLetterFileRequest {
    readonly creatorId: number
    readonly recipientId: number
    readonly isAnonymous: boolean
    readonly sentDate: string
    readonly serialNumber: number
    readonly fileContents: Stream
    readonly fileNo: number
    readonly fileExtension: string
    readonly mimeType?: string
}

interface OrgStatsResponse {
    readonly lettersTotal: number
    readonly lettersInTransit: number
    readonly lettersWaitingForResponse: number
}

interface Count {
    readonly cnt: number
}

const cntMapper = (row: Count | null) => (row ? row.cnt : 0)

const orgLetterMapper = letterMapper(false)

const unansweredCondition = `AND NOT EXISTS (SELECT 1 FROM ${tableName} l2 WHERE l2.runId=$runId AND l2.recipientId=l1.senderId AND l2.senderId=l1.recipientId AND l2.sentDateTime >= l1.sentDateTime) `

const buildPendingSql = (fields: string, addCondition: string) =>
    `SELECT ${fields} FROM ${tableName} l1 LEFT JOIN Character rec ON rec.id=l1.recipientId WHERE l1.runId=$runId AND l1.receivedDate IS NOT NULL AND l1.isEndOfLine=0 AND NOT l1.senderId IS NULL ${addCondition} ` +
    unansweredCondition +
    `ORDER BY l1.receivedDateTime`

export const OrgLetterService = {
    orgStats: async (db: Database, runId: number): Promise<OrgStatsResponse> => {
        // Compute total letters
        const param = { $runId: runId }
        const lettersTotal = await DB.get<Count>(
            db,
            `SELECT COUNT(*) as cnt FROM ${tableName} WHERE runId=$runId`,
            param,
        ).then(cntMapper)

        const lettersInTransit = await DB.get<Count>(
            db,
            `SELECT COUNT(*) as cnt FROM ${tableName} WHERE runId=$runId AND receivedDate IS NULL`,
            param,
        ).then(cntMapper)

        const lettersWaitingForResponse = await DB.get<Count>(db, buildPendingSql('COUNT(*) as cnt', ''), param).then(
            cntMapper,
        )

        return { lettersTotal, lettersInTransit, lettersWaitingForResponse }
    },

    orgListPCLongUnconfirmedLetters: async (db: Database, runId: number, daysLimit: number) =>
        DB.all<LetterDB>(
            db,
            `SELECT l.* FROM ${tableName} l LEFT JOIN Character rec ON rec.id=l.recipientId WHERE l.runId=$runId AND l.receivedDate IS NULL AND rec.isNPC=0 AND l.sentDateTime <= $timeLimit ORDER BY l.sentDate`,
            { $runId: runId, $timeLimit: (Math.floor(new Date().getTime() / 1000) - daysLimit * 86400) * 1000 },
        ).then(rows => rows.map(orgLetterMapper)),

    orgListPCLongPendingLetters: async (db: Database, runId: number, daysLimit: number) => {
        const sql = buildPendingSql('l1.*', 'AND rec.isNPC=0 AND l1.receivedDateTime <= $timeLimit')
        const timeLimit = (Math.floor(new Date().getTime() / 1000) - daysLimit * 86400) * 1000
        return DB.all<LetterDB>(db, sql, { $runId: runId, $timeLimit: timeLimit }).then(rows =>
            rows.map(orgLetterMapper),
        )
    },

    orgListIncomingLetters: async (db: Database, runId: number, forNpc: boolean) =>
        DB.all<LetterDB>(
            db,
            `SELECT l.* FROM ${tableName} l LEFT JOIN Character rec ON rec.id=l.recipientId WHERE l.runId=$runId AND l.receivedDate IS NULL AND rec.isNPC=$isNPC ORDER BY l.sentDate`,
            { $runId: runId, $isNPC: forNpc ? 1 : 0 },
        ).then(rows => rows.map(orgLetterMapper)),

    orgListPendingLetters: async (db: Database, runId: number, forNpc: boolean) =>
        DB.all<LetterDB>(db, buildPendingSql('l1.*', 'AND rec.isNPC=$isNPC'), {
            $runId: runId,
            $isNPC: forNpc ? 1 : 0,
        }).then(rows => rows.map(orgLetterMapper)),

    orgListAllLetters: async (
        db: Database,
        runId: number,
        { character1Id, character2Id, letterState, letterDirection, limit, offset }: AdminAllLettersFilter,
    ) => {
        let sql = `SELECT l1.* FROM ${tableName} l1 WHERE l1.runId=$runId `

        switch (letterDirection ?? LetterDirection.Both) {
            case LetterDirection.Both:
                if (character1Id && character2Id) {
                    sql += `AND ((l1.creatorId=$ch1 AND l1.recipientId=$ch2) OR (l1.creatorId=$ch2 AND l1.recipientId=$ch1)) `
                } else {
                    if (character1Id) {
                        sql += `AND (l1.creatorId=$ch1 OR l1.recipientId=$ch1) `
                    }
                    if (character2Id) {
                        sql += `AND (l1.creatorId=$ch2 OR l1.recipientId=$ch2) `
                    }
                }
                break
            case LetterDirection.C1C2:
                if (character1Id) {
                    sql += `AND l1.creatorId=$ch1 `
                }
                if (character2Id) {
                    sql += `AND l1.recipientId=$ch2 `
                }
                break
            case LetterDirection.C2C1:
                if (character2Id) {
                    sql += `AND l1.creatorId=$ch2 `
                }
                if (character1Id) {
                    sql += `AND l1.recipientId=$ch1 `
                }
                break
        }

        if (letterState === LetterState.Unconfirmed) {
            sql += `AND l1.receivedDate IS NULL `
        }
        if (letterState === LetterState.Unanswered) {
            sql += `${unansweredCondition} AND l1.receivedDate IS NOT NULL AND l1.isEndOfLine=0 AND NOT l1.senderId IS NULL `
        }

        sql += `ORDER BY l1.sentDateTime DESC, l1.id DESC LIMIT $limit OFFSET $offset `

        return DB.all<LetterDB>(db, sql, {
            $runId: runId,
            $ch1: character1Id ? parseInt(character1Id) : undefined,
            $ch2: character2Id ? parseInt(character2Id) : undefined,
            $limit: limit || 20,
            $offset: offset || 0,
        }).then(rows => rows.map(orgLetterMapper))
    },

    orgGetLetterById: async (db: Database, runId: number, letterId: number): Promise<Letter | null> =>
        DB.get<LetterDB>(db, `SELECT * FROM ${tableName} WHERE runId=$runId and id=$id`, {
            $runId: runId,
            $id: letterId,
        }).then(res => (res ? orgLetterMapper(res) : null)),

    orgCreateLetter: async (db: Database, runId: number, request: OrgCreateLetterRequest) => {
        const sentDateTime = new Date(request.sentDate).getTime()

        const sql =
            `INSERT INTO ${tableName}(runId, creatorId, senderId, recipientId, serialNumber, sentDate, sentDateTime, fileExtensions, isEndOfLine, orgComment) ` +
            `VALUES ($runId, $creatorId, $senderId, $recipientId, $serialNumber, $sentDate, $sentDateTime, $fileExtensions, 0, $orgComment)`

        return DB.run(db, sql, {
            $runId: runId,
            $creatorId: request.creatorId,
            $senderId: request.isAnonymous ? undefined : request.creatorId,
            $recipientId: request.recipientId,
            $serialNumber: request.serialNumber,
            $sentDate: request.sentDate,
            $sentDateTime: sentDateTime,
            $fileExtensions: request.fileExtensions,
            $orgComment: request.orgComment || '',
        })
    },

    orgUpdateLetter: async (db: Database, fs: IFileService, runId: number, request: OrgUpdateLetterRequest) => {
        const oldLetter = await OrgLetterService.orgGetLetterById(db, runId, request.letterId)

        const receivedDateTime = request.receivedDate ? new Date(request.receivedDate).getTime() : undefined
        const sentDateTime = request.sentDate ? new Date(request.sentDate).getTime() : undefined

        if (!oldLetter) {
            throw new InvalidValueError('letterId', request.letterId)
        }

        const filesUploaded = typeof request.fileExtensions === 'string'
        if (filesUploaded) {
            // Delete letter files if number of files is set (=> user uploaded a new set of files)
            await deleteLetterFiles(db, fs, oldLetter, oldLetter.creatorId || 0)
        } else {
            // Rename files
            const newIsAnonymous = request.isAnonymous !== undefined ? request.isAnonymous : !oldLetter.senderId
            await renameLetterFiles(
                db,
                fs,
                oldLetter,
                oldLetter.creatorId || 0,
                {
                    fileExtensions: oldLetter.fileExtensions,
                    serialNumber: request.serialNumber || oldLetter.serialNumber,
                    sentDate: request.sentDate || oldLetter.sentDate,
                    recipientId: request.recipientId || oldLetter.recipientId,
                    senderId: newIsAnonymous ? undefined : request.creatorId || oldLetter.creatorId,
                    runId: oldLetter.runId,
                },
                request.creatorId || oldLetter.creatorId || 0,
            )
        }

        // Values we will set
        const sets: string[] = []

        if (request.creatorId) {
            sets.push('creatorId=$creatorId')

            if (request.isAnonymous === undefined) {
                // We need to load previous letter version to determine whether it is anonymous or not
                const origLetter = await OrgLetterService.orgGetLetterById(db, runId, request.letterId)
                if (origLetter?.senderId) {
                    // Affect also sender
                    sets.push('senderId=$creatorId')
                }
            }
        }

        if (request.recipientId) {
            sets.push('recipientId=$recipientId')
        }

        if (request.sentDate) {
            sets.push('sentDate=$sentDate')
            sets.push('sentDateTime=$sentDateTime')
        }

        if (request.receivedDate !== undefined) {
            if (request.receivedDate.length > 0) {
                sets.push('receivedDate=$receivedDate')
                sets.push('receivedDateTime=$receivedDateTime')
            } else {
                // Clear received date
                sets.push('receivedDate=NULL')
                sets.push('receivedDateTime=NULL')
            }
        }

        if (request.isAnonymous === true) {
            sets.push('senderId=NULL')
        }

        if (request.isAnonymous === false) {
            if (request.creatorId) {
                sets.push('senderId=$creatorId')
            } else {
                sets.push('senderId=creatorId')
            }
        }

        if (request.isEndOfLine !== undefined) {
            sets.push(`isEndOfLine=${request.isEndOfLine ? 1 : 0}`)
        }

        if (request.orgComment !== undefined) {
            sets.push('orgComment=$orgComment')
        }

        if (request.serialNumber !== undefined) {
            sets.push('serialNumber=$serialNumber')
        }

        if (filesUploaded) {
            sets.push('fileExtensions=$fileExtensions')
        }

        const sql = `UPDATE ${tableName} SET ${sets.join(', ')} WHERE id=$id`

        const params = {
            $creatorId: request.creatorId,
            $recipientId: request.recipientId,
            $id: request.letterId,
            $sentDate: request.sentDate,
            $sentDateTime: sentDateTime,
            $receivedDate: request.receivedDate || undefined,
            $receivedDateTime: receivedDateTime,
            $orgComment: request.orgComment,
            $serialNumber: request.serialNumber,
            $fileExtensions: request.fileExtensions,
        }

        return DB.run(db, sql, params)
    },

    orgConfirmIncomingLetter: async (
        db: Database,
        runId: number,
        letterId: number,
        receivedDate: string,
        orgComment?: InputMaybe<string>,
    ) => {
        const receivedDateTime = new Date(receivedDate).getTime()

        let sql = `UPDATE ${tableName} SET receivedDate=$receivedDate, receivedDateTime=$receivedDateTime`
        if (orgComment) {
            sql += `, orgComment=$orgComment`
        }
        sql += ` WHERE id=$id AND runId=$runId`

        return DB.run(db, sql, {
            $id: letterId,
            $runId: runId,
            $receivedDate: receivedDate,
            $receivedDateTime: receivedDateTime,
            $orgComment: orgComment || undefined,
        })
    },

    orgMarkEndOfLine: async (db: Database, runId: number, letterId: number) => {
        const sql = `UPDATE ${tableName} SET isEndOfLine=1 WHERE id=$id AND runId=$runId`

        return DB.run(db, sql, {
            $id: letterId,
            $runId: runId,
        })
    },

    orgDeleteLetterFiles: async (db: Database, fs: IFileService, runId: number, letterId: number) => {
        const letter = await OrgLetterService.orgGetLetterById(db, runId, letterId)
        if (!letter) {
            throw new InvalidValueError('letterId', letterId)
        }

        await deleteLetterFiles(db, fs, letter, letter.creatorId || 0) // creatorId will be actually always filled for org
    },

    orgDeleteLetter: async (db: Database, fs: IFileService, runId: number, letterId: number) => {
        // Delete files first
        await OrgLetterService.orgDeleteLetterFiles(db, fs, runId, letterId)

        // Delete row
        return DB.run(db, `DELETE FROM ${tableName} WHERE id=$id and runId=$runId`, { $id: letterId, $runId: runId })
    },

    orgSaveLetterFile: async (db: Database, fs: IFileService, runId: number, request: OrgSaveLetterFileRequest) => {
        const { dir, fileBase } = await getFileNameComponents(db, runId, request.creatorId, request)
        await fs.storeFile(
            dir,
            getFileName(fileBase, request.fileNo, request.fileExtension),
            request.fileContents,
            request.mimeType,
        )
    },

    orgUnifyCharacters: async (db: Database, sourceCharacterId: number, destinationCharacterId: number) => {
        // Change ids in letter
        await DB.run(
            db,
            `UPDATE ${tableName} SET creatorId=$destinationCharacterId WHERE creatorId=$sourceCharacterId`,
            { $sourceCharacterId: sourceCharacterId, $destinationCharacterId: destinationCharacterId },
        )
        await DB.run(db, `UPDATE ${tableName} SET senderId=$destinationCharacterId WHERE senderId=$sourceCharacterId`, {
            $sourceCharacterId: sourceCharacterId,
            $destinationCharacterId: destinationCharacterId,
        })
        await DB.run(
            db,
            `UPDATE ${tableName} SET recipientId=$destinationCharacterId WHERE recipientId=$sourceCharacterId`,
            { $sourceCharacterId: sourceCharacterId, $destinationCharacterId: destinationCharacterId },
        )

        // Delete source character
        return CharacterService.deleteNPC(db, sourceCharacterId)
    },

    orgGetTokensForLetterFiles: async (db: Database, runId: number, letterId: number) =>
        getTokensForLetterFiles(db, await OrgLetterService.orgGetLetterById(db, runId, letterId)),

    orgGetNextLetterSerial: async (db: Database, runId: number, senderId: number, recipientId: number) => {
        const sql = `SELECT MAX(serialNumber) AS maxSerialNumber FROM ${tableName} WHERE runId=$runId AND senderId=$senderId AND recipientId=$recipientId`
        const res = await DB.get<{ maxSerialNumber: number }>(db, sql, {
            $runId: runId,
            $senderId: senderId,
            $recipientId: recipientId,
        })
        return res ? res.maxSerialNumber + 1 : 1
    },

    orgCharacterLetterCount: async (db: Database, characterId: number) => {
        const sql = `SELECT COUNT(*) AS cnt FROM ${tableName} WHERE creatorId=$characterId OR recipientId=$characterId`
        return DB.get<{ cnt: number }>(db, sql, { $characterId: characterId }).then(row => row?.cnt || 0)
    },
}
