import { Database } from 'sqlite3'
import { IFileService } from '../fileService/IFileService'
import {
    deleteLetterFiles,
    getFileName,
    getFileNameComponents,
    getTokensForLetterFiles,
    letterMapper,
    renameLetterFiles,
    tableName,
} from './utils'
import { DB } from '../../db/db'
import { LetterDB } from 'src/server/model/Letter'
import { InvalidValueError } from '../../../utils/exceptions'
import { Stream } from 'stream'
import { GameRunService } from '../gameRunService/GameRunService'

interface PlayerAuth {
    readonly runId: number
    readonly characterId: number
}

export interface PlayerConfirmLetterRequest {
    readonly senderId: number | undefined
    readonly sentDate: string
    readonly receivedDate: string
    readonly serialNumber: number
}

export interface PlayerCreateLetterRequest {
    readonly recipientId: number
    readonly isAnonymous: boolean
    readonly sentDate: string
    readonly serialNumber: number
    readonly fileExtensions?: string
}

export interface PlayerSaveLetterFileRequest {
    readonly recipientId: number
    readonly sentDate: string
    readonly serialNumber: number
    readonly isAnonymous: boolean
    readonly fileNo: number
    readonly fileContents: Stream
    readonly fileExtension: string
    readonly mimeType?: string
}

export interface PlayerUpdateLetterRequest extends PlayerCreateLetterRequest {
    readonly id: number
}

export interface PlayerStatsResponse {
    readonly lettersInTransit: number
}

const playerLetterMapper = letterMapper(true)

const checkRunActive = async (db: Database, runId: number) => {
    const run = await GameRunService.getRunById(db, runId)
    if (!run?.isActive) {
        throw new Error('Run is no longer active')
    }
}

export const PlayerLetterService = {
    playerStats: async (db: Database, auth: PlayerAuth): Promise<PlayerStatsResponse> => {
        const sql = `SELECT COUNT(*) as lettersInTransit FROM ${tableName} WHERE runId=$runId AND recipientId=$characterId AND receivedDate IS NULL`

        return DB.get<PlayerStatsResponse>(db, sql, { $runId: auth.runId, $characterId: auth.characterId }).then(
            stats => (stats ? stats : ({ lettersInTransit: 0 } as PlayerStatsResponse)),
        )
    },

    playerListPendingLetters: async (db: Database, auth: PlayerAuth) =>
        DB.all<LetterDB>(
            db,
            `SELECT * FROM ${tableName} t1 WHERE runId=$runId AND recipientId=$characterId AND NOT receivedDateTime is NULL AND NOT senderId IS NULL AND NOT isEndOfLine=1 ` +
                `AND NOT EXISTS (SELECT 1 FROM ${tableName} t2 WHERE t2.runId=$runId AND t2.recipientId=t1.senderId AND t2.senderId=$characterId AND t2.sentDateTime >= t1.receivedDateTime) ORDER BY receivedDateTime`,
            {
                $runId: auth.runId,
                $characterId: auth.characterId,
            },
        ).then(rows => rows.map(playerLetterMapper)),

    playerListOutgoingLetters: async (db: Database, auth: PlayerAuth, recipientId?: number) => {
        let sql = `SELECT * FROM ${tableName} t1 WHERE runId=$runId AND creatorId=$characterId`
        if (recipientId) {
            sql += ` AND recipientId=$recipientId`
        }
        sql += ` ORDER BY sentDateTime DESC`

        return DB.all<LetterDB>(db, sql, {
            $runId: auth.runId,
            $characterId: auth.characterId,
            $recipientId: recipientId,
        }).then(rows => rows.map(playerLetterMapper))
    },

    playerGetLetterById: (db: Database, auth: PlayerAuth, id: number) =>
        DB.get<LetterDB>(db, `SELECT * FROM ${tableName} WHERE runId=$runId AND creatorId=$characterId AND id=$id`, {
            $runId: auth.runId,
            $characterId: auth.characterId,
            $id: id,
        }).then(letter => (letter ? playerLetterMapper(letter) : null)),

    playerConfirmIncomingLetter: async (db: Database, auth: PlayerAuth, request: PlayerConfirmLetterRequest) => {
        const receivedDateTime = new Date(request.receivedDate).getTime()

        let sql =
            `UPDATE ${tableName} SET receivedDate=$receivedDate, receivedDateTime=$receivedDateTime WHERE runId=$runId AND recipientId=$characterId ` +
            `AND sentDate=$sentDate AND serialNumber=$serialNumber `

        if (request.senderId) {
            sql += `AND senderId=$senderId`
        } else {
            sql += 'AND senderId is NULL'
        }

        return DB.run(db, sql, {
            $runId: auth.runId,
            $characterId: auth.characterId,
            $receivedDate: request.receivedDate,
            $receivedDateTime: receivedDateTime,
            $sentDate: request.sentDate,
            $serialNumber: request.serialNumber,
            $senderId: request.senderId,
        })
    },

    playerCreateLetter: async (db: Database, auth: PlayerAuth, request: PlayerCreateLetterRequest) => {
        await checkRunActive(db, auth.runId)

        const sentDateTime = new Date(request.sentDate).getTime()

        const sql =
            `INSERT INTO ${tableName}(runId, creatorId, senderId, recipientId, serialNumber, sentDate, sentDateTime, fileExtensions, isEndOfLine) ` +
            `VALUES ($runId, $creatorId, $senderId, $recipientId, $serialNumber, $sentDate, $sentDateTime, $fileExtensions, 0)`

        return DB.run(db, sql, {
            $runId: auth.runId,
            $creatorId: auth.characterId,
            $senderId: request.isAnonymous ? undefined : auth.characterId,
            $recipientId: request.recipientId,
            $serialNumber: request.serialNumber,
            $sentDate: request.sentDate,
            $sentDateTime: sentDateTime,
            $fileExtensions: request.fileExtensions,
        })
    },

    playerUpdateLetter: async (
        db: Database,
        fs: IFileService,
        auth: PlayerAuth,
        request: PlayerUpdateLetterRequest,
    ) => {
        await checkRunActive(db, auth.runId)

        const oldLetter = await PlayerLetterService.playerGetLetterById(db, auth, request.id)
        if (!oldLetter) {
            throw new InvalidValueError('letterId', request.id)
        }

        const filesUploaded = typeof request.fileExtensions === 'string'

        if (filesUploaded) {
            // New files are to be uploaded - delete old files first
            await deleteLetterFiles(db, fs, oldLetter, oldLetter.senderId || 0)
        } else {
            // Rename files
            const newIsAnonymous = request.isAnonymous !== undefined ? request.isAnonymous : !oldLetter.senderId
            await renameLetterFiles(
                db,
                fs,
                oldLetter,
                auth.characterId,
                {
                    fileExtensions: oldLetter.fileExtensions,
                    serialNumber: request.serialNumber,
                    sentDate: request.sentDate,
                    recipientId: request.recipientId,
                    senderId: newIsAnonymous ? undefined : auth.characterId,
                    runId: oldLetter.runId,
                },
                auth.characterId,
            )
        }

        // Update data
        const sentDateTime = new Date(request.sentDate).getTime()
        const sql = `UPDATE ${tableName} SET senderId=$senderId, recipientId=$recipientId, serialNumber=$serialNumber, sentDate=$sentDate, sentDateTime=$sentDateTime, fileExtensions=$fileExtensions WHERE id=$id`
        return DB.run(db, sql, {
            $senderId: request.isAnonymous ? undefined : auth.characterId,
            $recipientId: request.recipientId,
            $serialNumber: request.serialNumber,
            $sentDate: request.sentDate,
            $sentDateTime: sentDateTime,
            $fileExtensions: filesUploaded ? request.fileExtensions : oldLetter.fileExtensions,
            $id: oldLetter.id,
        })
    },

    playerDeleteLetterFiles: async (db: Database, fs: IFileService, auth: PlayerAuth, letterId: number) => {
        const oldLetter = await PlayerLetterService.playerGetLetterById(db, auth, letterId)
        if (!oldLetter) {
            throw new InvalidValueError('letterId', letterId)
        }

        await deleteLetterFiles(db, fs, oldLetter, auth.characterId)
    },

    playerSaveLetterFile: async (
        db: Database,
        fs: IFileService,
        auth: PlayerAuth,
        request: PlayerSaveLetterFileRequest,
    ) => {
        await checkRunActive(db, auth.runId)
        const { dir, fileBase } = await getFileNameComponents(db, auth.runId, auth.characterId, request)
        await fs.storeFile(
            dir,
            getFileName(fileBase, request.fileNo, request.fileExtension),
            request.fileContents,
            request.mimeType,
        )
    },

    playerEndLetterLine: async (db: Database, auth: PlayerAuth, letterId: number) => {
        const sql = `UPDATE ${tableName} SET isEndOfLine=1 WHERE runId=$runId AND recipientId=$characterId AND id=$id`
        return DB.run(db, sql, {
            $runId: auth.runId,
            $characterId: auth.characterId,
            $id: letterId,
        })
    },

    playerGetTokensForLetterFiles: async (db: Database, auth: PlayerAuth, letterId: number) =>
        getTokensForLetterFiles(db, await PlayerLetterService.playerGetLetterById(db, auth, letterId)),

    playerGetNextLetterSerial: async (db: Database, auth: PlayerAuth, recipientId: number) => {
        const sql = `SELECT MAX(serialNumber) AS maxSerialNumber FROM ${tableName} WHERE runId=$runId AND senderId=$senderId AND recipientId=$recipientId`
        const res = await DB.get<{ maxSerialNumber: number }>(db, sql, {
            $runId: auth.runId,
            $senderId: auth.characterId,
            $recipientId: recipientId,
        })
        return res ? res.maxSerialNumber + 1 : 1
    },
}
