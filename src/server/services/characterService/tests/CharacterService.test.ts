// Insert initial test data
import { DB } from '../../../db/db'
import { setupDatabase } from '../../../db/setup'
import { CharacterService } from '../CharacterService'

const setupDb = async () => {
    const db = await DB.memory()
    await setupDatabase(db)
    return db
}

describe('CharacterService', () => {
    test('listCharacters()', async () => {
        const db = await setupDb()

        const characters = await CharacterService.listCharacters(db)

        expect(characters.length).toBeGreaterThan(20)
        expect(characters.filter(({ isNPC }) => !isNPC).length).toBe(12)
        expect(characters.find(c => c.name === 'Dorothea Snowley'))
    })

    describe('getCharacterById()', () => {
        it('should return character when it is present', async () => {
            const testName = 'Edward Reed'

            const db = await setupDb()
            const characters = await CharacterService.listCharacters(db)
            const edward = characters.find(ch => ch.name === testName)
            expect(edward).toBeDefined()

            if (edward) {
                const res = await CharacterService.getCharacterById(db, edward.id)
                expect(res?.name).toBe(testName)
            }
        })

        it('should return null when character is not present', async () => {
            const db = await setupDb()
            const res = await CharacterService.getCharacterById(db, 0)
            expect(res).toBeNull()
        })
    })

    describe('getCharacterByName()', () => {
        it('should return character with the same name with different case', async () => {
            const db = await setupDb()

            const createdId = (await CharacterService.createNPC(db, 'John Snow')).lastID

            const character = await CharacterService.getNPCByName(db, 'JOHN SNOW')
            expect(character?.id).toBe(createdId)
        })

        it('should not return character that is not NPC', async () => {
            const db = await setupDb()
            const characters = await CharacterService.listCharacters(db)

            const character = await CharacterService.getNPCByName(db, characters[0].name)
            expect(character).toBeNull()
        })
    })

    test('createNPC()', async () => {
        const testName = 'New Character'
        const db = await setupDb()

        const createdId = (await CharacterService.createNPC(db, testName)).lastID

        const res = await CharacterService.getCharacterById(db, createdId)
        expect(res?.name).toBe(testName)
        expect(res?.isNPC).toBe(true)
    })

    describe('deleteNPC()', () => {
        it('should delete NPC', async () => {
            const testName = 'New Character'
            const db = await setupDb()

            const createdId = (await CharacterService.createNPC(db, testName)).lastID

            await CharacterService.deleteNPC(db, createdId)

            const res = await CharacterService.getCharacterById(db, createdId)
            expect(res).toBeNull()
        })

        it('should not delete PC', async () => {
            const db = await setupDb()
            const testName = 'Edward Reed'

            const characters = await CharacterService.listCharacters(db)

            const edward = characters.find(ch => ch.name === testName)
            expect(edward?.isNPC).toBe(false)

            if (edward) {
                await CharacterService.deleteNPC(db, edward.id)

                const stillEdward = await CharacterService.getCharacterById(db, edward.id)
                expect(stillEdward).not.toBeNull()
            }
        })
    })
})
