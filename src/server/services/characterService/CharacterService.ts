import { Database } from 'sqlite3'
import { Character, CharacterDB } from '../../model/Character'
import { DB } from '../../db/db'

const characterMapper = (character: CharacterDB): Character => ({
    id: character.id,
    name: character.name,
    isNPC: !!character.isNPC,
})

const tableName = 'Character'

export const CharacterService = {
    listCharacters: async (db: Database) =>
        DB.all<CharacterDB>(db, `SELECT id, name, isNPC FROM ${tableName} ORDER BY isNPC, ordinal, name`, {}).then(
            rows => rows.map(characterMapper),
        ),

    listPlayerCharacters: async (db: Database) =>
        DB.all<CharacterDB>(db, `SELECT id, name, isNPC FROM ${tableName} WHERE isNPC=0 ORDER BY name`, {}).then(rows =>
            rows.map(characterMapper),
        ),

    getCharacterById: async (db: Database, id: number) =>
        DB.get<CharacterDB>(db, `SELECT id, name, isNPC FROM ${tableName} WHERE id=$id`, { $id: id }).then(chr =>
            chr ? characterMapper(chr) : null,
        ),

    getNPCByName: async (db: Database, name: string) =>
        DB.get<CharacterDB>(db, `SELECT id, name, isNPC FROM ${tableName} WHERE isNPC=1 AND lower(name)=$name`, {
            $name: name.toLocaleLowerCase(),
        }).then(chr => (chr ? characterMapper(chr) : null)),

    createNPC: async (db: Database, name: string) =>
        DB.run(db, `INSERT INTO ${tableName}(name, isNPC) VALUES($name, 1)`, { $name: name }),

    renameNPC: async (db: Database, id: number, name: string) =>
        DB.run(db, `UPDATE ${tableName} SET name=$name WHERE id=$id AND isNPC=1`, { $id: id, $name: name }),

    deleteNPC: async (db: Database, id: number) =>
        DB.run(db, `DELETE FROM ${tableName} WHERE id=$id AND isNPC=1`, { $id: id }),
}
