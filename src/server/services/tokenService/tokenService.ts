import jwt from 'jsonwebtoken'
import { googleClientId } from '../../../utils/googleAuth'
const { OAuth2Client } = require('google-auth-library')

const client = new OAuth2Client(googleClientId)

export interface PlayerToken {
    readonly runId: number
    readonly characterId: number
}

export interface AdminToken {
    readonly email: string
}

export interface DownloadToken {
    readonly directoryId: string
    readonly fileName: string
}

export const TokenService = {
    /**
     * Create player token for given run id and character id
     */
    createPlayerToken: (runId: number, characterId: number) => {
        const expiresIn = parseInt(process.env.JWT_EXPIRATION)
        const token: PlayerToken = { runId, characterId }
        const expires = Math.floor(new Date().getTime() / 1000) + expiresIn
        return {
            token: jwt.sign(token, process.env.JWT_SECRET, { expiresIn }),
            expires,
        }
    },

    /**
     * Verify token
     */
    verifyToken: (token: string) => {
        return jwt.verify(token, process.env.JWT_SECRET) as PlayerToken | AdminToken
    },

    /**
     * Verify google token & return payload
     */
    verifyGoogleToken: async (googleToken: string) => {
        // Verify google token first
        const ticket = await client.verifyIdToken({
            idToken: googleToken,
            clientId: googleClientId,
        })
        return ticket.getPayload()
    },

    /**
     * Create admin token
     */
    createAdminToken: async (email: string) => {
        const expiresIn = parseInt(process.env.JWT_EXPIRATION)
        const payload: AdminToken = { email }
        const expires = Math.floor(new Date().getTime() / 1000) + expiresIn
        return {
            token: jwt.sign(payload, process.env.JWT_SECRET, { expiresIn }),
            expires,
            email,
        }
    },

    createDownloadToken: (directoryId: string, fileName: string) => {
        const expiresIn = parseInt(process.env.DOWNLOAD_JWT_EXPIRATION)
        const payload = { directoryId, fileName } as DownloadToken
        return jwt.sign(payload, process.env.DOWNLOAD_JWT_SECRET, { expiresIn })
    },

    verifyDownloadToken: (token: string) => {
        return jwt.verify(token, process.env.DOWNLOAD_JWT_SECRET) as DownloadToken
    },
}
