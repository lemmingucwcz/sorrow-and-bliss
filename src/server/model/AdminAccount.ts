export interface AdminAccountDB {
    readonly id: number
    readonly email: string
    readonly sub?: string
}

export type AdminAccount = AdminAccountDB
