export interface Letter {
    /**
     * Letter primary key
     */
    readonly id: number
    /**
     * ID of run to which the letter belongs
     */
    readonly runId: number
    /**
     * ID of the creator character (in DB it is always present, it is invisible for the players
     */
    readonly creatorId?: number
    /**
     * ID of the sender character (is left out for anonymous letters)
     */
    readonly senderId?: number
    /**
     * ID of the recipient character
     */
    readonly recipientId: number
    /**
     * Letter serial number
     */
    readonly serialNumber: number
    /**
     * When letter was send (YYYY-MM-DD string)
     */
    readonly sentDate: string
    /**
     * When letter was received (YYYY-MM-DD string)
     */
    readonly receivedDate?: string
    /**
     * Comment by the organized - usually letter summary
     */
    readonly orgComment: string
    /**
     * Number of files (pages) the letter consists of
     */
    readonly numberOfFiles: number
    /**
     * Extensions of files, separated by dots
     */
    readonly fileExtensions: string
    /**
     * Letter is end of line and won't be replied.
     */
    readonly isEndOfLine: boolean
}

export interface LetterDB {
    readonly id: number
    readonly runId: number
    readonly creatorId: number
    readonly senderId?: number
    readonly recipientId: number
    readonly serialNumber: number
    readonly sentDate: string
    readonly receivedDate?: string
    readonly orgComment?: string
    readonly fileExtensions?: string
    readonly isEndOfLine: number
    readonly sentDateTime: number
    readonly receivedDateTime?: number
}
