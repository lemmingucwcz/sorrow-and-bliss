export interface GameRun {
    readonly id: number
    readonly name: string
    readonly isActive: boolean
}

export interface GameRunDB {
    readonly id: number
    readonly name: string
    readonly isActive: number
    readonly created: number
    readonly passwords: string
    readonly directoryId: string
}

export interface RunCharacterPassword {
    // Character id
    readonly id: number
    // Character name
    readonly name: string
    // Character password
    readonly password: string
}

export type CreateGameRunRequest = Pick<GameRun, 'name'>

export type UpdateGameRunRequest = Pick<GameRun, 'name'>
