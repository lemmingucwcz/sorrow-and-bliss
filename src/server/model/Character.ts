export interface Character {
    readonly id: number
    readonly name: string
    readonly isNPC: boolean
}

export interface CharacterDB {
    readonly id: number
    readonly name: string
    readonly isNPC: number
}
