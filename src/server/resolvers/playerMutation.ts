import { IResolvers } from '@graphql-tools/utils'
import { PlayerResolverContext } from './context'
import { PlayerLetterService } from '../services/letterService/PlayerLetterService'
import { convertId, getOrCreateCharacterId } from './utils'
import { MockFileService } from '../services/fileService/MockFileService'
import { PlayerCreateLetterRequest, PlayerUpdateLetterRequest } from 'src/graphql/__generated__/typescript-operations'
import { GoogleDriveFileService } from '../services/fileService/GoogleDriveFileService'

interface PlayerConfirmLetterRequest {
    readonly senderId?: string
    readonly sentDate: string
    readonly receivedDate: string
    readonly serialNumber: number
}

export default {
    PlayerMutation: {
        confirmIncomingLetter: async (
            _: unknown,
            { request }: { request: PlayerConfirmLetterRequest },
            context: PlayerResolverContext,
        ) => {
            const res = await PlayerLetterService.playerConfirmIncomingLetter(context.db, context.playerToken, {
                ...request,
                senderId: convertId(request.senderId),
            })
            return res.changes
        },

        createLetter: async (
            _: unknown,
            { request }: { request: PlayerCreateLetterRequest },
            context: PlayerResolverContext,
        ) => {
            const recipientId = await getOrCreateCharacterId(
                context.db,
                request.recipientId,
                request.newRecipientName || '',
            )
            const res = await PlayerLetterService.playerCreateLetter(context.db, context.playerToken, {
                ...request,
                recipientId,
            })
            return res.lastID
        },

        updateLetter: async (
            _: unknown,
            { request }: { request: PlayerUpdateLetterRequest },
            context: PlayerResolverContext,
        ) => {
            const recipientId = await getOrCreateCharacterId(
                context.db,
                request.recipientId,
                request.newRecipientName || '',
            )
            const res = await PlayerLetterService.playerUpdateLetter(
                context.db,
                GoogleDriveFileService,
                context.playerToken,
                {
                    ...request,
                    id: parseInt(request.id),
                    recipientId,
                    fileExtensions: request.fileExtensions ?? undefined,
                },
            )
            return res.changes
        },

        deleteLetterFiles: async (_: unknown, args: { id: string }, context: PlayerResolverContext) =>
            PlayerLetterService.playerDeleteLetterFiles(
                context.db,
                MockFileService,
                context.playerToken,
                parseInt(args.id),
            ),

        endLetterLine: async (_: unknown, args: { id: string }, context: PlayerResolverContext) =>
            (await PlayerLetterService.playerEndLetterLine(context.db, context.playerToken, parseInt(args.id))).changes,
    },
} as IResolvers
