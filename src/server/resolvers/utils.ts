import { Database } from 'sqlite3'
import { CharacterService } from '../services/characterService/CharacterService'

export const convertId = (id: string | undefined) => (id ? parseInt(id) : undefined)

/**
 * Get parsed recipient id or get NPC with newRecipientName or create new recipient and return its id
 */
export const getOrCreateCharacterId = async (
    db: Database,
    recipientId: string | undefined | null,
    newRecipientName: string,
) => {
    if (recipientId) {
        // We have id
        return parseInt(recipientId)
    }

    // Try to find existing NPC by name
    const character = await CharacterService.getNPCByName(db, newRecipientName)
    if (character) {
        // Found
        return character.id
    }

    // Create recipient
    return (await CharacterService.createNPC(db, newRecipientName)).lastID
}
