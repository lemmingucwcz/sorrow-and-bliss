import { IResolvers } from '@graphql-tools/utils'
import { ResolverContext } from './context'
import { GameRunService } from '../services/gameRunService/GameRunService'
import { CharacterService } from '../services/characterService/CharacterService'
import { OrgLetterService } from '../services/letterService/OrgLetterService'
import { AdminAllLettersFilter } from '../../graphql/__generated__/typescript-operations'
import { AdminAccountService } from '../services/adminAccountService/AdminAccountService'

interface IdArg {
    readonly id: string
}

interface RunIdArg {
    readonly runId: string
}

interface RunIdNpcArg {
    readonly runId: string
    readonly forNpc?: boolean
}

interface AllLettersArg {
    readonly runId: string
    readonly filter: AdminAllLettersFilter
}

interface RunIdDaysArg {
    readonly runId: string
    readonly daysLimit: number
}

export default {
    AdminQuery: {
        runs: (_: unknown, __: unknown, context: ResolverContext) => GameRunService.listRuns(context.db),
        runById: (_: unknown, args: IdArg, context: ResolverContext) =>
            GameRunService.getRunById(context.db, parseInt(args.id)),
        runPasswords: (_: unknown, args: IdArg, context: ResolverContext) =>
            GameRunService.getRunPasswords(context.db, parseInt(args.id)),

        characters: (_: unknown, __: unknown, context: ResolverContext) => CharacterService.listCharacters(context.db),
        characterLetterCount: (_: unknown, { characterId }: { characterId: string }, context: ResolverContext) =>
            OrgLetterService.orgCharacterLetterCount(context.db, parseInt(characterId)),

        allLetters: (_: unknown, args: AllLettersArg, context: ResolverContext) =>
            OrgLetterService.orgListAllLetters(context.db, parseInt(args.runId), args.filter),
        letterStats: (_: unknown, { runId }: RunIdArg, context: ResolverContext) =>
            OrgLetterService.orgStats(context.db, parseInt(runId)),
        incomingLetters: (_: unknown, { runId, forNpc }: RunIdNpcArg, context: ResolverContext) =>
            OrgLetterService.orgListIncomingLetters(context.db, parseInt(runId), !!forNpc),
        pendingLetters: (_: unknown, { runId, forNpc }: RunIdNpcArg, context: ResolverContext) =>
            OrgLetterService.orgListPendingLetters(context.db, parseInt(runId), !!forNpc),
        letterPagesDownloadTokens: (
            _: unknown,
            { runId, letterId }: { runId: string; letterId: string },
            context: ResolverContext,
        ) => OrgLetterService.orgGetTokensForLetterFiles(context.db, parseInt(runId), parseInt(letterId)),
        nextLetterSerial: async (
            _: unknown,
            { runId, senderId, recipientId }: { runId: string; senderId: string; recipientId: string },
            context: ResolverContext,
        ) =>
            OrgLetterService.orgGetNextLetterSerial(
                context.db,
                parseInt(runId),
                parseInt(senderId),
                parseInt(recipientId),
            ),

        pcLongUnconfirmedLetters: (_: unknown, { runId, daysLimit }: RunIdDaysArg, context: ResolverContext) =>
            OrgLetterService.orgListPCLongUnconfirmedLetters(context.db, parseInt(runId), daysLimit),
        pcLongPendingLetters: (_: unknown, { runId, daysLimit }: RunIdDaysArg, context: ResolverContext) =>
            OrgLetterService.orgListPCLongPendingLetters(context.db, parseInt(runId), daysLimit),

        adminAccounts: (_: unknown, __: unknown, context: ResolverContext) =>
            AdminAccountService.listAdminAccounts(context.db),
    },
} as IResolvers
