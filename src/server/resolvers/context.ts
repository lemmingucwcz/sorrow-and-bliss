import { Database } from 'sqlite3'
import { PlayerToken } from '../services/tokenService/tokenService'

/**
 * General context shape
 */
export interface ResolverContext {
    db: Database
    adminEmail?: string
    playerToken?: PlayerToken
}

/**
 * Context in the player subtree is guaranteed to contain playerToken
 */
export interface PlayerResolverContext {
    readonly db: Database
    readonly playerToken: PlayerToken
}
