import { CharacterService } from '../services/characterService/CharacterService'
import { ResolverContext } from './context'
import { IResolvers } from '@graphql-tools/utils'
import { GameRunService } from '../services/gameRunService/GameRunService'
import { TokenService } from '../services/tokenService/tokenService'
import { AdminAccountService } from '../services/adminAccountService/AdminAccountService'

interface PlayerTokenArgs {
    readonly runId: string
    readonly characterId: string
    readonly password: string
}

export default {
    PublicQuery: {
        playerCharacters: (_: unknown, __: unknown, context: ResolverContext) =>
            CharacterService.listPlayerCharacters(context.db),
        activeRuns: (_: unknown, __: unknown, context: ResolverContext) => GameRunService.listActiveRuns(context.db),
        playerToken: async (_: unknown, args: PlayerTokenArgs, context: ResolverContext) => {
            const runId = parseInt(args.runId)
            const characterId = parseInt(args.characterId)
            if (await GameRunService.checkRunPassword(context.db, { runId, characterId, password: args.password })) {
                // Login OK - return key
                return TokenService.createPlayerToken(runId, characterId)
            }
        },
        adminToken: async (_: unknown, { googleToken }: { googleToken: string }, context: ResolverContext) => {
            // Verify google token first
            const payload = await TokenService.verifyGoogleToken(googleToken)
            if (!payload) {
                console.log('Google token did not pass verification')
                return null
            }

            // Check admin email is OK
            if (!(await AdminAccountService.isEmailAdmin(context.db, payload.email, payload.sub))) {
                // Admin verification failed
                console.log('Admin verification failed')
                return null
            }

            // Create token
            return TokenService.createAdminToken(payload.email)
        },
    },
} as IResolvers
