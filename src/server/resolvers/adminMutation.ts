import { IResolvers } from '@graphql-tools/utils'
import { ResolverContext } from './context'
import { GameRunService } from '../services/gameRunService/GameRunService'
import { RunResult } from 'sqlite3'
import { CharacterService } from '../services/characterService/CharacterService'
import { OrgLetterService } from '../services/letterService/OrgLetterService'
import { MockFileService } from '../services/fileService/MockFileService'
import {
    AdminConfirmLetterRequest,
    AdminCreateLetterRequest,
    AdminUpdateLetterRequest,
} from '../../graphql/__generated__/typescript-operations'
import { getOrCreateCharacterId } from './utils'
import { GoogleDriveFileService } from '../services/fileService/GoogleDriveFileService'
import { AdminAccountService } from '../services/adminAccountService/AdminAccountService'

interface IdArg {
    readonly id: string
}

interface LetterRunArg {
    readonly letterId: string
    readonly runId: string
}

const lastIdMapper = (result: RunResult) => result.lastID
const changesMapper = (result: { changes: number }) => result.changes

export default {
    AdminMutation: {
        createRun: (_: unknown, { name }: { name: string }, context: ResolverContext) =>
            GameRunService.createRun(context.db, GoogleDriveFileService, { name }).then(lastIdMapper),
        updateRun: async (_: unknown, { id, name }: { id: number; name: string }, context: ResolverContext) =>
            GameRunService.updateRun(context.db, GoogleDriveFileService, id, { name }).then(changesMapper),
        activateRun: async (_: unknown, args: IdArg, context: ResolverContext) =>
            GameRunService.activateRun(context.db, parseInt(args.id)).then(changesMapper),
        deactivateRun: async (_: unknown, args: IdArg, context: ResolverContext) =>
            GameRunService.deactivateRun(context.db, parseInt(args.id)).then(changesMapper),
        deleteRun: async (_: unknown, args: IdArg, context: ResolverContext) =>
            GameRunService.deleteRun(context.db, GoogleDriveFileService, parseInt(args.id)).then(changesMapper),
        createNPC: async (_: unknown, { name }: { name: string }, context: ResolverContext) =>
            CharacterService.createNPC(context.db, name).then(lastIdMapper),
        renameNPC: async (_: unknown, { id, name }: { id: string; name: string }, context: ResolverContext) =>
            CharacterService.renameNPC(context.db, parseInt(id), name).then(changesMapper),
        deleteNPC: async (
            _: unknown,
            { id, replaceByNpc }: { id: string; replaceByNpc: string },
            context: ResolverContext,
        ) => OrgLetterService.orgUnifyCharacters(context.db, parseInt(id), parseInt(replaceByNpc)).then(changesMapper),

        deleteLetter: async (_: unknown, { letterId, runId }: LetterRunArg, context: ResolverContext) =>
            (await OrgLetterService.orgDeleteLetter(context.db, MockFileService, parseInt(runId), parseInt(letterId)))
                .changes,

        confirmLetter: async (
            _: unknown,
            { letterId, runId, request }: LetterRunArg & { request: AdminConfirmLetterRequest },
            context: ResolverContext,
        ) =>
            (
                await OrgLetterService.orgConfirmIncomingLetter(
                    context.db,
                    parseInt(runId),
                    parseInt(letterId),
                    request.receivedDate,
                    request.orgComment,
                )
            ).changes,

        endLetterLine: async (_: unknown, { runId, letterId }: LetterRunArg, context: ResolverContext) =>
            (await OrgLetterService.orgMarkEndOfLine(context.db, parseInt(runId), parseInt(letterId))).changes,

        createLetter: async (
            _: unknown,
            { runId, request }: { runId: number; request: AdminCreateLetterRequest },
            context: ResolverContext,
        ) => {
            const creatorId = await getOrCreateCharacterId(context.db, request.creatorId, request.newCreatorName || '')
            const recipientId = await getOrCreateCharacterId(
                context.db,
                request.recipientId,
                request.newRecipientName || '',
            )

            return (
                await OrgLetterService.orgCreateLetter(context.db, runId, {
                    ...request,
                    creatorId,
                    recipientId,
                    orgComment: request.orgComment || undefined,
                })
            ).lastID
        },

        updateLetter: async (
            _: unknown,
            { runId, request }: { runId: number; request: AdminUpdateLetterRequest },
            context: ResolverContext,
        ) => {
            let creatorId: number | undefined
            if (request.creatorId) {
                creatorId = parseInt(request.creatorId)
            } else if (request.newCreatorName) {
                creatorId = await getOrCreateCharacterId(context.db, undefined, request.newCreatorName)
            }

            let recipientId: number | undefined
            if (request.recipientId) {
                recipientId = parseInt(request.recipientId)
            } else if (request.newRecipientName) {
                recipientId = await getOrCreateCharacterId(context.db, undefined, request.newRecipientName)
            }

            return (
                await OrgLetterService.orgUpdateLetter(context.db, GoogleDriveFileService, runId, {
                    letterId: parseInt(request.letterId),
                    creatorId,
                    recipientId,
                    isAnonymous: request.isAnonymous ?? undefined,
                    serialNumber: request.serialNumber ?? undefined,
                    isEndOfLine: request.isEndOfLine ?? undefined,
                    sentDate: request.sentDate ?? undefined,
                    receivedDate: request.receivedDate ?? undefined,
                    orgComment: request.orgComment ?? undefined,
                    fileExtensions: request.fileExtensions ?? undefined,
                })
            ).lastID
        },

        addAdminAccount: async (_: unknown, { email }: { email: string }, context: ResolverContext) =>
            AdminAccountService.addAdminAccount(context.db, email).then(lastIdMapper),

        deleteAdminAccount: async (_: unknown, { id }: IdArg, context: ResolverContext) =>
            AdminAccountService.deleteAdminAccount(context.db, parseInt(id)).then(changesMapper),
    },
} as IResolvers
