import { IResolvers } from '@graphql-tools/utils'
import { PlayerResolverContext, ResolverContext } from './context'
import { CharacterService } from '../services/characterService/CharacterService'
import { PlayerLetterService } from '../services/letterService/PlayerLetterService'
import { convertId } from './utils'
import { PlayerToken } from '../services/tokenService/tokenService'
import { GameRunService } from '../services/gameRunService/GameRunService'

export default {
    PlayerQuery: {
        characters: (_: unknown, __: unknown, context: PlayerResolverContext) =>
            CharacterService.listCharacters(context.db),

        stats: (_: unknown, __: unknown, context: PlayerResolverContext) =>
            PlayerLetterService.playerStats(context.db, context.playerToken),
        pendingLetters: (_: unknown, __: unknown, context: PlayerResolverContext) =>
            PlayerLetterService.playerListPendingLetters(context.db, context.playerToken),
        outgoingLetters: (_: unknown, { recipientId }: { recipientId?: string }, context: PlayerResolverContext) =>
            PlayerLetterService.playerListOutgoingLetters(context.db, context.playerToken, convertId(recipientId)),
        letterPagesDownloadTokens: (_: unknown, { letterId }: { letterId: string }, context: ResolverContext) =>
            PlayerLetterService.playerGetTokensForLetterFiles(
                context.db,
                context.playerToken as PlayerToken,
                parseInt(letterId),
            ),
        nextLetterSerial: async (
            _: unknown,
            { recipientId }: { recipientId: string },
            context: PlayerResolverContext,
        ) => PlayerLetterService.playerGetNextLetterSerial(context.db, context.playerToken, parseInt(recipientId)),

        isRunActive: async (_: unknown, __: unknown, context: PlayerResolverContext) =>
            GameRunService.getRunById(context.db, context.playerToken.runId || 0).then(run => Boolean(run?.isActive)),
    },
} as IResolvers
