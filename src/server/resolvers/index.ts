import AdminQueryResolvers from './adminQuery'
import AdminMutationResolvers from './adminMutation'
import PlayerQueryResolvers from './playerQuery'
import PlayerMutationResolvers from './playerMutation'
import PublicResolvers from './public'
import { ResolverContext } from './context'
import { AdminAccountService } from '../services/adminAccountService/AdminAccountService'
import { ExecutionContext } from 'graphql/execution/execute'

const fmt2 = (n: number) => (n < 10 ? `0${n}` : `${n}`)

const logExecution = (info: ExecutionContext, auth: string) => {
    const now = new Date()
    const time = `${now.getFullYear()}-${fmt2(now.getMonth() + 1)}-${fmt2(now.getDate())} ${fmt2(
        now.getHours(),
    )}:${fmt2(now.getMinutes())}:${fmt2(now.getSeconds())}`
    const name = info.operation.name?.value
    console.log(`${time} | ${auth} | ${name} | ${JSON.stringify(info.variableValues)}`)
}

const resolversRoot = [
    {
        Query: {
            public: async () => {
                return {}
            },
            admin: async (_: unknown, __: unknown, context: ResolverContext) => {
                if (!context.adminEmail) {
                    throw new Error('Admin required')
                }
                if (!(await AdminAccountService.isEmailAdmin(context.db, context.adminEmail))) {
                    throw new Error('Account is no longer an admin')
                }
                return {}
            },
            player: async (_: unknown, __: unknown, context: ResolverContext) => {
                if (!context.playerToken) {
                    throw new Error('Auth required')
                }
                return {}
            },
        },
        Mutation: {
            admin: async (_: unknown, __: unknown, context: ResolverContext, info: ExecutionContext) => {
                if (!context.adminEmail) {
                    throw new Error('Admin required')
                }
                if (!(await AdminAccountService.isEmailAdmin(context.db, context.adminEmail))) {
                    throw new Error('Account is no longer an admin')
                }
                logExecution(info, context.adminEmail)
                return {}
            },
            player: async (_: unknown, __: unknown, context: ResolverContext, info: ExecutionContext) => {
                if (!context.playerToken) {
                    throw new Error('Auth required')
                }
                logExecution(info, `${context.playerToken?.runId}:${context.playerToken?.characterId}`)
                return {}
            },
        },
    },
    AdminQueryResolvers,
    AdminMutationResolvers,
    PublicResolvers,
    PlayerQueryResolvers,
    PlayerMutationResolvers,
]

export default resolversRoot
