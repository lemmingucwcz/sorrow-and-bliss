export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] }
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> }
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> }
export type AddAdminAccountMutationVariables = Exact<{
    email: Scalars['String']
}>

export type AddAdminAccountMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; addAdminAccount: string }
}

export type AdminAccountsQueryVariables = Exact<{ [key: string]: never }>

export type AdminAccountsQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        adminAccounts: Array<{ __typename?: 'AdminAccount'; id: string; email: string }>
    }
}

export type DeleteAdminAccountMutationVariables = Exact<{
    id: Scalars['ID']
}>

export type DeleteAdminAccountMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; deleteAdminAccount: number }
}

export type AdminConfirmLetterMutationVariables = Exact<{
    runId: Scalars['ID']
    letterId: Scalars['ID']
    request: AdminConfirmLetterRequest
}>

export type AdminConfirmLetterMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; confirmLetter: number }
}

export type AdminDashboardQueryVariables = Exact<{
    runId: Scalars['ID']
    daysLimitPending: Scalars['Int']
    daysLimitUnconfirmed: Scalars['Int']
}>

export type AdminDashboardQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        letterStats: {
            __typename?: 'AdminLetterStats'
            lettersInTransit: number
            lettersTotal: number
            lettersWaitingForResponse: number
        }
        pcLongPendingLetters: Array<{
            __typename?: 'Letter'
            id: string
            creatorId?: string | null | undefined
            senderId?: string | null | undefined
            recipientId: string
            serialNumber: number
            sentDate: string
            receivedDate?: string | null | undefined
            numberOfFiles: number
        }>
        pcLongUnconfirmedLetters: Array<{
            __typename?: 'Letter'
            id: string
            creatorId?: string | null | undefined
            senderId?: string | null | undefined
            recipientId: string
            serialNumber: number
            sentDate: string
            receivedDate?: string | null | undefined
            numberOfFiles: number
        }>
        incomingLetters: Array<{
            __typename?: 'Letter'
            id: string
            creatorId?: string | null | undefined
            senderId?: string | null | undefined
            recipientId: string
            serialNumber: number
            sentDate: string
            orgComment?: string | null | undefined
        }>
        pendingLetters: Array<{
            __typename?: 'Letter'
            id: string
            creatorId?: string | null | undefined
            senderId?: string | null | undefined
            recipientId: string
            serialNumber: number
            sentDate: string
            receivedDate?: string | null | undefined
        }>
    }
}

export type AdminEndLetterLineMutationVariables = Exact<{
    runId: Scalars['ID']
    letterId: Scalars['ID']
}>

export type AdminEndLetterLineMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; endLetterLine: number }
}

export type AdminCreateLetterMutationVariables = Exact<{
    runId: Scalars['ID']
    request: AdminCreateLetterRequest
}>

export type AdminCreateLetterMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; createLetter?: string | null | undefined }
}

export type AdminNextSerialHintQueryVariables = Exact<{
    runId: Scalars['ID']
    senderId: Scalars['ID']
    recipientId: Scalars['ID']
}>

export type AdminNextSerialHintQuery = {
    __typename?: 'Query'
    admin: { __typename?: 'AdminQuery'; nextLetterSerial: number }
}

export type AdminUpdateLetterMutationVariables = Exact<{
    runId: Scalars['ID']
    request: AdminUpdateLetterRequest
}>

export type AdminUpdateLetterMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; updateLetter?: string | null | undefined }
}

export type AdminDeleteLetterMutationVariables = Exact<{
    runId: Scalars['ID']
    letterId: Scalars['ID']
}>

export type AdminDeleteLetterMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; deleteLetter: number }
}

export type AdminLettersQueryVariables = Exact<{
    runId: Scalars['ID']
    filter: AdminAllLettersFilter
}>

export type AdminLettersQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        allLetters: Array<{
            __typename?: 'Letter'
            id: string
            creatorId?: string | null | undefined
            senderId?: string | null | undefined
            recipientId: string
            sentDate: string
            receivedDate?: string | null | undefined
            orgComment?: string | null | undefined
            serialNumber: number
            numberOfFiles: number
            isEndOfLine: boolean
        }>
    }
}

export type CharacterLetterCountQueryVariables = Exact<{
    characterId: Scalars['ID']
}>

export type CharacterLetterCountQuery = {
    __typename?: 'Query'
    admin: { __typename?: 'AdminQuery'; characterLetterCount: number }
}

export type DeleteNpcMutationVariables = Exact<{
    id: Scalars['ID']
    replaceByNpc: Scalars['ID']
}>

export type DeleteNpcMutation = { __typename?: 'Mutation'; admin: { __typename?: 'AdminMutation'; deleteNPC: number } }

export type RenameNpcMutationVariables = Exact<{
    id: Scalars['ID']
    name: Scalars['String']
}>

export type RenameNpcMutation = { __typename?: 'Mutation'; admin: { __typename?: 'AdminMutation'; renameNPC: number } }

export type ActivateRunMutationVariables = Exact<{
    id: Scalars['ID']
}>

export type ActivateRunMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; activateRun: number }
}

export type CreateRunMutationVariables = Exact<{
    name: Scalars['String']
}>

export type CreateRunMutation = { __typename?: 'Mutation'; admin: { __typename?: 'AdminMutation'; createRun: string } }

export type DeactivateRunMutationVariables = Exact<{
    id: Scalars['ID']
}>

export type DeactivateRunMutation = {
    __typename?: 'Mutation'
    admin: { __typename?: 'AdminMutation'; deactivateRun: number }
}

export type DeleteRunMutationVariables = Exact<{
    id: Scalars['ID']
}>

export type DeleteRunMutation = { __typename?: 'Mutation'; admin: { __typename?: 'AdminMutation'; deleteRun: number } }

export type RunPasswordsQueryVariables = Exact<{
    id: Scalars['ID']
}>

export type RunPasswordsQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        runPasswords: Array<{ __typename?: 'RunCharacterPassword'; id: string; name: string; password: string }>
    }
}

export type UpdateRunMutationVariables = Exact<{
    id: Scalars['ID']
    name: Scalars['String']
}>

export type UpdateRunMutation = { __typename?: 'Mutation'; admin: { __typename?: 'AdminMutation'; updateRun: number } }

export type AdminLoginQueryVariables = Exact<{
    googleToken: Scalars['String']
}>

export type AdminLoginQuery = {
    __typename?: 'Query'
    public: {
        __typename?: 'PublicQuery'
        adminToken?: { __typename?: 'AdminToken'; email: string; expires: number; token: string } | null | undefined
    }
}

export type LoginFormQueryQueryVariables = Exact<{ [key: string]: never }>

export type LoginFormQueryQuery = {
    __typename?: 'Query'
    public: {
        __typename?: 'PublicQuery'
        activeRuns: Array<{ __typename?: 'GameRun'; id: string; name: string }>
        playerCharacters: Array<{ __typename?: 'Character'; id: string; name: string }>
    }
}

export type LoginQueryQueryVariables = Exact<{
    runId: Scalars['ID']
    characterId: Scalars['ID']
    password: Scalars['String']
}>

export type LoginQueryQuery = {
    __typename?: 'Query'
    public: {
        __typename?: 'PublicQuery'
        playerToken?: { __typename?: 'PlayerToken'; expires: number; token: string } | null | undefined
    }
}

export type PlayerEndLetterLineMutationVariables = Exact<{
    letterId: Scalars['ID']
}>

export type PlayerEndLetterLineMutation = {
    __typename?: 'Mutation'
    player: { __typename?: 'PlayerMutation'; endLetterLine: number }
}

export type PlayerInfoQueryQueryVariables = Exact<{ [key: string]: never }>

export type PlayerInfoQueryQuery = {
    __typename?: 'Query'
    player: {
        __typename?: 'PlayerQuery'
        stats: { __typename?: 'PlayerStats'; lettersInTransit: number }
        pendingLetters: Array<{
            __typename?: 'Letter'
            id: string
            senderId?: string | null | undefined
            serialNumber: number
            receivedDate?: string | null | undefined
            sentDate: string
        }>
    }
}

export type PlayerCreateLetterMutationVariables = Exact<{
    sentDate: Scalars['String']
    recipientId?: Maybe<Scalars['ID']>
    serialNumber: Scalars['Int']
    fileExtensions: Scalars['String']
    isAnonymous: Scalars['Boolean']
    newRecipientName?: Maybe<Scalars['String']>
}>

export type PlayerCreateLetterMutation = {
    __typename?: 'Mutation'
    player: { __typename?: 'PlayerMutation'; createLetter: string }
}

export type PlayerNextSerialHintQueryVariables = Exact<{
    recipientId: Scalars['ID']
}>

export type PlayerNextSerialHintQuery = {
    __typename?: 'Query'
    player: { __typename?: 'PlayerQuery'; nextLetterSerial: number }
}

export type PlayerUpdateLetterMutationVariables = Exact<{
    request: PlayerUpdateLetterRequest
}>

export type PlayerUpdateLetterMutation = {
    __typename?: 'Mutation'
    player: { __typename?: 'PlayerMutation'; updateLetter: number }
}

export type PlayerConfirmIncomingLetterMutationMutationVariables = Exact<{
    request: PlayerConfirmLetterRequest
}>

export type PlayerConfirmIncomingLetterMutationMutation = {
    __typename?: 'Mutation'
    player: { __typename?: 'PlayerMutation'; confirmIncomingLetter: number }
}

export type PlayerSentLettersQueryVariables = Exact<{
    recipientId?: Maybe<Scalars['ID']>
}>

export type PlayerSentLettersQuery = {
    __typename?: 'Query'
    player: {
        __typename?: 'PlayerQuery'
        outgoingLetters: Array<{
            __typename?: 'Letter'
            id: string
            isEndOfLine: boolean
            numberOfFiles: number
            recipientId: string
            senderId?: string | null | undefined
            sentDate: string
            serialNumber: number
        }>
    }
}

export type IsRunActiveQueryVariables = Exact<{ [key: string]: never }>

export type IsRunActiveQuery = {
    __typename?: 'Query'
    player: { __typename?: 'PlayerQuery'; isRunActive?: boolean | null | undefined }
}

export type AdminDownloadTokensQueryVariables = Exact<{
    runId: Scalars['ID']
    letterId: Scalars['ID']
}>

export type AdminDownloadTokensQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        letterPagesDownloadTokens: Array<{ __typename?: 'DownloadToken'; token: string; fileName: string }>
    }
}

export type AdminRunsQueryVariables = Exact<{ [key: string]: never }>

export type AdminRunsQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        runs: Array<{ __typename?: 'GameRun'; id: string; isActive: boolean; name: string }>
    }
}

export type CharactersAdminQueryVariables = Exact<{ [key: string]: never }>

export type CharactersAdminQuery = {
    __typename?: 'Query'
    admin: {
        __typename?: 'AdminQuery'
        characters: Array<{ __typename?: 'Character'; id: string; isNPC: boolean; name: string }>
    }
}

export type CharactersPlayerQueryVariables = Exact<{ [key: string]: never }>

export type CharactersPlayerQuery = {
    __typename?: 'Query'
    player: {
        __typename?: 'PlayerQuery'
        characters: Array<{ __typename?: 'Character'; id: string; name: string; isNPC: boolean }>
    }
}

export type PlayerDownloadTokensQueryVariables = Exact<{
    letterId: Scalars['ID']
}>

export type PlayerDownloadTokensQuery = {
    __typename?: 'Query'
    player: {
        __typename?: 'PlayerQuery'
        letterPagesDownloadTokens: Array<{ __typename?: 'DownloadToken'; token: string; fileName: string }>
    }
}

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string
    String: string
    Boolean: boolean
    Int: number
    Float: number
}

export type AdminAccount = {
    __typename?: 'AdminAccount'
    email: Scalars['String']
    id: Scalars['ID']
}

export type AdminAllLettersFilter = {
    character1Id?: InputMaybe<Scalars['ID']>
    character2Id?: InputMaybe<Scalars['ID']>
    letterDirection?: InputMaybe<LetterDirection>
    letterState?: InputMaybe<LetterState>
    limit?: InputMaybe<Scalars['Int']>
    offset?: InputMaybe<Scalars['Int']>
}

export type AdminConfirmLetterRequest = {
    orgComment?: InputMaybe<Scalars['String']>
    receivedDate: Scalars['String']
}

export type AdminCreateLetterRequest = {
    creatorId?: InputMaybe<Scalars['ID']>
    fileExtensions: Scalars['String']
    isAnonymous: Scalars['Boolean']
    newCreatorName?: InputMaybe<Scalars['String']>
    newRecipientName?: InputMaybe<Scalars['String']>
    orgComment?: InputMaybe<Scalars['String']>
    recipientId?: InputMaybe<Scalars['ID']>
    sentDate: Scalars['String']
    serialNumber: Scalars['Int']
}

export type AdminLetterStats = {
    __typename?: 'AdminLetterStats'
    lettersInTransit: Scalars['Int']
    lettersTotal: Scalars['Int']
    lettersWaitingForResponse: Scalars['Int']
}

export type AdminMutation = {
    __typename?: 'AdminMutation'
    activateRun: Scalars['Int']
    addAdminAccount: Scalars['ID']
    confirmLetter: Scalars['Int']
    createLetter?: Maybe<Scalars['ID']>
    createNPC: Scalars['ID']
    createRun: Scalars['ID']
    deactivateRun: Scalars['Int']
    deleteAdminAccount: Scalars['Int']
    deleteLetter: Scalars['Int']
    deleteNPC: Scalars['Int']
    deleteRun: Scalars['Int']
    endLetterLine: Scalars['Int']
    renameNPC: Scalars['Int']
    updateLetter?: Maybe<Scalars['ID']>
    updateRun: Scalars['Int']
}

export type AdminMutationActivateRunArgs = {
    id: Scalars['ID']
}

export type AdminMutationAddAdminAccountArgs = {
    email: Scalars['String']
}

export type AdminMutationConfirmLetterArgs = {
    letterId: Scalars['ID']
    request: AdminConfirmLetterRequest
    runId: Scalars['ID']
}

export type AdminMutationCreateLetterArgs = {
    request: AdminCreateLetterRequest
    runId: Scalars['ID']
}

export type AdminMutationCreateNpcArgs = {
    name: Scalars['String']
}

export type AdminMutationCreateRunArgs = {
    name: Scalars['String']
}

export type AdminMutationDeactivateRunArgs = {
    id: Scalars['ID']
}

export type AdminMutationDeleteAdminAccountArgs = {
    id: Scalars['ID']
}

export type AdminMutationDeleteLetterArgs = {
    letterId: Scalars['ID']
    runId: Scalars['ID']
}

export type AdminMutationDeleteNpcArgs = {
    id: Scalars['ID']
    replaceByNpc: Scalars['ID']
}

export type AdminMutationDeleteRunArgs = {
    id: Scalars['ID']
}

export type AdminMutationEndLetterLineArgs = {
    letterId: Scalars['ID']
    runId: Scalars['ID']
}

export type AdminMutationRenameNpcArgs = {
    id: Scalars['ID']
    name?: InputMaybe<Scalars['String']>
}

export type AdminMutationUpdateLetterArgs = {
    request: AdminUpdateLetterRequest
    runId: Scalars['ID']
}

export type AdminMutationUpdateRunArgs = {
    id: Scalars['ID']
    name: Scalars['String']
}

export type AdminQuery = {
    __typename?: 'AdminQuery'
    adminAccounts: Array<AdminAccount>
    allLetters: Array<Letter>
    characterLetterCount: Scalars['Int']
    characters: Array<Character>
    incomingLetters: Array<Letter>
    letterPagesDownloadTokens: Array<DownloadToken>
    letterStats: AdminLetterStats
    nextLetterSerial: Scalars['Int']
    pcLongPendingLetters: Array<Letter>
    pcLongUnconfirmedLetters: Array<Letter>
    pendingLetters: Array<Letter>
    runById?: Maybe<GameRun>
    runPasswords: Array<RunCharacterPassword>
    runs: Array<GameRun>
}

export type AdminQueryAllLettersArgs = {
    filter: AdminAllLettersFilter
    runId: Scalars['ID']
}

export type AdminQueryCharacterLetterCountArgs = {
    characterId: Scalars['ID']
}

export type AdminQueryIncomingLettersArgs = {
    forNpc?: InputMaybe<Scalars['Boolean']>
    runId: Scalars['ID']
}

export type AdminQueryLetterPagesDownloadTokensArgs = {
    letterId: Scalars['ID']
    runId: Scalars['ID']
}

export type AdminQueryLetterStatsArgs = {
    runId: Scalars['ID']
}

export type AdminQueryNextLetterSerialArgs = {
    recipientId: Scalars['ID']
    runId: Scalars['ID']
    senderId: Scalars['ID']
}

export type AdminQueryPcLongPendingLettersArgs = {
    daysLimit: Scalars['Int']
    runId: Scalars['ID']
}

export type AdminQueryPcLongUnconfirmedLettersArgs = {
    daysLimit: Scalars['Int']
    runId: Scalars['ID']
}

export type AdminQueryPendingLettersArgs = {
    forNpc?: InputMaybe<Scalars['Boolean']>
    runId: Scalars['ID']
}

export type AdminQueryRunByIdArgs = {
    id: Scalars['ID']
}

export type AdminQueryRunPasswordsArgs = {
    id: Scalars['ID']
}

export type AdminToken = {
    __typename?: 'AdminToken'
    email: Scalars['String']
    expires: Scalars['Int']
    token: Scalars['String']
}

export type AdminUpdateLetterRequest = {
    creatorId?: InputMaybe<Scalars['ID']>
    fileExtensions?: InputMaybe<Scalars['String']>
    isAnonymous?: InputMaybe<Scalars['Boolean']>
    isEndOfLine?: InputMaybe<Scalars['Boolean']>
    letterId: Scalars['ID']
    newCreatorName?: InputMaybe<Scalars['String']>
    newRecipientName?: InputMaybe<Scalars['String']>
    orgComment?: InputMaybe<Scalars['String']>
    receivedDate?: InputMaybe<Scalars['String']>
    recipientId?: InputMaybe<Scalars['ID']>
    sentDate?: InputMaybe<Scalars['String']>
    serialNumber?: InputMaybe<Scalars['Int']>
}

export type Character = {
    __typename?: 'Character'
    id: Scalars['ID']
    isNPC: Scalars['Boolean']
    name: Scalars['String']
}

export type DownloadToken = {
    __typename?: 'DownloadToken'
    fileName: Scalars['String']
    token: Scalars['ID']
}

export type GameRun = {
    __typename?: 'GameRun'
    id: Scalars['ID']
    isActive: Scalars['Boolean']
    name: Scalars['String']
}

export type Letter = {
    __typename?: 'Letter'
    creatorId?: Maybe<Scalars['ID']>
    id: Scalars['ID']
    isEndOfLine: Scalars['Boolean']
    numberOfFiles: Scalars['Int']
    orgComment?: Maybe<Scalars['String']>
    receivedDate?: Maybe<Scalars['String']>
    recipientId: Scalars['ID']
    runId: Scalars['ID']
    senderId?: Maybe<Scalars['ID']>
    sentDate: Scalars['String']
    serialNumber: Scalars['Int']
}

export enum LetterDirection {
    Both = 'BOTH',
    C1C2 = 'C1C2',
    C2C1 = 'C2C1',
}

export enum LetterState {
    All = 'ALL',
    Unanswered = 'UNANSWERED',
    Unconfirmed = 'UNCONFIRMED',
}

export type Mutation = {
    __typename?: 'Mutation'
    admin: AdminMutation
    player: PlayerMutation
}

export type PlayerConfirmLetterRequest = {
    receivedDate: Scalars['String']
    senderId?: InputMaybe<Scalars['ID']>
    sentDate: Scalars['String']
    serialNumber: Scalars['Int']
}

export type PlayerCreateLetterRequest = {
    fileExtensions: Scalars['String']
    isAnonymous: Scalars['Boolean']
    newRecipientName?: InputMaybe<Scalars['String']>
    recipientId?: InputMaybe<Scalars['ID']>
    sentDate: Scalars['String']
    serialNumber: Scalars['Int']
}

export type PlayerMutation = {
    __typename?: 'PlayerMutation'
    confirmIncomingLetter: Scalars['Int']
    createLetter: Scalars['ID']
    deleteLetterFiles: Scalars['Int']
    endLetterLine: Scalars['Int']
    updateLetter: Scalars['Int']
}

export type PlayerMutationConfirmIncomingLetterArgs = {
    request: PlayerConfirmLetterRequest
}

export type PlayerMutationCreateLetterArgs = {
    request: PlayerCreateLetterRequest
}

export type PlayerMutationDeleteLetterFilesArgs = {
    id: Scalars['ID']
}

export type PlayerMutationEndLetterLineArgs = {
    id: Scalars['ID']
}

export type PlayerMutationUpdateLetterArgs = {
    request: PlayerUpdateLetterRequest
}

export type PlayerQuery = {
    __typename?: 'PlayerQuery'
    characters: Array<Character>
    isRunActive?: Maybe<Scalars['Boolean']>
    letterPagesDownloadTokens: Array<DownloadToken>
    nextLetterSerial: Scalars['Int']
    outgoingLetters: Array<Letter>
    pendingLetters: Array<Letter>
    stats: PlayerStats
}

export type PlayerQueryLetterPagesDownloadTokensArgs = {
    letterId: Scalars['ID']
}

export type PlayerQueryNextLetterSerialArgs = {
    recipientId: Scalars['ID']
}

export type PlayerQueryOutgoingLettersArgs = {
    recipientId?: InputMaybe<Scalars['ID']>
}

export type PlayerStats = {
    __typename?: 'PlayerStats'
    lettersInTransit: Scalars['Int']
}

export type PlayerToken = {
    __typename?: 'PlayerToken'
    expires: Scalars['Int']
    token: Scalars['String']
}

export type PlayerUpdateLetterRequest = {
    fileExtensions?: InputMaybe<Scalars['String']>
    id: Scalars['ID']
    isAnonymous: Scalars['Boolean']
    newRecipientName?: InputMaybe<Scalars['String']>
    recipientId?: InputMaybe<Scalars['ID']>
    sentDate: Scalars['String']
    serialNumber: Scalars['Int']
}

export type PublicQuery = {
    __typename?: 'PublicQuery'
    activeRuns: Array<GameRun>
    adminToken?: Maybe<AdminToken>
    playerCharacters: Array<Character>
    playerToken?: Maybe<PlayerToken>
}

export type PublicQueryAdminTokenArgs = {
    googleToken: Scalars['String']
}

export type PublicQueryPlayerTokenArgs = {
    characterId: Scalars['ID']
    password: Scalars['String']
    runId: Scalars['ID']
}

export type Query = {
    __typename?: 'Query'
    admin: AdminQuery
    player: PlayerQuery
    public: PublicQuery
}

export type RunCharacterPassword = {
    __typename?: 'RunCharacterPassword'
    id: Scalars['ID']
    name: Scalars['String']
    password: Scalars['String']
}
