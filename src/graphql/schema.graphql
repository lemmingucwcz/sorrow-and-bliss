type Character {
    id: ID!
    name: String!
    isNPC: Boolean!
}

type GameRun {
    id: ID!
    name: String!
    isActive: Boolean!
}

type RunCharacterPassword {
    id: ID!
    name: String!
    password: String!
}

type PlayerStats {
    lettersInTransit: Int!
}
type PlayerToken {
    token: String!
    expires: Int!
}

type AdminToken {
    token: String!
    expires: Int!
    email: String!
}

type PublicQuery {
    playerCharacters: [Character!]!
    activeRuns: [GameRun!]!
    playerToken(runId: ID!, characterId: ID!, password: String!): PlayerToken
    adminToken(googleToken: String!): AdminToken
}

type Letter {
    id: ID!
    runId: ID!
    creatorId: ID
    senderId: ID
    recipientId: ID!
    serialNumber: Int!
    sentDate: String!
    receivedDate: String
    orgComment: String
    numberOfFiles: Int!
    isEndOfLine: Boolean!
}

type DownloadToken {
    token: ID!
    fileName: String!
}

enum LetterState {
    ALL,
    UNCONFIRMED,
    UNANSWERED
}

enum LetterDirection {
    BOTH,
    C1C2,
    C2C1,
}

input AdminAllLettersFilter {
    character1Id: ID
    character2Id: ID
    letterState: LetterState
    letterDirection: LetterDirection
    offset: Int
    limit: Int
}

type AdminLetterStats {
    lettersTotal: Int!
    lettersInTransit: Int!
    lettersWaitingForResponse: Int!
}

type AdminAccount {
    id: ID!
    email: String!
}

type AdminQuery {
    # Runs
    runs: [GameRun!]!
    runById(id: ID!): GameRun
    runPasswords(id: ID!): [RunCharacterPassword!]!

    #Characters
    characters: [Character!]!
    characterLetterCount(characterId: ID!): Int!

    # Letters
    allLetters(runId: ID!, filter: AdminAllLettersFilter!): [Letter!]!
    letterStats(runId: ID!): AdminLetterStats!
    incomingLetters(runId: ID!, forNpc: Boolean): [Letter!]!
    pendingLetters(runId: ID!, forNpc: Boolean): [Letter!]!
    pcLongUnconfirmedLetters(runId: ID!, daysLimit: Int!): [Letter!]!
    pcLongPendingLetters(runId: ID!, daysLimit: Int!): [Letter!]!
    letterPagesDownloadTokens(runId: ID!, letterId: ID!): [DownloadToken!]!
    nextLetterSerial(runId: ID!, senderId: ID!, recipientId: ID!): Int!

    # Admins
    adminAccounts: [AdminAccount!]!
}

type PlayerQuery {
    #Characters
    characters: [Character!]!

    # Letters
    stats: PlayerStats!
    pendingLetters: [Letter!]!
    outgoingLetters(recipientId: ID): [Letter!]!
    letterPagesDownloadTokens(letterId: ID!): [DownloadToken!]!
    nextLetterSerial(recipientId: ID!): Int!

    # Check players run is active
    isRunActive: Boolean
}

input PlayerConfirmLetterRequest {
    senderId: ID
    sentDate: String!
    receivedDate: String!
    serialNumber: Int!
}

input PlayerCreateLetterRequest {
    recipientId: ID
    isAnonymous: Boolean!
    sentDate: String!
    serialNumber: Int!
    fileExtensions: String!
    newRecipientName: String
}
input PlayerUpdateLetterRequest {
    id: ID!
    recipientId: ID
    isAnonymous: Boolean!
    sentDate: String!
    serialNumber: Int!
    fileExtensions: String
    newRecipientName: String
}

type PlayerMutation {
    # Letters
    confirmIncomingLetter(request: PlayerConfirmLetterRequest!): Int!
    createLetter(request: PlayerCreateLetterRequest!): ID!
    updateLetter(request: PlayerUpdateLetterRequest!): Int!
    deleteLetterFiles(id: ID!): Int!
    endLetterLine(id: ID!): Int!
}

type Query {
    public: PublicQuery!
    admin: AdminQuery!
    player: PlayerQuery!
}

input AdminCreateLetterRequest {
    creatorId: ID
    newCreatorName: String
    recipientId: ID
    newRecipientName: String
    isAnonymous: Boolean!
    sentDate: String!
    fileExtensions: String!
    serialNumber: Int!
    orgComment: String
}

input AdminUpdateLetterRequest {
    letterId: ID!
    creatorId: ID
    newCreatorName: String
    recipientId: ID
    newRecipientName: String
    sentDate: String
    receivedDate: String
    isAnonymous: Boolean
    isEndOfLine: Boolean
    orgComment: String
    serialNumber: Int
    fileExtensions: String
}

input AdminConfirmLetterRequest {
    receivedDate: String!
    orgComment: String
}

type AdminMutation {
    # Runs
    createRun(name: String!): ID!
    updateRun(id: ID!, name: String!): Int!
    activateRun(id: ID!): Int!
    deactivateRun(id: ID!): Int!
    deleteRun(id: ID!): Int!

    # Characters
    createNPC(name: String!): ID!
    renameNPC(id: ID!, name: String): Int!
    deleteNPC(id: ID!, replaceByNpc: ID!): Int!

    # Admins
    addAdminAccount(email: String!): ID!
    deleteAdminAccount(id: ID!): Int!

    # Letters
    deleteLetter(runId: ID!, letterId: ID!): Int!
    confirmLetter(runId: ID!, letterId: ID!, request: AdminConfirmLetterRequest!): Int!
    endLetterLine(runId: ID!, letterId: ID!): Int!
    createLetter(runId: ID!, request: AdminCreateLetterRequest!): ID
    updateLetter(runId: ID!, request: AdminUpdateLetterRequest!): ID
}

type Mutation {
    admin: AdminMutation!
    player: PlayerMutation!
}
