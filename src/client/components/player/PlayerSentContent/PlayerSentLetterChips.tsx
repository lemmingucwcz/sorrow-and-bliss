import React from 'react'
import { usePlayerGetDownloadTokens } from '../../../hooks/usePlayerGetDownloadTokens'
import { SentLetter } from './PlayerSentLetterRow'
import { useTranslation } from 'next-i18next'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { Chip, IconButton, Paper, Tooltip } from '@mui/material'
import { Box } from '@mui/system'
import {
    Edit as EditIcon,
    Reorder as ReorderIcon,
    Send as SendIcon,
    VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material'
import { formatDate } from '../../../utils/dateUtils'
import DownloadPagesPopupMenu from '../../common/DownloadPagesPopupMenu/DownloadPagesPopupMenu'

interface Props {
    readonly letter: SentLetter
    readonly onPlayerGetTokens: ReturnType<typeof usePlayerGetDownloadTokens>
    readonly onEditLetter: () => void
}

const PlayerSentLetterChips = ({ letter, onPlayerGetTokens, onEditLetter }: Props) => {
    const { t } = useTranslation('player')
    const characters = useCharactersPlayer()

    return (
        <Paper sx={{ padding: 1, width: '100%' }}>
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Chip
                    label={characters.map[letter.recipientId || '']?.name}
                    icon={
                        !letter.senderId ? (
                            <Tooltip title={t('Letter.isAnonymous') as string}>
                                <VisibilityOffIcon fontSize="small" />
                            </Tooltip>
                        ) : undefined
                    }
                    sx={{ flex: 1, textOverflow: 'ellipsis', justifyContent: 'left', width: 0 }}
                />
            </Box>
            <Box
                sx={{
                    mt: 1,
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                    gap: 1,
                    justifyContent: 'space-between',
                }}
            >
                <Chip label={formatDate(letter.sentDate)} icon={<SendIcon />} />
                <Chip label={letter.serialNumber} icon={<ReorderIcon />} />
                <Box sx={{ flex: 1, textAlign: 'right' }}>
                    {letter.numberOfFiles > 0 ? (
                        <DownloadPagesPopupMenu onGetTokens={() => onPlayerGetTokens(letter.id)} />
                    ) : null}
                    <Tooltip title={t('OutgoingLetter.edit') as string}>
                        <IconButton onClick={onEditLetter}>
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                </Box>
            </Box>
        </Paper>
    )
}

export default PlayerSentLetterChips
