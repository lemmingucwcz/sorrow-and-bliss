import React from 'react'
import { CharacterAC } from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { useQuery } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import {
    PlayerSentLettersQuery,
    PlayerSentLettersQueryVariables,
} from 'src/graphql/__generated__/typescript-operations'
import {
    Alert,
    CircularProgress,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import PlayerSentLetterRow, { SentLetter } from './PlayerSentLetterRow'
import { usePlayerGetDownloadTokens } from '../../../hooks/usePlayerGetDownloadTokens'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { useIsMobile } from '../../../hooks/useIsMobile'
import PlayerSentLetterChips from './PlayerSentLetterChips'

const playerSentLettersQuery = require('./playerSentLettersQuery.graphql')

interface Props {
    readonly recipient?: CharacterAC
    readonly onStartEditingLetter: (letter: SentLetter) => void
}

const PlayerSentLetters = ({ recipient, onStartEditingLetter }: Props) => {
    const request = useGraphqlRequest()
    const { t } = useTranslation('player')
    const getDownloadTokens = usePlayerGetDownloadTokens()
    const { isFetching, data: letters } = useQuery([QueryKeys.playerSentLettersQuery, recipient?.id], async () => {
        const res = await request<PlayerSentLettersQuery, PlayerSentLettersQueryVariables>(playerSentLettersQuery, {
            recipientId: recipient?.id,
        })
        return res.player.outgoingLetters
    })
    const isMobile = useIsMobile()

    if (isFetching) {
        return <CircularProgress />
    }

    if (!letters || letters.length === 0) {
        return (
            <Alert severity="info">
                {t(recipient ? 'Player.noOutgoingLettersFilter' : 'Player.noOutgoingLetters')}
            </Alert>
        )
    }

    if (isMobile) {
        return (
            <>
                {letters.map(letter => (
                    <PlayerSentLetterChips
                        key={letter.id}
                        letter={letter}
                        onPlayerGetTokens={getDownloadTokens}
                        onEditLetter={() => onStartEditingLetter(letter)}
                    />
                ))}
            </>
        )
    }

    return (
        <>
            <TableContainer component={Paper} sx={{ width: 'auto' }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Letter.recipient')}</TableCell>
                            <TableCell>{t('Letter.sentDateShort')}</TableCell>
                            <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                            <TableCell>{t('Letter.isAnonymous')}</TableCell>
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {letters.map(letter => (
                            <PlayerSentLetterRow
                                key={letter.id}
                                letter={letter}
                                onEditLetter={() => onStartEditingLetter(letter)}
                                onPlayerGetTokens={getDownloadTokens}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default PlayerSentLetters
