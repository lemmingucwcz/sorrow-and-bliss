import { Chip, IconButton, TableCell, TableRow, Tooltip } from '@mui/material'
import React from 'react'
import { Letter } from '../../../../graphql/__generated__/typescript-operations'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { formatDate } from '../../../utils/dateUtils'
import {
    Edit as EditIcon,
    Send as SendIcon,
    Reorder as ReorderIcon,
    VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material'
import { useTranslation } from 'next-i18next'
import { usePlayerGetDownloadTokens } from 'src/client/hooks/usePlayerGetDownloadTokens'
import DownloadPagesPopupMenu from '../../common/DownloadPagesPopupMenu/DownloadPagesPopupMenu'

export type SentLetter = Pick<Letter, 'id' | 'serialNumber' | 'sentDate' | 'recipientId' | 'senderId' | 'numberOfFiles'>

interface Props {
    readonly letter: SentLetter
    readonly onPlayerGetTokens: ReturnType<typeof usePlayerGetDownloadTokens>
    readonly onEditLetter: () => void
}

const PlayerSentLetterRow = ({ letter, onEditLetter, onPlayerGetTokens }: Props) => {
    const characters = useCharactersPlayer()
    const { t } = useTranslation('player')

    return (
        <TableRow>
            <TableCell>{characters.map[letter.recipientId || '']?.name}</TableCell>
            <TableCell>
                <Chip label={formatDate(letter.sentDate)} icon={<SendIcon />} />
            </TableCell>
            <TableCell>
                <Chip label={letter.serialNumber} icon={<ReorderIcon />} />
            </TableCell>
            <TableCell>{letter.senderId ? null : <VisibilityOffIcon />}</TableCell>
            <TableCell sx={{ textAlign: 'right' }}>
                {letter.numberOfFiles > 0 ? (
                    <DownloadPagesPopupMenu onGetTokens={() => onPlayerGetTokens(letter.id)} />
                ) : null}
                <Tooltip title={t('OutgoingLetter.edit') as string}>
                    <IconButton onClick={onEditLetter}>
                        <EditIcon />
                    </IconButton>
                </Tooltip>
            </TableCell>
        </TableRow>
    )
}

export default PlayerSentLetterRow
