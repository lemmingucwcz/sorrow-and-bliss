import React, { useState } from 'react'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { Button, Paper } from '@mui/material'
import FormFieldCharacterAutocomplete, {
    CharacterAC,
} from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { Form } from 'react-final-form'
import { useTranslation } from 'next-i18next'
import PlayerSentLettersList from './PlayerSentLettersList'
import PlayerLetterFormDialog from '../PlayerLetterFormDialog/PlayerLetterFormDialog'
import { SentLetter } from './PlayerSentLetterRow'
import { AddBox as AddBoxIcon } from '@mui/icons-material'
import { useFeedback } from '../../../hooks/useFeedback'

interface FormShape {
    readonly recipient?: CharacterAC
}

const PlayerSentContent = () => {
    const characters = useCharactersPlayer()
    const { t } = useTranslation('player')
    const [showingCreate, setShowingCreate] = useState(false)
    const [editingLetter, setEditingLetter] = useState<SentLetter>()
    const feedback = useFeedback()

    const handleCreate = () => setShowingCreate(true)
    const handleClose = (message?: string) => {
        setShowingCreate(false)
        setEditingLetter(undefined)
        feedback(message || '')
    }

    return (
        <Form<FormShape>
            onSubmit={() => {}}
            render={({ values }) => (
                <PageContentBox>
                    <Button
                        onClick={handleCreate}
                        variant="contained"
                        startIcon={<AddBoxIcon />}
                        sx={{ marginBottom: '10px' }}
                    >
                        {t('Player.newOutgoingLetter')}
                    </Button>

                    <Paper sx={{ width: 300, marginBottom: '10px' }}>
                        <FormFieldCharacterAutocomplete
                            characters={characters.list}
                            name="recipient"
                            label={t('Letter.recipient')}
                            fullWidth
                        />
                    </Paper>
                    {(editingLetter || showingCreate) && (
                        <PlayerLetterFormDialog editLetter={editingLetter} onClose={handleClose} />
                    )}
                    <PlayerSentLettersList recipient={values.recipient} onStartEditingLetter={setEditingLetter} />
                </PageContentBox>
            )}
        />
    )
}

export default PlayerSentContent
