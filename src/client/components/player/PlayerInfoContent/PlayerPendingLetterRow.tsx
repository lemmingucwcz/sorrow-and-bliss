import { Chip, IconButton, TableCell, TableRow, Tooltip } from '@mui/material'
import React from 'react'
import { getAgeColor, formatDate, letterAge } from 'src/client/utils/dateUtils'
import { Letter } from 'src/graphql/__generated__/typescript-operations'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import {
    Send as SendIcon,
    MarkunreadMailbox as MarkunreadMailboxIcon,
    AccessTime as AccessTimeIcon,
    CancelScheduleSend as CancelScheduleSendIcon,
    Reorder as ReorderIcon,
    Reply as ReplyIcon,
} from '@mui/icons-material'
import { useTranslation } from 'next-i18next'

interface Props {
    readonly letter: Pick<Letter, 'id' | 'serialNumber' | 'sentDate' | 'receivedDate' | 'senderId'>
    readonly onEndLetterLine: () => void
    readonly onCreateAnswer: () => void
}

const PlayerPendingLetterRow = ({ letter, onEndLetterLine, onCreateAnswer }: Props) => {
    const characters = useCharactersPlayer()
    const age = letterAge(letter.receivedDate)
    const { t } = useTranslation('player')

    return (
        <TableRow>
            <TableCell>{characters.map[letter.senderId || '']?.name}</TableCell>
            <TableCell>
                <Chip label={formatDate(letter.sentDate)} icon={<SendIcon />} />
            </TableCell>
            <TableCell>
                <Chip label={letter.serialNumber} icon={<ReorderIcon />} />
            </TableCell>
            <TableCell>
                <Chip label={formatDate(letter.receivedDate)} icon={<MarkunreadMailboxIcon />} />
            </TableCell>
            <TableCell>
                <Chip label={age} icon={<AccessTimeIcon />} color={getAgeColor(age)} />
            </TableCell>
            <TableCell>
                <Tooltip title={t('IncomingLetter.createReply') as string}>
                    <IconButton onClick={onCreateAnswer}>
                        <ReplyIcon />
                    </IconButton>
                </Tooltip>
                <Tooltip title={t('IncomingLetter.endLetterLine') as string}>
                    <IconButton onClick={onEndLetterLine}>
                        <CancelScheduleSendIcon />
                    </IconButton>
                </Tooltip>
            </TableCell>
        </TableRow>
    )
}

export default PlayerPendingLetterRow
