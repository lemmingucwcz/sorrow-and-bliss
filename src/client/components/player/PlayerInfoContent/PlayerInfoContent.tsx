import React, { useState } from 'react'
import { useTranslation } from 'next-i18next'
import {
    Alert,
    Button,
    LinearProgress,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from '@mui/material'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import {
    PlayerEndLetterLineMutation,
    PlayerEndLetterLineMutationVariables,
    PlayerInfoQueryQuery,
} from '../../../../graphql/__generated__/typescript-operations'
import PlayerPendingLetterRow from './PlayerPendingLetterRow'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import PlayerLetterFormDialog from '../PlayerLetterFormDialog/PlayerLetterFormDialog'
import { AddBox as AddBoxIcon, MarkEmailRead as MarkEmailReadIcon } from '@mui/icons-material'
import PlayerNewIncomingLetterDialog from '../PlayerNewIncomingLetterDialog/PlayerNewIncomingLetterDialog'
import { QueryKeys } from 'src/client/utils/queryKeys'
import { useShowConfirmDialog } from '../../../context/ConfirmDialogContext'
import { useFeedback } from '../../../hooks/useFeedback'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import PlayerPendingLetterChips from './PlayerPendingLetterChips'
import { useIsMobile } from '../../../hooks/useIsMobile'

const playerInfoQuery = require('./playerInfoQuery.graphql')
const playerEndLetterLineMutation = require('./playerEndLetterLineMutation.graphql')

type DialogType = 'newOutgoing' | 'newIncoming'

const PlayerInfoContent = () => {
    const { t } = useTranslation('player')
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const [dialogType, setDialogType] = useState<DialogType>()
    const [initialRecipientId, setInitialRecipientId] = useState<string>()
    const feedback = useFeedback()
    const showConfirmDialog = useShowConfirmDialog()
    const { data } = useQuery(QueryKeys.playerInfoQuery, async () => request<PlayerInfoQueryQuery>(playerInfoQuery, {}))
    const { mutate } = useMutation(async (letterId: string) => {
        await request<PlayerEndLetterLineMutation, PlayerEndLetterLineMutationVariables>(playerEndLetterLineMutation, {
            letterId,
        })

        await queryClient.invalidateQueries(QueryKeys.playerInfoQuery)
    })
    const isMobile = useIsMobile()

    if (!data) {
        return <LinearProgress />
    }

    const handleClose = (message?: string) => {
        setDialogType(undefined)
        setInitialRecipientId(undefined)
        feedback(message || '')
    }

    const handleNewOutgoingLetter = () => {
        setInitialRecipientId(undefined)
        setDialogType('newOutgoing')
    }

    const handleNewIncomingLetter = () => {
        setDialogType('newIncoming')
    }

    const handleEndLetterLine = async (letterId: string) => {
        const res = await showConfirmDialog({
            text: t('Player.confirmEndLetterLine'),
            confirmText: t('Player.confirmEndLetterLineConfirm'),
        })
        if (res === 'confirm') {
            mutate(letterId)
        }
    }

    const handleCreateAnswer = (recipientId: string) => {
        setInitialRecipientId(recipientId)
        setDialogType('newOutgoing')
    }

    const lit = data.player.stats.lettersInTransit
    const letters = data.player.pendingLetters

    return (
        <PageContentBox>
            <Alert severity={lit === 0 ? 'info' : 'warning'}>
                {lit === 0 ? t('Player.incoming_none') : t('Player.incoming', { count: lit })}
            </Alert>

            <Button
                onClick={handleNewIncomingLetter}
                variant="contained"
                startIcon={<MarkEmailReadIcon />}
                disabled={lit === 0}
            >
                {t('Player.newIncomingLetter')}
            </Button>

            <Button onClick={handleNewOutgoingLetter} variant="contained" startIcon={<AddBoxIcon />}>
                {t('Player.newOutgoingLetter')}
            </Button>

            <Typography variant="h6" sx={{ marginTop: '10px' }}>
                {t('Player.pendingLetters')}
            </Typography>

            {letters.length > 0 && !isMobile ? (
                <TableContainer component={Paper} sx={{ width: 'auto' }}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>{t('Letter.sender')}</TableCell>
                                <TableCell>{t('Letter.sentDateShort')}</TableCell>
                                <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                                <TableCell>{t('Letter.receivedDateShort')}</TableCell>
                                <TableCell />
                                <TableCell />
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {letters.map(letter => (
                                <PlayerPendingLetterRow
                                    key={letter.id}
                                    letter={letter}
                                    onEndLetterLine={() => handleEndLetterLine(letter.id)}
                                    onCreateAnswer={() => handleCreateAnswer(letter.senderId || '')}
                                />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {letters.length > 0 && isMobile ? (
                <>
                    {letters.map(letter => (
                        <PlayerPendingLetterChips
                            key={letter.id}
                            letter={letter}
                            onEndLetterLine={() => handleEndLetterLine(letter.id)}
                            onCreateAnswer={() => handleCreateAnswer(letter.senderId || '')}
                        />
                    ))}
                </>
            ) : null}
            {letters.length === 0 ? <Alert severity="success">{t('Player.noPendingLetters')}</Alert> : null}
            {dialogType === 'newOutgoing' ? (
                <PlayerLetterFormDialog onClose={handleClose} initialRecipientId={initialRecipientId} />
            ) : null}
            {dialogType === 'newIncoming' ? <PlayerNewIncomingLetterDialog onClose={handleClose} /> : null}
        </PageContentBox>
    )
}

export default PlayerInfoContent
