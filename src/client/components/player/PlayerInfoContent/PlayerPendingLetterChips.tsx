import React from 'react'
import { Letter } from '../../../../graphql/__generated__/typescript-operations'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { formatDate, getAgeColor, letterAge } from '../../../utils/dateUtils'
import { useTranslation } from 'next-i18next'
import { Chip, IconButton, Paper, Tooltip } from '@mui/material'
import {
    AccessTime as AccessTimeIcon,
    CancelScheduleSend as CancelScheduleSendIcon,
    MarkunreadMailbox as MarkunreadMailboxIcon,
    Reorder as ReorderIcon,
    Reply as ReplyIcon,
    Send as SendIcon,
} from '@mui/icons-material'
import { Box } from '@mui/system'

interface Props {
    readonly letter: Pick<Letter, 'id' | 'serialNumber' | 'sentDate' | 'receivedDate' | 'senderId'>
    readonly onEndLetterLine: () => void
    readonly onCreateAnswer: () => void
}

const PlayerPendingLetterChips = ({ letter, onEndLetterLine, onCreateAnswer }: Props) => {
    const characters = useCharactersPlayer()
    const age = letterAge(letter.receivedDate)
    const { t } = useTranslation('player')

    return (
        <Paper sx={{ padding: 1, width: '100%' }}>
            <Box sx={{ display: 'flex', gap: 1 }}>
                <Chip
                    label={characters.map[letter.senderId || '']?.name}
                    sx={{ flex: 1, textOverflow: 'ellipsis', justifyContent: 'left', width: '100%' }}
                />
                <Chip label={age} icon={<AccessTimeIcon />} color={getAgeColor(age)} />
            </Box>
            <Box
                sx={{
                    mt: 1,
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                    gap: 1,
                }}
            >
                <Chip label={formatDate(letter.sentDate)} icon={<SendIcon />} />
                <Chip label={letter.serialNumber} icon={<ReorderIcon />} />
                <Chip
                    label={formatDate(letter.receivedDate)}
                    icon={<MarkunreadMailboxIcon />}
                    sx={{
                        '@media screen and (max-width: 420px)': {
                            display: 'none',
                        },
                    }}
                />
                <Box sx={{ flex: 1, textAlign: 'right' }}>
                    <Tooltip title={t('IncomingLetter.createReply') as string}>
                        <IconButton onClick={onCreateAnswer}>
                            <ReplyIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={t('IncomingLetter.endLetterLine') as string}>
                        <IconButton onClick={onEndLetterLine}>
                            <CancelScheduleSendIcon />
                        </IconButton>
                    </Tooltip>
                </Box>
            </Box>
        </Paper>
    )
}

export default PlayerPendingLetterChips
