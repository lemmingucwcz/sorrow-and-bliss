import React, { useState } from 'react'
import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@mui/material'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { useTranslation } from 'next-i18next'
import { Form } from 'react-final-form'
import FormFieldCharacterAutocomplete, {
    CharacterAC,
    createNpcId,
} from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import FormFieldSwitch from '../../common/FormFieldSwitch/FormFieldSwitch'
import {
    fieldValidator,
    validateMaxFiles,
    validatePositiveInteger,
    validateRequired,
} from '../../../../utils/validationUtils'
import { TFunction } from 'i18next'
import { useMutation, useQueryClient } from 'react-query'
import {
    PlayerCreateLetterMutation,
    PlayerCreateLetterMutationVariables,
    PlayerNextSerialHintQuery,
    PlayerNextSerialHintQueryVariables,
    PlayerUpdateLetterMutation,
    PlayerUpdateLetterMutationVariables,
    PlayerUpdateLetterRequest,
} from '../../../../graphql/__generated__/typescript-operations'
import CloseDialogButton from '../../common/CloseDialogButton/CloseDialogButton'
import { QueryKeys } from '../../../utils/queryKeys'
import { SentLetter } from '../PlayerSentContent/PlayerSentLetterRow'
import { usePlayerCredentials } from '../../../context/PlayerCredentialsContext'
import { useFilesUploader } from '../../../hooks/useFilesUploader'
import FormFieldFiles from '../../common/FormFieldFiles/FormFieldFiles'
import FileUploadProgress from '../../common/FileUploadProgress/FileUploadProgress'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { getFileExtensions } from '../../../../utils/fileUtils'
import FormFieldSerialNumber from '../../common/FormFieldSerialNumber/FormFieldSerialNumber'

const playerCreateLetterMutation = require('./playerCreateLetterMutation.graphql')
const playerUpdateLetterMutation = require('./playerUpdateLetterMutation.graphql')
const nextSerialHintQuery = require('./playerNextSerialHintQuery.graphql')

interface Props {
    readonly editLetter?: SentLetter
    readonly initialRecipientId?: string
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly recipient?: CharacterAC
    readonly newRecipientName: string
    readonly serialNumber: string
    readonly sentDate: string
    readonly isAnonymous: boolean
    readonly files: File[]
}

const newLetterInitialValues: FormShape = {
    sentDate: '',
    isAnonymous: false,
    newRecipientName: '',
    serialNumber: '',
    files: [],
}

const formLevelValidate = (t: TFunction) => (values: FormShape) => {
    const res: Record<string, string | undefined> = {}

    if (!values.recipient) {
        if (!values.newRecipientName) {
            res.newRecipientName = t('Errors.required')
        }
    }

    return res
}

const PlayerLetterFormDialog = ({ editLetter, initialRecipientId, onClose }: Props) => {
    const characters = useCharactersPlayer()
    const { t } = useTranslation('player')
    const { t: ct } = useTranslation('common')
    const gRequest = useGraphqlRequest()
    const queryClient = useQueryClient()
    const credentials = usePlayerCredentials()
    const { progress, onBeforeUpdate, onUploadFiles, onFinish } = useFilesUploader()

    const [initialValues] = useState(() =>
        editLetter
            ? ({
                  sentDate: editLetter.sentDate,
                  serialNumber: editLetter.serialNumber.toString(),
                  newRecipientName: '',
                  recipient: characters.map[editLetter.recipientId],
                  isAnonymous: !editLetter.senderId,
                  files: [],
              } as FormShape)
            : {
                  ...newLetterInitialValues,
                  recipient: initialRecipientId ? characters.map[initialRecipientId] : undefined,
              },
    )

    const { isLoading: isMutatingC, mutate: mutateC } = useMutation(
        async ({ request, files }: { request: PlayerCreateLetterMutationVariables; files: File[] }) => {
            const res = await gRequest<PlayerCreateLetterMutation, PlayerCreateLetterMutationVariables>(
                playerCreateLetterMutation,
                request,
            )

            if (res.player.createLetter) {
                await queryClient.invalidateQueries(QueryKeys.playerInfoQuery)
                await queryClient.invalidateQueries(QueryKeys.playerSentLettersQuery)
                await queryClient.invalidateQueries(QueryKeys.playerNextSerial)

                // Created - upload files
                await onUploadFiles({
                    token: credentials.token,
                    letterId: res.player.createLetter,
                    runId: '0', // Will be taken from auth actually
                    files,
                })
            }
        },
    )

    const { isLoading: isMutatingU, mutate: mutateU } = useMutation(
        async ({ request, files }: { request: PlayerUpdateLetterRequest; files: File[] }) => {
            const res = await gRequest<PlayerUpdateLetterMutation, PlayerUpdateLetterMutationVariables>(
                playerUpdateLetterMutation,
                { request },
            )

            if (res.player.updateLetter) {
                await queryClient.invalidateQueries(QueryKeys.playerInfoQuery)
                await queryClient.invalidateQueries(QueryKeys.playerSentLettersQuery)
                await queryClient.invalidateQueries(QueryKeys.playerNextSerial)

                if (request.newRecipientName) {
                    // Refetch characters when we have created a new NPC
                    await queryClient.invalidateQueries(QueryKeys.playerCharacters)
                }

                // Updated - upload files
                await onUploadFiles({
                    token: credentials.token,
                    letterId: request.id,
                    runId: '0', // Will be taken from auth actually
                    files,
                })
            }
        },
    )

    const handleFormSubmit = async (values: FormShape) => {
        const recipientId = values.recipient?.id || '0'
        const { files } = values
        onBeforeUpdate(files)
        if (editLetter) {
            await mutateU({
                request: {
                    id: editLetter.id,
                    sentDate: values.sentDate,
                    serialNumber: parseInt(values.serialNumber),
                    isAnonymous: values.isAnonymous,
                    recipientId: parseInt(recipientId) > 0 ? recipientId : undefined,
                    fileExtensions: files.length > 0 ? getFileExtensions(files) : undefined,
                    newRecipientName: values.newRecipientName,
                },
                files,
            })
        } else {
            await mutateC({
                request: {
                    sentDate: values.sentDate,
                    serialNumber: parseInt(values.serialNumber),
                    isAnonymous: values.isAnonymous,
                    recipientId: parseInt(recipientId) > 0 ? recipientId : undefined,
                    fileExtensions: getFileExtensions(files),
                    newRecipientName: values.newRecipientName,
                },
                files,
            })
        }
        onFinish()
        onClose(t(editLetter ? 'LetterForm.letterUpdated' : 'LetterForm.letterCreated'))
    }

    const handleClose = () => onClose()

    const isMutating = isMutatingC || isMutatingU

    return (
        <Dialog open={true}>
            <Form<FormShape>
                onSubmit={handleFormSubmit}
                initialValues={initialValues}
                validate={formLevelValidate(t)}
                render={({ handleSubmit, form, values }) => {
                    const recipientId = values.recipient?.id || ''
                    const hintQueryFn = async () => {
                        const res = await gRequest<PlayerNextSerialHintQuery, PlayerNextSerialHintQueryVariables>(
                            nextSerialHintQuery,
                            {
                                recipientId,
                            },
                        )
                        return res.player.nextLetterSerial
                    }
                    const queryKey = [QueryKeys.playerNextSerial, recipientId]

                    return (
                        <>
                            <DialogTitle>{t(editLetter ? 'LetterForm.titleEdit' : 'LetterForm.titleNew')}</DialogTitle>
                            <DialogContent
                                sx={{
                                    '& .MuiAutocomplete-listbox': {
                                        maxHeight: '200px',
                                    },
                                }}
                            >
                                <form onSubmit={handleSubmit} style={{ paddingTop: '5px' }}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <FormFieldCharacterAutocomplete
                                                characters={characters.list}
                                                name="recipient"
                                                label={t('Letter.recipient')}
                                                fullWidth
                                                autoFocus
                                                allowCreating="recipient"
                                                validate={fieldValidator(ct, validateRequired)}
                                            />
                                        </Grid>
                                        {values.recipient?.id === createNpcId ? (
                                            <Grid item xs={12}>
                                                <FormFieldTextInput
                                                    name="newRecipientName"
                                                    label={t('LetterForm.newRecipientName')}
                                                    fullWidth
                                                />
                                            </Grid>
                                        ) : null}
                                        <Grid item xs={12}>
                                            <FormFieldSwitch name="isAnonymous" label={t('LetterForm.sendAnonymous')} />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormFieldSerialNumber
                                                name="serialNumber"
                                                label={t('Letter.serialNumber')}
                                                validate={fieldValidator(ct, [
                                                    validateRequired,
                                                    validatePositiveInteger,
                                                ])}
                                                isAnonymous={values.isAnonymous}
                                                isEditing={Boolean(editLetter)}
                                                queryEnabled={Boolean(values.recipient)}
                                                queryFn={hintQueryFn}
                                                queryKey={queryKey}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormFieldTextInput
                                                name="sentDate"
                                                label={t('Letter.sentDate')}
                                                type="date"
                                                fullWidth
                                                validate={fieldValidator(ct, validateRequired)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormFieldFiles
                                                name="files"
                                                label={t('LetterForm.filesLabel')}
                                                placeholder={t('LetterForm.filesPlaceholder')}
                                                validate={fieldValidator(
                                                    ct,
                                                    editLetter
                                                        ? validateMaxFiles(25)
                                                        : [validateRequired, validateMaxFiles(25)],
                                                )}
                                                hint={
                                                    editLetter
                                                        ? ct(
                                                              values.files.length > 0
                                                                  ? 'FileUploadHint.hasFiles'
                                                                  : 'FileUploadHint.noFiles',
                                                          )
                                                        : undefined
                                                }
                                            />
                                        </Grid>
                                    </Grid>
                                </form>
                                <FileUploadProgress progress={progress} />
                            </DialogContent>
                            <DialogActions>
                                <CloseDialogButton disabled={isMutating} onClick={handleClose} />
                                <Button
                                    variant="contained"
                                    onClick={() => {
                                        form.submit()
                                    }}
                                    disabled={isMutating}
                                    startIcon={isMutating ? <CircularProgress /> : null}
                                >
                                    {t(editLetter ? 'LetterForm.submitEdit' : 'LetterForm.submitNew')}
                                </Button>
                            </DialogActions>
                        </>
                    )
                }}
            />
        </Dialog>
    )
}

export default PlayerLetterFormDialog
