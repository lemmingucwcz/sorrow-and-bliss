import { Alert, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@mui/material'
import React, { useState } from 'react'
import FormFieldCharacterAutocomplete, {
    anonymousSenderId,
    CharacterAC,
} from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { Form } from 'react-final-form'
import { useTranslation } from 'next-i18next'
import { fieldValidator, validatePositiveInteger, validateRequired } from '../../../../utils/validationUtils'
import CloseDialogButton from '../../common/CloseDialogButton/CloseDialogButton'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { useCharactersPlayer } from '../../../hooks/useCharactersPlayer'
import { useMutation, useQueryClient } from 'react-query'
import {
    PlayerConfirmIncomingLetterMutationMutation,
    PlayerConfirmIncomingLetterMutationMutationVariables,
    PlayerConfirmLetterRequest,
} from '../../../../graphql/__generated__/typescript-operations'
import { QueryKeys } from '../../../utils/queryKeys'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

const playerConfirmIncomingLetterMutation = require('./playerConfirmIncomingLetterMutation.graphql')

interface Props {
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly sender?: CharacterAC
    readonly serialNumber: string
    readonly sentDate: string
    readonly receivedDate: string
}

const PlayerNewIncomingLetterDialog = ({ onClose }: Props) => {
    const [notFound, setNotFound] = useState(false)
    const characters = useCharactersPlayer()
    const gRequest = useGraphqlRequest()
    const queryClient = useQueryClient()
    const handleClose = () => onClose()
    const { t } = useTranslation('player')
    const { t: ct } = useTranslation('common')

    const { isLoading: isMutating, mutate } = useMutation(async (request: PlayerConfirmLetterRequest) => {
        setNotFound(false)

        const res = await gRequest<
            PlayerConfirmIncomingLetterMutationMutation,
            PlayerConfirmIncomingLetterMutationMutationVariables
        >(playerConfirmIncomingLetterMutation, { request })

        if (res.player.confirmIncomingLetter) {
            queryClient.invalidateQueries(QueryKeys.playerInfoQuery)
            onClose(t('IncomingLetter.registered'))
        } else {
            setNotFound(true)
        }
    })

    const handleFormSubmit = (values: FormShape) => {
        const senderId = values.sender?.id
        mutate({
            senderId: senderId === anonymousSenderId ? undefined : senderId,
            serialNumber: parseInt(values.serialNumber),
            sentDate: values.sentDate,
            receivedDate: values.receivedDate,
        })
    }

    return (
        <Dialog open={true} onClose={handleClose}>
            <Form<FormShape>
                onSubmit={handleFormSubmit}
                render={({ handleSubmit, form }) => (
                    <>
                        <DialogTitle>{t('IncomingLetter.title')}</DialogTitle>
                        <DialogContent
                            sx={{
                                '& .MuiAutocomplete-listbox': {
                                    maxHeight: '200px',
                                },
                            }}
                        >
                            <form onSubmit={handleSubmit} style={{ paddingTop: '5px' }}>
                                <Grid container spacing={2}>
                                    {notFound ? (
                                        <Grid item xs={12}>
                                            <Alert severity="error">{t('IncomingLetter.doesNotExist')}</Alert>
                                        </Grid>
                                    ) : null}
                                    <Grid item xs={12}>
                                        <FormFieldCharacterAutocomplete
                                            characters={characters.list}
                                            name="sender"
                                            label={t('Letter.sender')}
                                            fullWidth
                                            validate={fieldValidator(ct, validateRequired)}
                                            addAnnonymous
                                            autoFocus
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormFieldTextInput
                                            name="serialNumber"
                                            label={t('Letter.serialNumber')}
                                            type="number"
                                            fullWidth
                                            validate={fieldValidator(ct, [validateRequired, validatePositiveInteger])}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormFieldTextInput
                                            name="sentDate"
                                            label={t('Letter.sentDate')}
                                            type="date"
                                            fullWidth
                                            validate={fieldValidator(ct, validateRequired)}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormFieldTextInput
                                            name="receivedDate"
                                            label={t('Letter.receivedDate')}
                                            type="date"
                                            fullWidth
                                            validate={fieldValidator(ct, validateRequired)}
                                        />
                                    </Grid>
                                </Grid>
                            </form>
                        </DialogContent>
                        <DialogActions>
                            <CloseDialogButton disabled={isMutating} onClick={handleClose} />
                            <Button
                                variant="contained"
                                onClick={() => {
                                    form.submit()
                                }}
                                disabled={isMutating}
                                startIcon={isMutating ? <CircularProgress /> : null}
                            >
                                {t('IncomingLetter.register')}
                            </Button>
                        </DialogActions>
                    </>
                )}
            />
        </Dialog>
    )
}

export default PlayerNewIncomingLetterDialog
