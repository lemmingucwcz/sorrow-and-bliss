import React from 'react'
import SBBottomNavigation from '../../common/SBBottomNavigation/SBBottomNavigation'
import { useTranslation } from 'next-i18next'
import { Dashboard as DashboardIcon, SendAndArchive as SendAndArchiveIcon } from '@mui/icons-material'

const PlayerBottomNavigation = () => {
    const { t } = useTranslation('player')

    return (
        <SBBottomNavigation
            actions={[
                {
                    label: t('Navigation.dashboard'),
                    icon: <DashboardIcon />,
                    route: '/player',
                },
                {
                    label: t('Navigation.outgoing'),
                    icon: <SendAndArchiveIcon />,
                    route: '/player/sent',
                },
            ]}
        />
    )
}

export default PlayerBottomNavigation
