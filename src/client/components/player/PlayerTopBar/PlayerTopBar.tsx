import React, { useCallback, useEffect } from 'react'
import { useTranslation } from 'next-i18next'
import { usePlayerCredentials } from '../../../context/PlayerCredentialsContext'
import TopBar from '../../common/TopBar/TopBar'
import { CredentialsService } from '../../../services/CredentialsService'
import { useRouter } from 'next/router'
import { useQuery, useQueryClient } from 'react-query'
import { IsRunActiveQuery } from '../../../../graphql/__generated__/typescript-operations'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { useFeedback } from '../../../hooks/useFeedback'
import { useGraphqlClient } from '../../../context/GraphqlClientContext'
import { useIsMobile } from '../../../hooks/useIsMobile'

const isRunActiveQuery = require('./isRunActiveQuery.graphql')

const PlayerTopBar = () => {
    const { t } = useTranslation('player')
    const playerCredentials = usePlayerCredentials()
    const router = useRouter()
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const graphqlClient = useGraphqlClient()
    const feedback = useFeedback()
    const isMobile = useIsMobile()
    const { data: isRunActive } = useQuery(
        'isRunActive',
        () =>
            request<IsRunActiveQuery>(isRunActiveQuery)
                .then(res => Boolean(res.player.isRunActive))
                .catch(() => false),
        {
            refetchOnMount: 'always',
            staleTime: 300000,
        },
    )

    useEffect(() => {
        if (!playerCredentials.token) {
            // Redirect to login when user was logged out
            router.push({ pathname: '/loginPlayer' })
        }
    }, [router, playerCredentials])

    const handleLogout = useCallback(
        async (clearInfo?: boolean) => {
            CredentialsService.logoutPlayer(clearInfo)
            queryClient.clear()
            graphqlClient.setHeader('Authorization', '')
            await router.push({ pathname: '/loginPlayer' })
        },
        [graphqlClient, queryClient, router],
    )

    useEffect(() => {
        if (isRunActive === false) {
            // If run is / became inactive, logout
            feedback(t('Player.runNoLongerActive'), 'error')
            handleLogout(true)
        }
    }, [handleLogout, isRunActive, feedback, t])

    if (isMobile) {
        return (
            <TopBar onLogout={handleLogout} subtitle={playerCredentials.runName}>
                {playerCredentials.characterName}
            </TopBar>
        )
    }

    return (
        <TopBar onLogout={handleLogout}>
            {t('Player.pageTitle', {
                runName: playerCredentials.runName,
                characterName: playerCredentials.characterName,
            })}
        </TopBar>
    )
}

export default PlayerTopBar
