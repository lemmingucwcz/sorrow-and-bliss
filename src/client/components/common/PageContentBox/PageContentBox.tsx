import React from 'react'
import { Box } from '@mui/material'

const PageContentBox: React.FC = ({ children }) => (
    <Box
        sx={{
            display: 'flex',
            flexDirection: 'column',
            gap: '10px',
            alignItems: 'center',
            padding: '20px 20px 100px', // 100px is for bottom navigation
        }}
    >
        {children}
    </Box>
)

export default PageContentBox
