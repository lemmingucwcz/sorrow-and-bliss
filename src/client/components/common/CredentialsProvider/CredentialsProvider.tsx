import React, { useEffect, useState } from 'react'
import { PlayerCredentialsContext } from 'src/client/context/PlayerCredentialsContext'
import {
    AdminCredentials,
    CredentialsService,
    dummyAdminCredentials,
    dummyPlayerCredentials,
    PlayerCredentials,
} from 'src/client/services/CredentialsService'
import { AdminCredentialsContext } from 'src/client/context/AdminCredentialsContext'
import { useGraphqlClient } from '../../../context/GraphqlClientContext'
import { useRouter } from 'next/router'
import AppLoadingPaper from '../AppLoadingPaper'

interface Props {
    readonly pathType: 'player' | 'admin'
}

const CredentialsProvider: React.FC<Props> = ({ pathType, children }) => {
    const router = useRouter()
    const [playerCredentials, setPlayerCredentials] = useState<PlayerCredentials>()
    const [adminCredentials, setAdminCredentials] = useState<AdminCredentials | undefined>()
    const client = useGraphqlClient()

    useEffect(() => {
        // Determine whether we have valid credentials for given path
        if (pathType === 'admin') {
            const credentials = CredentialsService.getAdminCredentials()

            if (!credentials || credentials.type !== 'admin' || !credentials.token) {
                // Need admin login
                setAdminCredentials(undefined)
                router.push({ pathname: '/loginAdmin' })
                return
            }
            client.setHeader('Authorization', `Bearer ${credentials.token}`)
            setAdminCredentials(credentials as AdminCredentials)
        }

        if (pathType === 'player') {
            const credentials = CredentialsService.getPlayerCredentials()

            if (!credentials || credentials.type !== 'player' || !credentials.token) {
                // Need player login
                setPlayerCredentials(undefined)
                router.push({ pathname: '/loginPlayer' })
                return
            }
            client.setHeader('Authorization', `Bearer ${credentials.token}`)
            setPlayerCredentials(credentials as PlayerCredentials)
        }
    }, [pathType, router, client])

    const haveCredentials =
        (pathType === 'admin' && Boolean(adminCredentials)) || (pathType === 'player' && Boolean(playerCredentials))

    if (!haveCredentials) {
        // Delay rendering when we still need to resolve credentials
        return <AppLoadingPaper />
    }

    return (
        <PlayerCredentialsContext.Provider value={playerCredentials || dummyPlayerCredentials}>
            <AdminCredentialsContext.Provider value={adminCredentials || dummyAdminCredentials}>
                {children}
            </AdminCredentialsContext.Provider>
        </PlayerCredentialsContext.Provider>
    )
}

export default CredentialsProvider
