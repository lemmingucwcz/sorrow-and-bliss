import React from 'react'
import { useTranslation } from 'next-i18next'
import { Button } from '@mui/material'

interface Props {
    readonly disabled?: boolean
    readonly onClick: () => void
}

const CloseDialogButton = ({ disabled, onClick }: Props) => {
    const { t } = useTranslation('common')

    return (
        <Button onClick={onClick} disabled={disabled}>
            {t('close')}
        </Button>
    )
}

export default CloseDialogButton
