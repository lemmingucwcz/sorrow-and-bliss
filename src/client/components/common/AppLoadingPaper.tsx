import React from 'react'
import { useTranslation } from 'next-i18next'
import { CircularProgress, Paper, Typography } from '@mui/material'

const AppLoadingPaper = () => {
    const { t } = useTranslation('common')

    return (
        <Paper
            sx={{
                position: 'absolute',
                left: '50%',
                top: '50%',
                transform: 'translate(-50%,-50%)',
                textAlign: 'center',
                padding: '30px',
            }}
        >
            <Typography variant="h6" sx={{ marginBottom: '30px' }}>
                {t('welcome')}
            </Typography>
            <CircularProgress />
        </Paper>
    )
}

export default AppLoadingPaper
