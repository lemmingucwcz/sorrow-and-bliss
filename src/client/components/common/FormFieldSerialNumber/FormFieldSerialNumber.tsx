import React from 'react'
import { QueryKey, useQuery } from 'react-query'
import { useTranslation } from 'next-i18next'
import { FieldValidator } from 'final-form'
import { TFunction } from 'i18next'
import FormFieldTextInput from '../FormFieldTextInput/FormFieldTextInput'

interface Props {
    readonly name: string
    readonly label: React.ReactNode
    readonly validate?: FieldValidator<string>
    readonly isAnonymous?: boolean
    readonly isEditing: boolean
    readonly queryKey: QueryKey
    readonly queryFn: () => Promise<number>
    readonly queryEnabled: boolean
}

interface Args {
    readonly t: TFunction
    readonly isAnonymous: boolean
    readonly isEditing: boolean
    readonly queryEnabled: boolean
    readonly queryResult?: number
}

const getHint = ({ t, isAnonymous, isEditing, queryEnabled, queryResult }: Args) => {
    if (isEditing) {
        // No hint when editing
        return undefined
    }

    if (isAnonymous) {
        // Anonymous letters have
        return t('NextSerialHint.anonymous')
    }

    if (!queryEnabled || typeof queryResult === undefined) {
        // No hint yet
        return undefined
    }

    return t('NextSerialHint.regular', { serialNumber: queryResult })
}

const FormFieldSerialNumber = ({
    name,
    label,
    validate,
    isAnonymous = false,
    isEditing,
    queryEnabled,
    queryFn,
    queryKey,
}: Props) => {
    const { t } = useTranslation('common')

    const { data } = useQuery(queryKey, queryFn, { enabled: queryEnabled && !isAnonymous && !isEditing })

    const hint = getHint({ t, isAnonymous, queryEnabled, isEditing, queryResult: data })

    return <FormFieldTextInput name={name} label={label} validate={validate} type="number" hint={hint} fullWidth />
}

export default FormFieldSerialNumber
