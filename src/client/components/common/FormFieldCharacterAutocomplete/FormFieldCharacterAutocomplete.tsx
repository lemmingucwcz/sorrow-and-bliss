import React, { useMemo } from 'react'
import FormFieldAutocomplete, { FormFieldAutocompleteProps } from '../FormFieldAutocomplete/FormFieldAutocomplete'
import { Character } from '../../../../graphql/__generated__/typescript-operations'
import { useTranslation } from 'next-i18next'
import { SxProps } from '@mui/system'
import { Theme } from '@mui/material/styles'
import { Box } from '@mui/material'
import {
    AccountBox as AccountBoxIcon,
    AccountCircleOutlined as AccountCircleOutlinedIcon,
    Person as PersonIcon,
    PersonAdd as PersonAddIcon,
} from '@mui/icons-material'

export type CharacterAC = Pick<Character, 'id' | 'name' | 'isNPC'>

export const createNpcId = '-1'
export const anonymousSenderId = '-2'

interface Props extends Omit<FormFieldAutocompleteProps<CharacterAC>, 'options'> {
    readonly allowCreating?: 'sender' | 'recipient'
    readonly addAnnonymous?: boolean
    readonly characters: CharacterAC[]
    readonly sx?: SxProps<Theme>
}

const getIcon = ({ id, isNPC }: CharacterAC) => {
    if (id === createNpcId) {
        return <PersonAddIcon />
    }
    if (id === anonymousSenderId) {
        return <AccountCircleOutlinedIcon />
    }

    return isNPC ? <PersonIcon /> : <AccountBoxIcon />
}

const FormFieldCharacterAutocomplete = ({
    name,
    label,
    validate,
    fullWidth,
    allowCreating,
    addAnnonymous,
    characters,
    autoFocus,
    sx,
}: Props) => {
    const { t } = useTranslation('common')

    const options = useMemo(() => {
        if (addAnnonymous) {
            return [...characters, { id: anonymousSenderId, name: t('anonymousSender'), isNPC: true } as CharacterAC]
        }

        return allowCreating
            ? [
                  ...characters,
                  {
                      id: createNpcId,
                      name: t(allowCreating === 'recipient' ? 'createNpcRecipient' : 'createNpcSender'),
                      isNPC: true,
                  } as CharacterAC,
              ]
            : characters
    }, [addAnnonymous, allowCreating, characters, t])

    return (
        <FormFieldAutocomplete
            name={name}
            label={label}
            options={options}
            validate={validate}
            fullWidth={fullWidth}
            renderOption={(props, option) => (
                <Box component="li" sx={{ display: 'flex', alignItems: 'center', gap: '5px' }} {...props}>
                    {getIcon(option)}
                    {option.name}
                </Box>
            )}
            getOptionLabel={o => o.name}
            sx={sx}
            autoFocus={autoFocus}
        />
    )
}

export default FormFieldCharacterAutocomplete
