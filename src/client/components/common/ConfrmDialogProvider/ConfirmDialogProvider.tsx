import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material'
import React, { useCallback, useState } from 'react'
import {
    ConfirmDialogButtons,
    ConfirmDialogContext,
    ConfirmDialogRequest,
} from 'src/client/context/ConfirmDialogContext'
import { useTranslation } from 'next-i18next'

type ResolveFn = (res: ConfirmDialogButtons) => void

const ConfirmDialogProvider: React.FC = ({ children }) => {
    const resolveRef = React.useRef<ResolveFn>()
    const [activeRequest, setActiveRequest] = useState<ConfirmDialogRequest>()
    const { t } = useTranslation('common')

    const showConfirmDialog = useCallback((request: ConfirmDialogRequest) => {
        const promise = new Promise<ConfirmDialogButtons>(resolve => {
            resolveRef.current = resolve
        })
        setActiveRequest(request)
        return promise
    }, [])

    const handleCancel = () => {
        setActiveRequest(undefined)
        resolveRef.current?.('cancel')
    }

    const handleConfirm = () => {
        setActiveRequest(undefined)
        resolveRef.current?.('confirm')
    }

    return (
        <ConfirmDialogContext.Provider value={showConfirmDialog}>
            {children}
            {activeRequest && (
                <Dialog open>
                    <DialogTitle>{activeRequest.title || t('ConfirmDialog.defaultTitle')}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>{activeRequest.text}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCancel}>
                            {activeRequest.cancelText || t('ConfirmDialog.defaultCancel')}
                        </Button>
                        <Button onClick={handleConfirm} variant="contained">
                            {activeRequest.confirmText || t('ConfirmDialog.defaultConfirm')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </ConfirmDialogContext.Provider>
    )
}

export default ConfirmDialogProvider
