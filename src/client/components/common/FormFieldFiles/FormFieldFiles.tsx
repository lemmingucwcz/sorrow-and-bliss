import React, { useState } from 'react'
import { useField } from 'react-final-form'
import { useDropzone } from 'react-dropzone'
import {
    Box,
    FormHelperText,
    IconButton,
    InputLabel,
    List,
    ListItem,
    ListItemText,
    Typography,
    useTheme,
} from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import { FieldValidator } from 'final-form'
import { useFeedback } from '../../../hooks/useFeedback'
import { useTranslation } from 'next-i18next'

interface Props {
    readonly name: string
    readonly label?: React.ReactNode
    readonly placeholder?: React.ReactNode
    readonly hint?: React.ReactNode
    readonly validate?: FieldValidator<File[]>
}

const FormFieldFiles = ({ name, label, placeholder, hint, validate }: Props) => {
    const theme = useTheme()
    const {
        input,
        meta: { error, touched, valid },
    } = useField<File[]>(name, { validate })
    const [active, setActive] = useState(false)
    const feedback = useFeedback()
    const { t } = useTranslation('common')
    const { getRootProps, getInputProps } = useDropzone({
        maxSize: 16000000,
        multiple: true,
        onDragEnter: () => setActive(true),
        onDragLeave: () => setActive(false),
        onDrop: (acceptedFiles, rejectedFiles) => {
            setActive(false)
            input.onChange({ target: { value: [...input.value, ...acceptedFiles] } })

            if (rejectedFiles.length > 0) {
                feedback(t('filesRejected'), 'error')
            }
        },
    })

    const deleteFile = (fileIndex: number) => {
        const newFiles = [...input.value]
        newFiles.splice(fileIndex, 1)
        input.onChange({ target: { value: newFiles } })
    }

    const hasError = touched && !valid
    const helperText = hasError ? error : hint

    return (
        <Box>
            <InputLabel shrink error={hasError}>
                {label}
            </InputLabel>
            {input.value.length > 0 ? (
                <List sx={{ paddingTop: 0, marginTop: '-6px' }}>
                    {input.value.map((file, n) => (
                        <ListItem
                            key={`${file.name}-${n}`}
                            dense
                            sx={{
                                paddingLeft: 0,
                                maxWidth: '100%',
                                whiteSpace: 'nowrap',
                                '& .MuiTypography-root': {
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                },
                            }}
                            secondaryAction={
                                <IconButton edge="end" onClick={() => deleteFile(n)}>
                                    <DeleteIcon />
                                </IconButton>
                            }
                        >
                            <ListItemText primary={file.name} />
                        </ListItem>
                    ))}
                </List>
            ) : null}
            <Box
                {...getRootProps()}
                sx={{
                    padding: '20px 10px',
                    borderRadius: '5px',
                    border: `1px dashed ${
                        hasError ? theme.palette.error.main : active ? theme.palette.grey.A700 : theme.palette.grey.A400
                    }`,
                    cursor: 'pointer',
                }}
            >
                <input {...getInputProps()} />
                <Typography sx={{ color: hasError ? theme.palette.error.main : theme.palette.grey.A400 }}>
                    {placeholder}
                </Typography>
            </Box>
            <FormHelperText error={hasError}>{helperText}</FormHelperText>
        </Box>
    )
}

export default FormFieldFiles
