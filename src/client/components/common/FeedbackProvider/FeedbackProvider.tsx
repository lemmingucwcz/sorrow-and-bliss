import React, { useCallback, useState } from 'react'
import { Alert, Snackbar } from '@mui/material'
import { AlertColor } from '@mui/material/Alert/Alert'
import { FeedbackContext } from 'src/client/context/FeedbackContext'

const defaultFeedbackState = { message: '', open: false, severity: 'success' as AlertColor }

const FeedbackProvider: React.FC = ({ children }) => {
    const [state, setState] = useState(defaultFeedbackState)

    const feedback = useCallback((message: string = '', severity: AlertColor = 'success') => {
        setState({
            message,
            severity,
            open: !!message,
        })
    }, [])

    const handleClose = () => setState({ ...state, open: false })

    return (
        <FeedbackContext.Provider value={feedback}>
            {children}
            <Snackbar
                open={state.open}
                autoHideDuration={5000}
                onClose={handleClose}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
                <Alert severity={state.severity} onClose={handleClose}>
                    {state.message}
                </Alert>
            </Snackbar>
        </FeedbackContext.Provider>
    )
}

export default FeedbackProvider
