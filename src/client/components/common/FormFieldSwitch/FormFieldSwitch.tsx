import { FormControlLabel, Switch } from '@mui/material'
import React from 'react'
import { useField } from 'react-final-form'

interface Props {
    readonly name: string
    readonly label: string
}

const FormFieldSwitch = ({ name, label }: Props) => {
    const {
        input: { checked, onChange, onBlur },
    } = useField<boolean>(name, { type: 'checkbox' })

    return (
        <FormControlLabel
            control={<Switch value={true} checked={checked} onBlur={onBlur} onChange={onChange} />}
            label={label}
        />
    )
}

export default FormFieldSwitch
