import React from 'react'
import { FieldValidator } from 'final-form'
import { useField } from 'react-final-form'
import { Autocomplete, AutocompleteValue, FormControl, FormHelperText, TextField } from '@mui/material'
import { AutocompleteRenderOptionState } from '@mui/material/Autocomplete/Autocomplete'
import { Theme } from '@mui/material/styles'
import { SxProps } from '@mui/system'

export interface FormFieldAutocompleteProps<T> {
    readonly name: string
    readonly label: React.ReactNode
    readonly fullWidth?: boolean
    readonly autoFocus?: boolean
    readonly validate?: FieldValidator<T>
    readonly getOptionLabel?: (option: T) => string
    readonly renderOption?: (
        props: React.HTMLAttributes<HTMLLIElement>,
        option: T,
        state: AutocompleteRenderOptionState,
    ) => React.ReactNode

    readonly options: T[]
    readonly sx?: SxProps<Theme>
}

const FormFieldAutocomplete = <T,>({
    label,
    name,
    fullWidth,
    validate,
    getOptionLabel,
    renderOption,
    options,
    autoFocus,
    sx,
}: FormFieldAutocompleteProps<T>) => {
    const {
        input: { value, onChange, onBlur },
        meta: { error, touched, valid },
    } = useField<T>(name, { validate })

    const hasError = touched && !valid

    const handleChange = (event: React.SyntheticEvent, val: AutocompleteValue<T, false, false, false>) => {
        onChange({ target: { value: val } })
    }

    return (
        <FormControl fullWidth={fullWidth} error={hasError} sx={sx}>
            <Autocomplete<T, false, false, false>
                fullWidth={fullWidth}
                disablePortal
                renderInput={params => <TextField {...params} error={hasError} label={label} autoFocus={autoFocus} />}
                options={options}
                value={value ? value : null}
                onChange={handleChange}
                onBlur={onBlur}
                getOptionLabel={getOptionLabel}
                renderOption={renderOption}
            />
            {hasError && <FormHelperText>{error}</FormHelperText>}
        </FormControl>
    )
}

export default FormFieldAutocomplete
