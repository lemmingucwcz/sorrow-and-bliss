import React from 'react'
import { BottomNavigation, BottomNavigationAction, Paper, useTheme } from '@mui/material'
import { useRouter } from 'next/router'

interface RouteAction {
    readonly route: string
    readonly label: string
    readonly icon: React.ReactNode
}

interface Props {
    readonly actions: RouteAction[]
}

const SBBottomNavigation = ({ actions }: Props) => {
    const router = useRouter()
    const theme = useTheme()

    const handleChange = (_: unknown, newValue: string) => {
        router.push({ pathname: newValue })
    }

    return (
        <Paper
            sx={{
                position: 'fixed',
                bottom: 10,
                left: '50%',
                transform: 'translate(-50%, 0)',
                padding: '10px',
                background: theme.palette.grey.A100,
            }}
        >
            <BottomNavigation showLabels value={router.pathname} onChange={handleChange}>
                {actions.map(action => (
                    <BottomNavigationAction
                        key={action.route}
                        label={action.label}
                        value={action.route}
                        icon={action.icon}
                    />
                ))}
            </BottomNavigation>
        </Paper>
    )
}

export default SBBottomNavigation
