import React from 'react'
import { AppBar, Box, IconButton, Toolbar, Typography } from '@mui/material'
import MailIcon from '@mui/icons-material/Mail'
import LogoutIcon from '@mui/icons-material/Logout'

interface Props {
    readonly subtitle?: string
    readonly onLogout?: () => void
}

const TopBar: React.FC<Props> = ({ subtitle, onLogout, children }) => (
    <AppBar position="static">
        <Toolbar>
            <MailIcon />
            {subtitle ? (
                <Box component="div" sx={{ flexGrow: 1, paddingLeft: '20px' }}>
                    <Typography variant="body1" component="div">
                        {children}
                    </Typography>
                    <Typography variant="caption" component="div">
                        {subtitle}
                    </Typography>
                </Box>
            ) : (
                <Typography variant="h6" component="div" sx={{ flexGrow: 1, paddingLeft: '20px' }}>
                    {children}
                </Typography>
            )}
            {onLogout ? (
                <IconButton onClick={onLogout} color="inherit">
                    <LogoutIcon />
                </IconButton>
            ) : null}
        </Toolbar>
    </AppBar>
)

export default TopBar
