import React from 'react'
import { LinearProgress } from '@mui/material'

interface Props {
    readonly progress: number | null
}

const FileUploadProgress = ({ progress }: Props) => {
    if (progress === null) {
        return null
    }

    return (
        <LinearProgress
            sx={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}
            variant="determinate"
            value={progress}
        />
    )
}

export default FileUploadProgress
