import React, { PropsWithChildren } from 'react'
import { useField } from 'react-final-form'
import { FormControl, FormHelperText, InputLabel, Select } from '@mui/material'
import { FieldValidator } from 'final-form'

interface Props<T> {
    readonly name: string
    readonly label: React.ReactNode
    readonly fullWidth?: boolean
    readonly validate?: FieldValidator<T>
}

const FormFieldSelect = <T,>({ label, name, fullWidth, validate, children }: PropsWithChildren<Props<T>>) => {
    const {
        input: { value, onChange, onBlur },
        meta: { error, touched, valid },
    } = useField<T>(name, { validate })

    const hasError = touched && !valid

    return (
        <FormControl fullWidth={fullWidth} error={hasError}>
            <InputLabel>{label}</InputLabel>
            <Select label={label} name={name} value={value} onChange={onChange} onBlur={onBlur} error={hasError}>
                {children}
            </Select>
            {hasError && <FormHelperText>{error}</FormHelperText>}
        </FormControl>
    )
}

export default FormFieldSelect
