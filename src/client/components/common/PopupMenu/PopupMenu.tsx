import { IconButton, Menu, MenuItem } from '@mui/material'
import React, { MouseEventHandler } from 'react'

export interface PopupMenuItem {
    readonly action: string
    readonly content: React.ReactNode
    readonly icon: React.ReactNode
}

interface Props {
    readonly icon: React.ReactNode
    readonly items: PopupMenuItem[]
    readonly onClick: (action: string) => void
    readonly onOpen?: () => void
}

const PopupMenu = ({ icon, items, onClick, onOpen }: Props) => {
    const [anchorEl, setAnchorEl] = React.useState<Element | null>(null)
    const open = Boolean(anchorEl)
    const handleClick: MouseEventHandler = event => {
        setAnchorEl(event.currentTarget as Element)
        onOpen?.()
    }
    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <>
            <IconButton onClick={handleClick}>{icon}</IconButton>
            <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
                {items.map(item => (
                    <MenuItem
                        key={item.action}
                        onClick={() => {
                            handleClose()
                            onClick(item.action)
                        }}
                    >
                        {item.content}
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}

export default PopupMenu
