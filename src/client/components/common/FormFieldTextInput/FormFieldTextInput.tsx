import React from 'react'
import { useField } from 'react-final-form'
import { FormControl, FormHelperText, InputLabel, OutlinedInput, Theme } from '@mui/material'
import { FieldValidator } from 'final-form'
import { SxProps } from '@mui/system'

interface Props {
    readonly name: string
    readonly label: React.ReactNode
    readonly type?: string
    readonly fullWidth?: boolean
    readonly multiline?: boolean
    readonly autoFocus?: boolean
    readonly validate?: FieldValidator<string>
    readonly sx?: SxProps<Theme>
    readonly hint?: React.ReactNode
}

const FormFieldTextInput = ({ name, label, type, validate, fullWidth, multiline, autoFocus, sx, hint }: Props) => {
    const {
        input: { value, onChange, onBlur },
        meta: { error, touched, valid },
    } = useField<string>(name, { validate, type })

    const hasError = touched && !valid

    return (
        <FormControl fullWidth={fullWidth} error={hasError} sx={sx}>
            <InputLabel shrink={type === 'date' ? true : undefined}>{label}</InputLabel>
            <OutlinedInput
                error={hasError}
                onBlur={onBlur}
                onChange={onChange}
                value={value}
                label={label}
                type={type}
                multiline={multiline}
                autoFocus={autoFocus}
                notched={type === 'date' ? true : undefined}
            />
            {(hasError || hint) && <FormHelperText>{hasError ? error : hint}</FormHelperText>}
        </FormControl>
    )
}

export default FormFieldTextInput
