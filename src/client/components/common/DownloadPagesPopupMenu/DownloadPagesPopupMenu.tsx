import { ContentCopy as ContentCopyIcon } from '@mui/icons-material'
import React, { useState } from 'react'
import { DownloadToken } from '../../../../graphql/__generated__/typescript-operations'
import PopupMenu, { PopupMenuItem } from '../PopupMenu/PopupMenu'
import { useTranslation } from 'next-i18next'

interface Props {
    readonly onGetTokens: () => Promise<DownloadToken[]>
}

const DownloadPagesPopupMenu = ({ onGetTokens }: Props) => {
    const [tokens, setTokens] = useState<DownloadToken[]>([])
    const [menuItems, setMenuItems] = useState<PopupMenuItem[]>([])
    const { t } = useTranslation('common')

    const handleClick = (encodedToken: string) => {
        const token = tokens.find(to => to.token === encodedToken)
        if (token) {
            window.open(`/api/file-download/${encodedToken}/${token.fileName}`, '_blank')
        }
    }

    const handleOpen = async () => {
        // Lazy load items
        const loadedTokens = await onGetTokens()
        setTokens(loadedTokens)
        setMenuItems(
            loadedTokens.map(
                (token, n) =>
                    ({
                        action: token.token,
                        content: t('pageToDownload', { pageNo: n + 1 }),
                    } as PopupMenuItem),
            ),
        )
    }

    return <PopupMenu icon={<ContentCopyIcon />} items={menuItems} onClick={handleClick} onOpen={handleOpen} />
}

export default DownloadPagesPopupMenu
