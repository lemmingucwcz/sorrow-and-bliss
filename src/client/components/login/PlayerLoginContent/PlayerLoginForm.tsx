import React from 'react'
import {
    Character,
    GameRun,
    LoginQueryQuery,
    LoginQueryQueryVariables,
} from 'src/graphql/__generated__/typescript-operations'
import { Button, CircularProgress, Grid, MenuItem } from '@mui/material'
import { useTranslation } from 'next-i18next'
import { Form } from 'react-final-form'
import FormFieldSelect from '../../common/FormFieldSelect/FormFieldSelect'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { fieldValidator, validateRequired } from 'src/utils/validationUtils'
import { useRouter } from 'next/router'
import { CredentialsService } from '../../../services/CredentialsService'
import { useMutation } from 'react-query'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { useFeedback } from '../../../hooks/useFeedback'

const loginQuery = require('./loginQuery.graphql')

interface Props {
    readonly runs: Array<Pick<GameRun, 'id' | 'name'>>
    readonly characters: Array<Pick<Character, 'id' | 'name'>>
}

interface FormShape {
    readonly runId?: string
    readonly characterId?: string
    readonly password: string
}

const PlayerLoginForm = ({ runs, characters }: Props) => {
    const { t } = useTranslation('player')
    const { t: ct } = useTranslation('common')
    const router = useRouter()
    const request = useGraphqlRequest()
    const feedback = useFeedback()

    const initialValues: FormShape = {
        runId: runs.length === 1 ? runs[0].id : undefined,
        password: '',
    }
    const { isLoading, mutate } = useMutation(async (values: LoginQueryQueryVariables) => {
        request<LoginQueryQuery, LoginQueryQueryVariables>(loginQuery, values).then(res => {
            const pt = res.public.playerToken
            if (pt) {
                CredentialsService.storePlayerCredentials(
                    {
                        type: 'player',
                        token: pt.token,
                        characterName: characters.find(ch => ch.id === values.characterId)?.name || '',
                        runName: runs.find(r => r.id === values.runId)?.name || '',
                    },
                    pt.expires,
                )
                router.push({ pathname: '/player' })
            } else {
                feedback(t('LoginPage.loginError'), 'error')
            }
        })
    })

    const handleFormSubmit = (values: FormShape) => {
        mutate({
            characterId: values.characterId || '',
            runId: values.runId || '',
            password: values.password,
        })
    }

    return (
        <Form<FormShape>
            onSubmit={handleFormSubmit}
            initialValues={initialValues}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2} sx={{ textAlign: 'left' }}>
                        <Grid item xs={12}>
                            <FormFieldSelect<string>
                                name="runId"
                                label={t('LoginPage.run')}
                                fullWidth
                                validate={fieldValidator(ct, validateRequired)}
                            >
                                {runs.map(run => (
                                    <MenuItem key={run.id} value={run.id}>
                                        {run.name}
                                    </MenuItem>
                                ))}
                            </FormFieldSelect>
                        </Grid>
                        <Grid item xs={12}>
                            <FormFieldSelect<string>
                                name="characterId"
                                label={t('LoginPage.character')}
                                fullWidth
                                validate={fieldValidator(ct, validateRequired)}
                            >
                                {characters.map(char => (
                                    <MenuItem key={char.id} value={char.id}>
                                        {char.name}
                                    </MenuItem>
                                ))}
                            </FormFieldSelect>
                        </Grid>
                        <Grid item xs={12}>
                            <FormFieldTextInput
                                name="password"
                                label={t('LoginPage.password')}
                                type="password"
                                fullWidth
                                validate={fieldValidator(ct, validateRequired)}
                            />
                        </Grid>
                        <Grid item xs={12} sx={{ textAlign: 'center' }}>
                            <Button variant="contained" type="submit" disabled={isLoading}>
                                {isLoading ? <CircularProgress /> : null}
                                {t('LoginPage.login')}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            )}
        />
    )
}

export default PlayerLoginForm
