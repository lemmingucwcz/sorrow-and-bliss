import { useQuery } from 'react-query'
import { LoginFormQueryQuery } from '../../../../graphql/__generated__/typescript-operations'
import { Paper } from '@mui/material'
import React from 'react'
import PlayerLoginForm from './PlayerLoginForm'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

const loginFormQuery = require('./loginFormQuery.graphql')

const PlayerLoginContent = () => {
    const request = useGraphqlRequest()
    const { data } = useQuery('loginFormQuery', async () => request<LoginFormQueryQuery>(loginFormQuery))

    return (
        <PageContentBox>
            <Paper
                sx={{
                    padding: '20px',
                    width: '400px',
                    textAlign: 'center',
                    '@media screen and (max-width: 500px)': {
                        width: '100%',
                    },
                }}
            >
                {data ? (
                    <PlayerLoginForm runs={data.public.activeRuns} characters={data.public.playerCharacters} />
                ) : (
                    <PlayerLoginForm runs={[]} characters={[]} />
                )}
            </Paper>
        </PageContentBox>
    )
}

export default PlayerLoginContent
