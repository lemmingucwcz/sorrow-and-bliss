import React, { useEffect, useState } from 'react'
import { useTranslation } from 'next-i18next'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import { Alert, CircularProgress, Typography } from '@mui/material'
import { googleClientId } from 'src/utils/googleAuth'
import { useMutation } from 'react-query'
import { AdminLoginQuery, AdminLoginQueryVariables } from '../../../../graphql/__generated__/typescript-operations'
import { CredentialsService } from '../../../services/CredentialsService'
import { useRouter } from 'next/router'
import { useGraphqlRequest } from 'src/client/hooks/useGraphqlRequest'

const adminLoginQuery = require('./adminLoginQuery.graphql')

declare const global: {
    google: any
}

const AdminLoginContent = () => {
    const { t } = useTranslation('admin')
    const request = useGraphqlRequest()
    const router = useRouter()
    const [isAuthError, setIsAuthError] = useState(false)
    const { isLoading, mutate } = useMutation(async (googleToken: string) => {
        setIsAuthError(false)
        const res = await request<AdminLoginQuery, AdminLoginQueryVariables>(adminLoginQuery, {
            googleToken,
        })
        const at = res.public.adminToken
        if (at) {
            CredentialsService.storeAdminCredentials(
                {
                    type: 'admin',
                    token: at.token,
                    email: at.email,
                },
                at.expires,
            )
            router.push({ pathname: '/admin' })
        } else {
            setIsAuthError(true)
        }
    })

    useEffect(() => {
        const handleCredentialResponse = (response: any) => {
            mutate(response.credential)
        }

        // Load google accounts API
        const elementId = 'googleApi'
        const existingScript = document.getElementById(elementId)
        if (!existingScript) {
            const script = document.createElement('script')
            script.src = 'https://accounts.google.com/gsi/client'
            script.id = elementId
            document.body.appendChild(script)
        }

        // Wait until library is loaded
        const timer = window.setInterval(() => {
            const identityApi = global.google?.accounts?.id
            if (identityApi) {
                // Show button
                window.clearInterval(timer)
                global.google.accounts.id.initialize({
                    client_id: googleClientId,
                    callback: handleCredentialResponse,
                })
                global.google.accounts.id.renderButton(document.getElementById('buttonDiv'), {
                    theme: 'outline',
                    size: 'large',
                })
            }
        }, 200)
    }, [mutate])

    return (
        <PageContentBox>
            {isAuthError && <Alert severity="error">{t('LoginPage.loginError')}</Alert>}
            <Typography>{t('LoginPage.instructions')}</Typography>
            <div id="buttonDiv" />
            {isLoading && (
                <Typography>
                    <CircularProgress size={12} sx={{ marginRight: '10px' }} />
                    {t('LoginPage.authenticating')}
                </Typography>
            )}
        </PageContentBox>
    )
}

export default AdminLoginContent
