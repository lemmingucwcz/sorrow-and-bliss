import React, { useEffect } from 'react'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import { useAdminCredentials } from 'src/client/context/AdminCredentialsContext'
import { CredentialsService } from '../../../services/CredentialsService'
import TopBar from '../../common/TopBar/TopBar'
import { useQueryClient } from 'react-query'
import { useGraphqlClient } from '../../../context/GraphqlClientContext'

const AdminTopBar = () => {
    const { t } = useTranslation('admin')
    const adminCredentials = useAdminCredentials()
    const router = useRouter()
    const queryClient = useQueryClient()
    const graphqlClient = useGraphqlClient()

    useEffect(() => {
        if (!adminCredentials.token) {
            // Redirect to login when user was logged out
            router.push({ pathname: '/loginAdmin' })
        }
    }, [router, adminCredentials])

    const handleLogout = async () => {
        CredentialsService.logoutAdmin()
        queryClient.clear()
        graphqlClient.setHeader('Authorization', '')
        await router.push({ pathname: '/loginAdmin' })
    }

    return (
        <TopBar onLogout={handleLogout}>
            {t('Admin.pageTitle', {
                email: adminCredentials.email,
            })}
        </TopBar>
    )
}

export default AdminTopBar
