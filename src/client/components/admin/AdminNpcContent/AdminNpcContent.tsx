import React, { useState } from 'react'
import { Character } from 'src/graphql/__generated__/typescript-operations'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import {
    CircularProgress,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
} from '@mui/material'
import { Create as CreateIcon, Delete as DeleteIcon } from '@mui/icons-material'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { useTranslation } from 'next-i18next'
import EditNpcDialog from './EditNpcDialog'
import { useFeedback } from '../../../hooks/useFeedback'
import DeleteNpcDialog from './DeleteNpcDialog'

const AdminNpcContent = () => {
    const [editingNpc, setEditingNpc] = useState<Character>()
    const [deletingNpc, setDeletingNpc] = useState<Character>()
    const characters = useCharactersAdmin()
    const feedback = useFeedback()
    const { t } = useTranslation('admin')

    const handleClose = (message?: string) => {
        if (message) {
            feedback(message)
        }
        setEditingNpc(undefined)
        setDeletingNpc(undefined)
    }

    return (
        <PageContentBox>
            {characters.list.length > 0 ? (
                <TableContainer component={Paper} sx={{ width: 'auto', textAlign: 'center' }}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>{t('NPCs.name')}</TableCell>
                                <TableCell />
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {characters.list
                                .filter(({ isNPC }) => isNPC)
                                .map(character => (
                                    <TableRow key={character.id}>
                                        <TableCell>{character.name}</TableCell>
                                        <TableCell>
                                            <Tooltip title={t('NPCs.edit') as string}>
                                                <IconButton
                                                    onClick={() => {
                                                        setEditingNpc(character)
                                                    }}
                                                >
                                                    <CreateIcon />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title={t('NPCs.delete') as string}>
                                                <IconButton
                                                    onClick={() => {
                                                        setDeletingNpc(character)
                                                    }}
                                                >
                                                    <DeleteIcon />
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : (
                <CircularProgress />
            )}
            {editingNpc ? <EditNpcDialog editNpc={editingNpc} onClose={handleClose} /> : null}
            {deletingNpc ? (
                <DeleteNpcDialog deleteNpc={deletingNpc} characters={characters.list} onClose={handleClose} />
            ) : null}
        </PageContentBox>
    )
}

export default AdminNpcContent
