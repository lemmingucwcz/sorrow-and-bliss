import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material'
import React from 'react'
import {
    Character,
    RenameNpcMutation,
    RenameNpcMutationVariables,
} from 'src/graphql/__generated__/typescript-operations'
import { useTranslation } from 'next-i18next'
import { Form } from 'react-final-form'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { useMutation, useQueryClient } from 'react-query'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { QueryKeys } from '../../../utils/queryKeys'
import { fieldValidator, validateRequired } from '../../../../utils/validationUtils'

interface Props {
    readonly editNpc: Character
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly name: string
}

const renameNpcMutation = require('./graphql/renameNpcMutation.graphql')

const EditNpcDialog = ({ editNpc, onClose }: Props) => {
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')
    const request = useGraphqlRequest()
    const client = useQueryClient()
    const { mutate, isLoading } = useMutation(async (values: FormShape) => {
        const res = await request<RenameNpcMutation, RenameNpcMutationVariables>(renameNpcMutation, {
            id: editNpc.id,
            name: values.name,
        })

        if (res.admin.renameNPC) {
            await client.invalidateQueries(QueryKeys.adminCharacters)
            onClose(t('NPCs.nameChanged'))
        }
    })

    const handleFormSubmit = (values: FormShape) => {
        mutate(values)
    }

    const initialValues: FormShape = {
        name: editNpc.name,
    }

    return (
        <Form<FormShape>
            onSubmit={handleFormSubmit}
            initialValues={initialValues}
            render={({ handleSubmit }) => (
                <Dialog open={true}>
                    <DialogTitle>{t('NPCs.editTitle')}</DialogTitle>
                    <DialogContent>
                        <FormFieldTextInput
                            sx={{ mt: '5px' }}
                            name="name"
                            autoFocus
                            label={t('NPCs.name')}
                            validate={fieldValidator(ct, validateRequired)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => onClose()} disabled={isLoading}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={() => handleSubmit()} variant="contained" disabled={isLoading}>
                            {t('NPCs.submitEdit')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        />
    )
}

export default EditNpcDialog
