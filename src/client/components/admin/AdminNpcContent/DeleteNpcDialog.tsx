import React, { useMemo } from 'react'
import {
    Character,
    CharacterLetterCountQuery,
    CharacterLetterCountQueryVariables,
    DeleteNpcMutation,
    DeleteNpcMutationVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import FormFieldCharacterAutocomplete, {
    CharacterAC,
} from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { useTranslation } from 'next-i18next'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import { Form } from 'react-final-form'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography } from '@mui/material'
import { fieldValidator, validateRequired } from '../../../../utils/validationUtils'

interface Props {
    readonly deleteNpc: Character
    readonly characters: CharacterAC[]
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly character?: CharacterAC
}

const letterCountQuery = require('./graphql/characterLetterCountQuery.graphql')
const deleteNpcMutation = require('./graphql/deleteNpcMutation.graphql')

const DeletNpcDialog = ({ deleteNpc, characters, onClose }: Props) => {
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')
    const request = useGraphqlRequest()
    const client = useQueryClient()
    const { mutate, isLoading } = useMutation(async (values: FormShape) => {
        const res = await request<DeleteNpcMutation, DeleteNpcMutationVariables>(deleteNpcMutation, {
            id: deleteNpc.id,
            replaceByNpc: values.character?.id || '0',
        })

        if (res.admin.deleteNPC) {
            await client.invalidateQueries(QueryKeys.adminCharacters)
            onClose(t('NPCs.deleted'))
        }
    })
    const { data } = useQuery(
        'letterCount',
        async () => {
            const res = await request<CharacterLetterCountQuery, CharacterLetterCountQueryVariables>(letterCountQuery, {
                characterId: deleteNpc.id,
            })
            return res.admin.characterLetterCount
        },
        { refetchOnMount: 'always' },
    )

    const handleFormSubmit = (values: FormShape) => {
        mutate(values)
    }

    const charList = useMemo(
        () => characters.filter(char => char.isNPC && char.id !== deleteNpc.id),
        [characters, deleteNpc],
    )

    return (
        <Form<FormShape>
            onSubmit={handleFormSubmit}
            render={({ handleSubmit }) => (
                <Dialog open={true}>
                    <DialogTitle>{t('NPCs.deleteTitle')}</DialogTitle>
                    {data !== undefined ? (
                        <DialogContent
                            sx={{
                                minHeight: '300px',
                                minWidth: '400px',
                                '& .MuiAutocomplete-listbox': {
                                    maxHeight: '200px',
                                },
                            }}
                        >
                            <Typography>
                                {t('NPCs.lettersConfirm', { letters: t('NPCs.letters', { count: data }) })}
                            </Typography>
                            <FormFieldCharacterAutocomplete
                                sx={{ mt: 2 }}
                                characters={charList}
                                name="character"
                                label={t('NPCs.transferLabel')}
                                validate={fieldValidator(ct, validateRequired)}
                                fullWidth
                            />
                        </DialogContent>
                    ) : null}
                    <DialogActions>
                        <Button onClick={() => onClose()} disabled={isLoading}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={() => handleSubmit()} variant="contained" disabled={isLoading}>
                            {t('NPCs.submitEdit')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        />
    )
}

export default DeletNpcDialog
