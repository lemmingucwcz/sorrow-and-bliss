import { useState } from 'react'
import {
    AdminCreateLetterMutation,
    AdminCreateLetterMutationVariables,
    AdminCreateLetterRequest,
    AdminNextSerialHintQuery,
    AdminNextSerialHintQueryVariables,
    AdminUpdateLetterMutation,
    AdminUpdateLetterMutationVariables,
    AdminUpdateLetterRequest,
    Letter,
} from '../../../../graphql/__generated__/typescript-operations'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { useTranslation } from 'next-i18next'
import { useMutation, useQueryClient } from 'react-query'
import FormFieldCharacterAutocomplete, {
    CharacterAC,
    createNpcId,
} from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { QueryKeys } from '../../../utils/queryKeys'
import { TFunction } from 'i18next'
import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@mui/material'
import { Form } from 'react-final-form'
import {
    fieldValidator,
    validateMaxFiles,
    validatePositiveInteger,
    validateRequired,
} from '../../../../utils/validationUtils'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import FormFieldSwitch from '../../common/FormFieldSwitch/FormFieldSwitch'
import CloseDialogButton from '../../common/CloseDialogButton/CloseDialogButton'
import FormFieldFiles from '../../common/FormFieldFiles/FormFieldFiles'
import { useAdminCredentials } from '../../../context/AdminCredentialsContext'
import { useFilesUploader } from '../../../hooks/useFilesUploader'
import FileUploadProgress from '../../common/FileUploadProgress/FileUploadProgress'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { getFileExtensions } from '../../../../utils/fileUtils'
import FormFieldSerialNumber from '../../common/FormFieldSerialNumber/FormFieldSerialNumber'

export type LetterToEdit = Pick<
    Letter,
    | 'id'
    | 'creatorId'
    | 'senderId'
    | 'recipientId'
    | 'serialNumber'
    | 'sentDate'
    | 'receivedDate'
    | 'orgComment'
    | 'numberOfFiles'
    | 'isEndOfLine'
>

interface Props {
    readonly runId: string
    readonly editLetter?: LetterToEdit
    readonly initialCreatorId?: string
    readonly initialRecipientId?: string
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly creator?: CharacterAC
    readonly newCreatorName: string
    readonly recipient?: CharacterAC
    readonly newRecipientName: string
    readonly serialNumber: string
    readonly sentDate: string
    readonly receivedDate: string
    readonly isAnonymous: boolean
    readonly isEndOfLine: boolean
    readonly orgComment: string
    readonly files: File[]
}

const newLetterInitialValues: FormShape = {
    sentDate: '',
    isAnonymous: false,
    isEndOfLine: false,
    newRecipientName: '',
    newCreatorName: '',
    receivedDate: '',
    orgComment: '',
    serialNumber: '',
    files: [],
}

const adminCreateLetterMutation = require('./adminCreateLetterMutation.graphql')
const adminUpdateLetterMutation = require('./adminUpdateLetterMutation.graphql')
const adminNextSerialHintQuery = require('./adminNextSerialHintQuery.graphql')

const formLevelValidate = (t: TFunction) => (values: FormShape) => {
    const res: Record<string, string | undefined> = {}

    if (!values.creator) {
        if (!values.newCreatorName) {
            res.newCreatorName = t('Errors.required')
        }
    }
    if (!values.recipient) {
        if (!values.newRecipientName) {
            res.newRecipientName = t('Errors.required')
        }
    }

    return res
}

const AdminLetterFormDialog = ({ runId, editLetter, initialCreatorId, initialRecipientId, onClose }: Props) => {
    const characters = useCharactersAdmin()
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')
    const gRequest = useGraphqlRequest()
    const queryClient = useQueryClient()
    const credentials = useAdminCredentials()
    const { progress, onBeforeUpdate, onUploadFiles, onFinish } = useFilesUploader()

    const [initialValues] = useState(() =>
        editLetter
            ? ({
                  sentDate: editLetter.sentDate,
                  receivedDate: editLetter.receivedDate,
                  serialNumber: editLetter.serialNumber.toString(),
                  newRecipientName: '',
                  newCreatorName: '',
                  recipient: characters.map[editLetter.recipientId],
                  creator: characters.map[editLetter.creatorId || ''],
                  isAnonymous: !editLetter.senderId,
                  isEndOfLine: editLetter.isEndOfLine,
                  orgComment: editLetter.orgComment,
                  files: [],
              } as FormShape)
            : {
                  ...newLetterInitialValues,
                  creator: initialCreatorId ? characters.map[initialCreatorId] : undefined,
                  recipient: initialRecipientId ? characters.map[initialRecipientId] : undefined,
              },
    )

    const { isLoading: isMutatingC, mutate: mutateC } = useMutation(
        async ({ request, files }: { request: AdminCreateLetterRequest; files: File[] }) => {
            const res = await gRequest<AdminCreateLetterMutation, AdminCreateLetterMutationVariables>(
                adminCreateLetterMutation,
                { runId, request },
            )
            if (res.admin.createLetter) {
                await queryClient.invalidateQueries(QueryKeys.adminDashboard)
                await queryClient.invalidateQueries(QueryKeys.adminAllLetters)
                await queryClient.invalidateQueries(QueryKeys.adminNextSerial)
                if (request.newCreatorName || request.newRecipientName) {
                    // Refresh characters when we have created a new NPC
                    await queryClient.invalidateQueries(QueryKeys.adminCharacters)
                }

                // Updated - upload files
                await onUploadFiles({
                    token: credentials.token,
                    letterId: res.admin.createLetter,
                    runId,
                    files,
                })
                onClose(t('LetterForm.letterCreated'))
            }
            onFinish()
        },
    )

    const { isLoading: isMutatingU, mutate: mutateU } = useMutation(
        async ({ request, files }: { request: AdminUpdateLetterRequest; files: File[] }) => {
            const res = await gRequest<AdminUpdateLetterMutation, AdminUpdateLetterMutationVariables>(
                adminUpdateLetterMutation,
                { runId, request },
            )
            if (res.admin.updateLetter) {
                await queryClient.invalidateQueries(QueryKeys.adminDashboard)
                await queryClient.invalidateQueries(QueryKeys.adminAllLetters)
                await queryClient.invalidateQueries(QueryKeys.adminNextSerial)
                if (request.newCreatorName || request.newRecipientName) {
                    // Refresh characters when we have created a new NPC
                    await queryClient.invalidateQueries(QueryKeys.adminCharacters)
                }
                // Updated - upload files
                await onUploadFiles({
                    token: credentials.token,
                    letterId: request.letterId,
                    runId,
                    files,
                })
                onClose(t('LetterForm.letterUpdated'))
            }
            onFinish()
        },
    )

    const handleFormSubmit = (values: FormShape) => {
        const creatorId = values.creator?.id || '0'
        const recipientId = values.recipient?.id || '0'
        const { files } = values
        onBeforeUpdate(files)
        if (editLetter) {
            return mutateU({
                request: {
                    creatorId: parseInt(creatorId) > 0 ? creatorId : undefined,
                    fileExtensions: files.length > 0 ? getFileExtensions(files) : undefined,
                    isAnonymous: values.isAnonymous,
                    isEndOfLine: values.isEndOfLine,
                    letterId: editLetter.id,
                    newCreatorName: values.newCreatorName || undefined,
                    newRecipientName: values.newRecipientName || undefined,
                    orgComment: values.orgComment,
                    receivedDate: values.receivedDate || '',
                    recipientId: parseInt(recipientId) > 0 ? recipientId : undefined,
                    sentDate: values.sentDate,
                    // Keep old files when none are provided during updated
                    serialNumber: parseInt(values.serialNumber),
                },
                files,
            })
        } else {
            return mutateC({
                request: {
                    creatorId: parseInt(creatorId) > 0 ? creatorId : undefined,
                    newCreatorName: values.newCreatorName,
                    recipientId: parseInt(recipientId) > 0 ? recipientId : undefined,
                    newRecipientName: values.newRecipientName,
                    sentDate: values.sentDate,
                    serialNumber: parseInt(values.serialNumber),
                    isAnonymous: values.isAnonymous,
                    orgComment: values.orgComment,
                    fileExtensions: getFileExtensions(files),
                },
                files,
            })
        }
    }

    const handleClose = () => onClose()

    const isMutating = isMutatingC || isMutatingU

    return (
        <Dialog open={true}>
            <Form<FormShape>
                onSubmit={handleFormSubmit}
                initialValues={initialValues}
                validate={formLevelValidate(t)}
                render={({ handleSubmit, form, values }) => {
                    const senderId = values.creator?.id || ''
                    const recipientId = values.recipient?.id || ''
                    const hintQueryFn = async () => {
                        const res = await gRequest<AdminNextSerialHintQuery, AdminNextSerialHintQueryVariables>(
                            adminNextSerialHintQuery,
                            {
                                runId,
                                senderId,
                                recipientId,
                            },
                        )
                        return res.admin.nextLetterSerial
                    }
                    const queryKey = [QueryKeys.adminNextSerial, runId, senderId, recipientId]

                    return (
                        <>
                            <DialogTitle>{t(editLetter ? 'LetterForm.titleEdit' : 'LetterForm.titleNew')}</DialogTitle>
                            <DialogContent
                                sx={{
                                    '& .MuiAutocomplete-listbox': {
                                        maxHeight: '200px',
                                    },
                                }}
                            >
                                <form onSubmit={handleSubmit} style={{ paddingTop: '5px' }}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <FormFieldCharacterAutocomplete
                                                characters={characters.list}
                                                name="creator"
                                                label={t('Letter.sender')}
                                                fullWidth
                                                allowCreating="sender"
                                                validate={fieldValidator(ct, validateRequired)}
                                                autoFocus
                                            />
                                        </Grid>
                                        {values.creator?.id === createNpcId ? (
                                            <Grid item xs={12}>
                                                <FormFieldTextInput
                                                    name="newCreatorName"
                                                    label={t('LetterForm.newCreatorName')}
                                                    fullWidth
                                                />
                                            </Grid>
                                        ) : null}
                                        <Grid item xs={12}>
                                            <FormFieldCharacterAutocomplete
                                                characters={characters.list}
                                                name="recipient"
                                                label={t('Letter.recipient')}
                                                fullWidth
                                                allowCreating="recipient"
                                                validate={fieldValidator(ct, validateRequired)}
                                            />
                                        </Grid>
                                        {values.recipient?.id === createNpcId ? (
                                            <Grid item xs={12}>
                                                <FormFieldTextInput
                                                    name="newRecipientName"
                                                    label={t('LetterForm.newRecipientName')}
                                                    fullWidth
                                                />
                                            </Grid>
                                        ) : null}
                                        <Grid item xs={12}>
                                            <FormFieldSwitch name="isAnonymous" label={t('LetterForm.sendAnonymous')} />
                                        </Grid>
                                        {Boolean(editLetter) && (
                                            <Grid item xs={12}>
                                                <FormFieldSwitch
                                                    name="isEndOfLine"
                                                    label={t('LetterForm.isEndOfLine')}
                                                />
                                            </Grid>
                                        )}
                                        <Grid item xs={12}>
                                            <FormFieldSerialNumber
                                                name="serialNumber"
                                                label={t('Letter.serialNumber')}
                                                queryFn={hintQueryFn}
                                                queryEnabled={Boolean(values.creator) && Boolean(values.recipient)}
                                                queryKey={queryKey}
                                                isAnonymous={values.isAnonymous}
                                                isEditing={Boolean(editLetter)}
                                                validate={fieldValidator(ct, [
                                                    validateRequired,
                                                    validatePositiveInteger,
                                                ])}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormFieldTextInput
                                                name="sentDate"
                                                label={t('Letter.sentDate')}
                                                type="date"
                                                fullWidth
                                                validate={fieldValidator(ct, validateRequired)}
                                            />
                                        </Grid>
                                        {editLetter ? (
                                            <Grid item xs={12}>
                                                <FormFieldTextInput
                                                    name="receivedDate"
                                                    label={t('Letter.receivedDate')}
                                                    type="date"
                                                    fullWidth
                                                />
                                            </Grid>
                                        ) : null}
                                        <Grid item xs={12}>
                                            <FormFieldFiles
                                                name="files"
                                                label={t('LetterForm.filesLabel')}
                                                placeholder={t('LetterForm.filesPlaceholder')}
                                                validate={fieldValidator(
                                                    ct,
                                                    editLetter
                                                        ? validateMaxFiles(25)
                                                        : [validateRequired, validateMaxFiles(25)],
                                                )}
                                                hint={
                                                    editLetter
                                                        ? ct(
                                                              values.files.length > 0
                                                                  ? 'FileUploadHint.hasFiles'
                                                                  : 'FileUploadHint.noFiles',
                                                          )
                                                        : undefined
                                                }
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormFieldTextInput
                                                name="orgComment"
                                                label={t('Letter.orgComment')}
                                                multiline
                                                fullWidth
                                            />
                                        </Grid>
                                    </Grid>
                                </form>
                                <FileUploadProgress progress={progress} />
                            </DialogContent>
                            <DialogActions>
                                <CloseDialogButton disabled={isMutating} onClick={handleClose} />
                                <Button
                                    variant="contained"
                                    onClick={() => {
                                        form.submit()
                                    }}
                                    disabled={isMutating}
                                    startIcon={isMutating ? <CircularProgress /> : null}
                                >
                                    {t(editLetter ? 'LetterForm.submitEdit' : 'LetterForm.submitNew')}
                                </Button>
                            </DialogActions>
                        </>
                    )
                }}
            />
        </Dialog>
    )
}

export default AdminLetterFormDialog
