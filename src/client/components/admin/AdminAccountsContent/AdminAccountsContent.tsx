import React, { useState } from 'react'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { QueryKeys } from 'src/client/utils/queryKeys'
import {
    AdminAccount,
    AdminAccountsQuery,
    AdminAccountsQueryVariables,
    DeleteAdminAccountMutation,
    DeleteAdminAccountMutationVariables,
} from 'src/graphql/__generated__/typescript-operations'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import {
    Button,
    CircularProgress,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import { AddCircle as AddCircleIcon, Delete as DeleteIcon } from '@mui/icons-material'
import { useFeedback } from '../../../hooks/useFeedback'
import { useShowConfirmDialog } from '../../../context/ConfirmDialogContext'
import NewAdminAccountDialog from './NewAdminAccountDialog'
import { useAdminCredentials } from '../../../context/AdminCredentialsContext'

const adminAccountsQuery = require('./graphql/adminAccountsQuery.graphql')
const deleteAdminAccountMutation = require('./graphql/deleteAdminAccount.graphql')

const AdminAccountsContext = () => {
    const feedback = useFeedback()
    const queryClient = useQueryClient()
    const request = useGraphqlRequest()
    const credentials = useAdminCredentials()
    const { t } = useTranslation('admin')
    const { data } = useQuery(QueryKeys.adminAccounts, async () => {
        const res = await request<AdminAccountsQuery, AdminAccountsQueryVariables>(adminAccountsQuery)
        return res.admin.adminAccounts
    })
    const { mutate } = useMutation(async (id: string) => {
        const res = await request<DeleteAdminAccountMutation, DeleteAdminAccountMutationVariables>(
            deleteAdminAccountMutation,
            { id },
        )
        if (res.admin.deleteAdminAccount) {
            feedback(t('Admins.deleted'))
            await queryClient.invalidateQueries(QueryKeys.adminAccounts)
        }
    })
    const [creatingNew, setCreatingNew] = useState(false)
    const showConfirmDialog = useShowConfirmDialog()

    const handleCreateNew = () => {
        setCreatingNew(true)
    }

    const handleClose = (message?: string) => {
        setCreatingNew(false)
        if (message) {
            feedback(message)
        }
    }

    const handleDeleteAccount = async (account: AdminAccount) => {
        const response = await showConfirmDialog({
            text: t('Admins.deleteConfirm.question', { email: account.email }),
            confirmText: t('Admins.deleteConfirm.button'),
        })
        if (response === 'confirm') {
            mutate(account.id)
        }
    }

    return (
        <PageContentBox>
            {data ? (
                <>
                    <Button
                        variant="contained"
                        sx={{ margin: '20px 0' }}
                        onClick={handleCreateNew}
                        startIcon={<AddCircleIcon />}
                    >
                        {t('Admins.addNew')}
                    </Button>
                    <TableContainer component={Paper} sx={{ width: 'auto', textAlign: 'center' }}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>{t('Admins.email')}</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map(admin => {
                                    const self = admin.email === credentials.email
                                    return (
                                        <TableRow key={admin.id}>
                                            <TableCell>{admin.email}</TableCell>
                                            <TableCell>
                                                <Tooltip title={t('Admins.delete') as string}>
                                                    <IconButton
                                                        onClick={() => handleDeleteAccount(admin)}
                                                        disabled={self}
                                                    >
                                                        <DeleteIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {creatingNew ? <NewAdminAccountDialog onClose={handleClose} /> : null}
                </>
            ) : (
                <CircularProgress />
            )}
        </PageContentBox>
    )
}

export default AdminAccountsContext
