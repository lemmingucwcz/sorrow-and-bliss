import React from 'react'
import { Form } from 'react-final-form'
import { useMutation, useQueryClient } from 'react-query'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'
import {
    AddAdminAccountMutation,
    AddAdminAccountMutationVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import { QueryKeys } from '../../../utils/queryKeys'
import { useTranslation } from 'next-i18next'
import { Alert, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { fieldValidator, validateEmail, validateRequired } from '../../../../utils/validationUtils'

interface Props {
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly email: string
}

const addAdminAccountMutation = require('./graphql/addAdminAccountMutation.graphql')

const NewAdminAccountDialog = ({ onClose }: Props) => {
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const { mutate, isLoading } = useMutation(async (values: FormShape) => {
        const res = await request<AddAdminAccountMutation, AddAdminAccountMutationVariables>(addAdminAccountMutation, {
            email: values.email,
        })
        if (res.admin.addAdminAccount) {
            await queryClient.invalidateQueries(QueryKeys.adminAccounts)
            onClose(t('Admins.new.created'))
        }
    })

    return (
        <Form<FormShape>
            onSubmit={values => mutate(values)}
            render={({ handleSubmit }) => (
                <Dialog open={true}>
                    <DialogTitle>{t('Admins.new.title')}</DialogTitle>
                    <DialogContent>
                        <Alert severity="warning">{t('Admins.new.googleWarning')}</Alert>
                        <FormFieldTextInput
                            sx={{ mt: 2 }}
                            name="email"
                            label={t('Admins.email')}
                            type="email"
                            autoFocus
                            fullWidth
                            validate={fieldValidator(ct, [validateRequired, validateEmail])}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => onClose()} disabled={isLoading}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={handleSubmit} variant="contained" disabled={isLoading}>
                            {t('Admins.new.submit')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        />
    )
}

export default NewAdminAccountDialog
