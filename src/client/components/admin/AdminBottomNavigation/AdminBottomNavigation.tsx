import React from 'react'
import SBBottomNavigation from '../../common/SBBottomNavigation/SBBottomNavigation'
import { useTranslation } from 'next-i18next'
import {
    Dashboard as DashboardIcon,
    Email as EmailIcon,
    Lock as LockIcon,
    Person as PersonIcon,
    Update as UpdateIcon,
} from '@mui/icons-material'

const AdminBottomNavigation = () => {
    const { t } = useTranslation('admin')

    return (
        <SBBottomNavigation
            actions={[
                {
                    label: t('Navigation.dashboard'),
                    icon: <DashboardIcon />,
                    route: '/admin',
                },
                {
                    label: t('Navigation.letters'),
                    icon: <EmailIcon />,
                    route: '/admin/letters',
                },
                {
                    label: t('Navigation.npcs'),
                    icon: <PersonIcon />,
                    route: '/admin/npcs',
                },
                {
                    label: t('Navigation.runs'),
                    icon: <UpdateIcon />,
                    route: '/admin/runs',
                },
                {
                    label: t('Navigation.admins'),
                    icon: <LockIcon />,
                    route: '/admin/admins',
                },
            ]}
        />
    )
}

export default AdminBottomNavigation
