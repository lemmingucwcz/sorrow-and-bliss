import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@mui/material'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { Form } from 'react-final-form'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { fieldValidator, validateRequired } from '../../../../utils/validationUtils'

interface Props {
    readonly initialOrgComment?: string
    readonly onClose: () => void
    readonly onConfirmed: (receivedDate: string, orgComment: string) => void
}

interface FormShape {
    readonly receivedDate: string
    readonly orgComment: string
}

const ConfirmLetterDialog = ({ initialOrgComment, onClose, onConfirmed }: Props) => {
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')

    const handleFormSubmit = ({ receivedDate, orgComment }: FormShape) => {
        if (receivedDate.length > 0) {
            onConfirmed(receivedDate, orgComment)
        }
    }

    return (
        <Form<FormShape>
            onSubmit={handleFormSubmit}
            initialValues={{ receivedDate: '', orgComment: initialOrgComment }}
            render={({ handleSubmit }) => (
                <Dialog open>
                    <DialogTitle>{t('ConfirmLetterDialog.title')}</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <FormFieldTextInput
                                    name="receivedDate"
                                    label={t('Letter.receivedDate')}
                                    fullWidth
                                    validate={fieldValidator(ct, validateRequired)}
                                    type="date"
                                    sx={{ marginTop: '5px' }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormFieldTextInput
                                    name="orgComment"
                                    label={t('Letter.orgComment')}
                                    fullWidth
                                    multiline
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose}>{t('cancel')}</Button>
                        <Button onClick={handleSubmit}>{t('ConfirmLetterDialog.confirm')}</Button>
                    </DialogActions>
                </Dialog>
            )}
        />
    )
}

export default ConfirmLetterDialog
