import React from 'react'
import { Letter } from 'src/graphql/__generated__/typescript-operations'
import { useTranslation } from 'next-i18next'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import { formatDate } from '../../../utils/dateUtils'
import LetterSenderCell from '../LetterSenderCell/LetterSenderCell'
import LettersAccordion from './LettersAccordion'

interface Props {
    readonly letters: Array<
        Pick<Letter, 'id' | 'creatorId' | 'senderId' | 'recipientId' | 'serialNumber' | 'sentDate' | 'receivedDate'>
    >
}

const AdminLongPendingLetters = ({ letters }: Props) => {
    const { t } = useTranslation('admin')
    const characters = useCharactersAdmin()

    return (
        <LettersAccordion summary={t('Dashboard.longPendingLetters', { count: letters.length })}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{t('Letter.sender')}</TableCell>
                        <TableCell>{t('Letter.recipient')}</TableCell>
                        <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                        <TableCell>{t('Letter.sentDateShort')}</TableCell>
                        <TableCell>{t('Letter.receivedDateShort')}</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {letters.map(letter => (
                        <TableRow key={letter.id}>
                            <LetterSenderCell letter={letter} characters={characters.map} />
                            <TableCell>{characters.map[letter.recipientId]?.name}</TableCell>
                            <TableCell>{letter.serialNumber}</TableCell>
                            <TableCell>{formatDate(letter.sentDate)}</TableCell>
                            <TableCell>{formatDate(letter.receivedDate)}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </LettersAccordion>
    )
}

export default AdminLongPendingLetters
