import React, { useState } from 'react'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import AdminRunSelect from '../AdminRunSelect/AdminRunSelect'
import { useQuery } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import {
    AdminDashboardQuery,
    AdminDashboardQueryVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import { Button, Chip, CircularProgress, Divider, Paper, Tooltip, Typography } from '@mui/material'
import MailIcon from '@mui/icons-material/Mail'
import LocalShippingIcon from '@mui/icons-material/LocalShipping'
import { useTranslation } from 'next-i18next'
import AdminIncomingLetters from './AdminIncomingLetters'
import AdminPendingLetters from './AdminPendingLetters'
import AdminLetterFormDialog from '../AdminLetterFormDialog/AdminLetterFormDialog'
import { AddCircle as AddCircleIcon, HourglassFull as HourglassFullIcon } from '@mui/icons-material'
import AdminLongUndeliveredLetters from './AdminLongUndeliveredLetters'
import AdminLongPendingLetters from './AdminLongPendingLetters'
import { useFeedback } from 'src/client/hooks/useFeedback'
import { useGraphqlRequest } from 'src/client/hooks/useGraphqlRequest'
import getConfig from 'next/config'

const adminDashboardQuery = require('./adminDashboardQuery.graphql')

const { publicRuntimeConfig } = getConfig()
const daysLimitUnconfirmed = parseInt(publicRuntimeConfig.LETTER_AGE_LONG_UNCONFIRMED)
const daysLimitPending = parseInt(publicRuntimeConfig.LETTER_AGE_LONG_UNANSWERED)

interface CreatingState {
    readonly creatorId?: string
    readonly recipientId?: string
}

const AdminDashboardContent = () => {
    const [runId, setRunId] = useState('')
    const { t } = useTranslation('admin')
    const [creatingLetter, setCreatingLetter] = useState<CreatingState>()
    const feedback = useFeedback()
    const request = useGraphqlRequest()
    const { data } = useQuery(
        [QueryKeys.adminDashboard, runId],
        async () => {
            const res = await request<AdminDashboardQuery, AdminDashboardQueryVariables>(adminDashboardQuery, {
                runId,
                daysLimitUnconfirmed,
                daysLimitPending,
            })
            return res.admin
        },
        { enabled: Boolean(runId) },
    )

    const handleCreateLetter = (creatorId?: string, recipientId?: string) => {
        setCreatingLetter({ creatorId, recipientId })
    }

    const handleDialogClose = (message?: string) => {
        feedback(message || '')
        setCreatingLetter(undefined)
    }

    return (
        <PageContentBox>
            <AdminRunSelect runId={runId} setRunId={setRunId} />

            {runId && !data && <CircularProgress />}
            {runId && data && (
                <>
                    <Paper sx={{ display: 'flex', gap: '10px', marginTop: '20px', padding: '10px' }}>
                        <Tooltip title={t('Dashboard.lettersTotal') as string}>
                            <Chip label={data.letterStats.lettersTotal} icon={<MailIcon />} />
                        </Tooltip>
                        <Tooltip title={t('Dashboard.lettersInTransit') as string}>
                            <Chip label={data.letterStats.lettersInTransit} icon={<LocalShippingIcon />} />
                        </Tooltip>
                        <Tooltip title={t('Dashboard.lettersWaitingForResponse') as string}>
                            <Chip label={data.letterStats.lettersWaitingForResponse} icon={<HourglassFullIcon />} />
                        </Tooltip>
                    </Paper>

                    <AdminLongUndeliveredLetters letters={data.pcLongUnconfirmedLetters} runId={runId} />
                    <AdminLongPendingLetters letters={data.pcLongPendingLetters} />
                </>
            )}
            {data && (
                <>
                    <Divider variant="middle" sx={{ width: '100%', mb: 1 }} />
                    <Button onClick={() => handleCreateLetter()} startIcon={<AddCircleIcon />} variant="contained">
                        {t('Dashboard.createLetter')}
                    </Button>
                    <Typography variant="h6" sx={{ marginTop: '20px' }}>
                        {t('Dashboard.incomingLetters')}
                    </Typography>
                    <AdminIncomingLetters runId={runId} letters={data.incomingLetters} />
                    <Typography variant="h6" sx={{ marginTop: '20px' }}>
                        {t('Dashboard.pendingLetters')}
                    </Typography>
                    <AdminPendingLetters
                        runId={runId}
                        letters={data.pendingLetters}
                        onCreateAnswer={handleCreateLetter}
                    />
                </>
            )}
            {!data && <CircularProgress sx={{ margin: '20px auto' }} />}
            {creatingLetter ? (
                <AdminLetterFormDialog
                    runId={runId}
                    onClose={handleDialogClose}
                    initialCreatorId={creatingLetter.creatorId}
                    initialRecipientId={creatingLetter.recipientId}
                />
            ) : null}
        </PageContentBox>
    )
}

export default AdminDashboardContent
