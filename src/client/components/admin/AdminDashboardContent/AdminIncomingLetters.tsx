import React, { useState } from 'react'
import {
    AdminConfirmLetterMutation,
    AdminConfirmLetterMutationVariables,
    Letter,
} from 'src/graphql/__generated__/typescript-operations'
import {
    Alert,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { formatDate } from '../../../utils/dateUtils'
import { useMutation, useQueryClient } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import { AssignmentTurnedIn as AssignmentTurnedIcon } from '@mui/icons-material'
import ConfirmLetterDialog from './ConfirmLetterDialog'
import { useGraphqlRequest } from 'src/client/hooks/useGraphqlRequest'

interface Props {
    readonly runId: string
    readonly letters: Array<Pick<Letter, 'id' | 'senderId' | 'creatorId' | 'recipientId' | 'serialNumber' | 'sentDate'>>
}

const adminConfirmLetterMutation = require('./adminConfirmLetterMutation.graphql')

const AdminIncomingLetters = ({ runId, letters }: Props) => {
    const { t } = useTranslation('admin')
    const characters = useCharactersAdmin()
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const [confirmLetter, setConfirmLetter] = useState<Pick<Letter, 'id' | 'orgComment'>>()
    const { mutate } = useMutation(
        async ({
            letterId,
            receivedDate,
            orgComment,
        }: {
            letterId: string
            receivedDate: string
            orgComment: string
        }) => {
            const res = await request<AdminConfirmLetterMutation, AdminConfirmLetterMutationVariables>(
                adminConfirmLetterMutation,
                {
                    runId,
                    letterId,
                    request: {
                        receivedDate,
                        orgComment: orgComment || undefined,
                    },
                },
            )

            if (res.admin.confirmLetter) {
                await queryClient.invalidateQueries(QueryKeys.adminDashboard)
                setConfirmLetter(undefined)
            }
        },
    )

    const handleDialogClose = () => {
        setConfirmLetter(undefined)
    }

    const handleDialogConfirm = (receivedDate: string, orgComment: string) => {
        mutate({
            letterId: confirmLetter?.id as string,
            receivedDate,
            orgComment,
        })
    }

    if (letters.length === 0) {
        return <Alert severity="info">{t('Dashboard.noIncomingLetters')}</Alert>
    }

    return (
        <TableContainer component={Paper} sx={{ width: 'auto' }}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{t('Letter.sender')}</TableCell>
                        <TableCell>{t('Letter.recipient')}</TableCell>
                        <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                        <TableCell>{t('Letter.sentDateShort')}</TableCell>
                        <TableCell />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {letters.map(letter => (
                        <TableRow key={letter.id}>
                            <TableCell sx={{ opacity: letter.senderId ? 1 : 0.4 }}>
                                {characters.map[letter.creatorId || '']?.name}
                            </TableCell>
                            <TableCell>{characters.map[letter.recipientId]?.name}</TableCell>
                            <TableCell>{letter.serialNumber}</TableCell>
                            <TableCell>{formatDate(letter.sentDate)}</TableCell>
                            <TableCell>
                                <Tooltip title={t('Dashboard.confirmIncomingLetter') as string}>
                                    <IconButton
                                        onClick={() => {
                                            setConfirmLetter(letter)
                                        }}
                                    >
                                        <AssignmentTurnedIcon />
                                    </IconButton>
                                </Tooltip>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            {confirmLetter ? (
                <ConfirmLetterDialog
                    initialOrgComment={confirmLetter.orgComment || ''}
                    onClose={handleDialogClose}
                    onConfirmed={handleDialogConfirm}
                />
            ) : null}
        </TableContainer>
    )
}

export default AdminIncomingLetters
