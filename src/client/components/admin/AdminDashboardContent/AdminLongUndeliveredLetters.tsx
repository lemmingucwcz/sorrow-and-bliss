import React from 'react'
import { Letter } from '../../../../graphql/__generated__/typescript-operations'
import { useTranslation } from 'next-i18next'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import LetterSenderCell from '../LetterSenderCell/LetterSenderCell'
import { formatDate } from '../../../utils/dateUtils'
import DownloadPagesPopupMenu from '../../common/DownloadPagesPopupMenu/DownloadPagesPopupMenu'
import { useAdminGetDownloadTokens } from '../../../hooks/useAdminGetDownloadTokens'
import LettersAccordion from './LettersAccordion'

interface Props {
    readonly letters: Array<
        Pick<
            Letter,
            | 'id'
            | 'creatorId'
            | 'senderId'
            | 'recipientId'
            | 'serialNumber'
            | 'sentDate'
            | 'receivedDate'
            | 'numberOfFiles'
        >
    >
    readonly runId: string
}

const AdminLongUndeliveredLetters = ({ letters, runId }: Props) => {
    const { t } = useTranslation('admin')
    const characters = useCharactersAdmin()
    const adminGetTokens = useAdminGetDownloadTokens()

    return (
        <LettersAccordion summary={t('Dashboard.longUndeliveredLetters', { count: letters.length })}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{t('Letter.sender')}</TableCell>
                        <TableCell>{t('Letter.recipient')}</TableCell>
                        <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                        <TableCell>{t('Letter.sentDateShort')}</TableCell>
                        <TableCell />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {letters.map(letter => (
                        <TableRow key={letter.id}>
                            <LetterSenderCell letter={letter} characters={characters.map} />
                            <TableCell>{characters.map[letter.recipientId]?.name}</TableCell>
                            <TableCell>{letter.serialNumber}</TableCell>
                            <TableCell>{formatDate(letter.sentDate)}</TableCell>
                            <TableCell>
                                {letter.numberOfFiles > 0 ? (
                                    <DownloadPagesPopupMenu onGetTokens={() => adminGetTokens(runId, letter.id)} />
                                ) : null}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </LettersAccordion>
    )
}

export default AdminLongUndeliveredLetters
