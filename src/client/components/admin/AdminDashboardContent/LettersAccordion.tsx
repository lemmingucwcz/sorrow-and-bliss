import React from 'react'
import { Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material'

interface Props {
    readonly summary: string
}

const LettersAccordion: React.FC<Props> = ({ summary, children }) => (
    <Accordion sx={{ margin: '10px 0' }}>
        <AccordionSummary>
            <Typography>{summary}</Typography>
        </AccordionSummary>
        <AccordionDetails>{children}</AccordionDetails>
    </Accordion>
)

export default LettersAccordion
