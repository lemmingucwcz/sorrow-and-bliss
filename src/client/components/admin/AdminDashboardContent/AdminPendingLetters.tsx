import React from 'react'
import {
    AdminEndLetterLineMutation,
    AdminEndLetterLineMutationVariables,
    Letter,
} from '../../../../graphql/__generated__/typescript-operations'
import {
    Alert,
    Chip,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import { formatDate, getAgeColor, letterAge } from '../../../utils/dateUtils'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import {
    AccessTime as AccessTimeIcon,
    CancelScheduleSend as CancelScheduleSendIcon,
    Reply as ReplyIcon,
} from '@mui/icons-material'
import { useMutation, useQueryClient } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import { useShowConfirmDialog } from '../../../context/ConfirmDialogContext'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

interface Props {
    readonly runId: string
    readonly letters: Array<
        Pick<Letter, 'id' | 'creatorId' | 'senderId' | 'recipientId' | 'serialNumber' | 'sentDate' | 'receivedDate'>
    >
    readonly onCreateAnswer: (creatorId: string, recipientId: string) => void
}

const adminEndLetterLineMutation = require('./adminEndLetterLineMutation.graphql')

const AdminPendingLetters = ({ runId, letters, onCreateAnswer }: Props) => {
    const { t } = useTranslation('admin')
    const characters = useCharactersAdmin()
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const showConfirmDialog = useShowConfirmDialog()
    const { mutate } = useMutation(async (letterId: string) => {
        const res = await request<AdminEndLetterLineMutation, AdminEndLetterLineMutationVariables>(
            adminEndLetterLineMutation,
            { letterId, runId },
        )

        if (res.admin.endLetterLine) {
            await queryClient.invalidateQueries(QueryKeys.adminDashboard)
        }
    })

    if (letters.length === 0) {
        return <Alert severity="success">{t('Dashboard.noPendingLetters')}</Alert>
    }

    const handleEndLetterLine = async (letterId: string) => {
        const res = await showConfirmDialog({
            text: t('Dashboard.confirmEndLetterLine'),
            confirmText: t('Dashboard.confirmEndLetterLineConfirm'),
        })

        if (res === 'confirm') {
            mutate(letterId)
        }
    }

    return (
        <TableContainer component={Paper} sx={{ width: 'auto' }}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{t('Letter.sender')}</TableCell>
                        <TableCell>{t('Letter.recipient')}</TableCell>
                        <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                        <TableCell>{t('Letter.sentDateShort')}</TableCell>
                        <TableCell>{t('Letter.receivedDateShort')}</TableCell>
                        <TableCell />
                        <TableCell />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {letters.map(letter => {
                        const age = letterAge(letter.receivedDate)
                        return (
                            <TableRow key={letter.id}>
                                <TableCell sx={{ opacity: letter.senderId ? 1 : 0.4 }}>
                                    {characters.map[letter.creatorId || '']?.name}
                                </TableCell>
                                <TableCell>{characters.map[letter.recipientId]?.name}</TableCell>
                                <TableCell>{letter.serialNumber}</TableCell>
                                <TableCell>{formatDate(letter.sentDate)}</TableCell>
                                <TableCell>{formatDate(letter.receivedDate)}</TableCell>
                                <TableCell>
                                    <Chip label={age} icon={<AccessTimeIcon />} color={getAgeColor(age)} />
                                </TableCell>
                                <TableCell>
                                    <Tooltip title={t('Dashboard.createReply') as string}>
                                        <IconButton
                                            onClick={() => onCreateAnswer(letter.recipientId, letter.creatorId || '')}
                                        >
                                            <ReplyIcon />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title={t('Dashboard.endLetterLine') as string}>
                                        <IconButton onClick={() => handleEndLetterLine(letter.id)}>
                                            <CancelScheduleSendIcon />
                                        </IconButton>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default AdminPendingLetters
