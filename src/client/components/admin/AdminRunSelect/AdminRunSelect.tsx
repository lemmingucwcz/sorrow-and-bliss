import React, { useEffect } from 'react'
import { useAdminRuns } from '../../../hooks/useAdminRuns'
import { FormControl, InputLabel, Select } from '@mui/material'
import { useTranslation } from 'next-i18next'
import { SelectChangeEvent } from '@mui/material/Select/SelectInput'

interface Props {
    readonly runId: string
    readonly setRunId: (runId: string) => void
}

const runIdKey = 'ADMIN_RUN_ID'

const AdminRunSelect = ({ runId, setRunId }: Props) => {
    const runs = useAdminRuns()
    const { t } = useTranslation('admin')

    useEffect(() => {
        if (!runId && runs.length > 0) {
            // Check for stored run id
            const storedRunId = localStorage.getItem(runIdKey)
            const selectedRun = runs.find(run => run.id === storedRunId)
            if (selectedRun) {
                // Found previously selected run - set its id
                setRunId(selectedRun.id)
                return
            }

            // Find active run id
            const firstActiveRun = runs.find(run => run.isActive)
            if (firstActiveRun) {
                localStorage.setItem(runIdKey, firstActiveRun.id)
                setRunId(firstActiveRun.id)
            }
        }
    }, [runs, runId, setRunId])

    const inactiveRuns = runs.filter(run => !run.isActive)
    const activeRuns = runs.filter(run => run.isActive)

    const handleChange = (event: SelectChangeEvent) => {
        const value = event.target.value
        localStorage.setItem(runIdKey, value)
        setRunId(value)
    }

    return (
        <FormControl>
            <InputLabel htmlFor="adminRunSelect">{t('selectRun')}</InputLabel>
            <Select
                id="adminRunSelect"
                native
                value={runId}
                onChange={handleChange}
                label={t('selectRun')}
                sx={{ minWidth: '200px' }}
            >
                {activeRuns.length > 0 ? (
                    <optgroup label={t('activeRuns')}>
                        {runs
                            .filter(run => run.isActive)
                            .map(run => (
                                <option key={run.id} value={run.id}>
                                    {run.name}
                                </option>
                            ))}
                    </optgroup>
                ) : null}

                {inactiveRuns.length > 0 ? (
                    <optgroup label={t('inactiveRuns')}>
                        {inactiveRuns.map(run => (
                            <option key={run.id} value={run.id}>
                                {run.name}
                            </option>
                        ))}
                    </optgroup>
                ) : null}
            </Select>
        </FormControl>
    )
}

export default AdminRunSelect
