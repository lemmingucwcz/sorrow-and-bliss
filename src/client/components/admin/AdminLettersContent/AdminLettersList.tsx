import React from 'react'
import {
    AdminLettersQuery,
    AdminLettersQueryVariables,
    LetterDirection,
    LetterState,
} from '../../../../graphql/__generated__/typescript-operations'
import { useInfiniteQuery } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import {
    Alert,
    Button,
    CircularProgress,
    Paper,
    Table,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import AdminLettersTableBody from './AdminLettersTableBody'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

interface Props {
    readonly character1Id?: string
    readonly character2Id?: string
    readonly runId: string
    readonly letterState: LetterState
    readonly letterDirection: LetterDirection
}

const adminLettersQuery = require('./adminLettersQuery.graphql')

const PAGE_SIZE = 20

const AdminLettersList = ({ character1Id, character2Id, runId, letterState, letterDirection }: Props) => {
    const request = useGraphqlRequest()
    const { t } = useTranslation('admin')
    const fetchPage = async ({ pageParam: offset = 0 }) => {
        const res = await request<AdminLettersQuery, AdminLettersQueryVariables>(adminLettersQuery, {
            runId,
            filter: {
                character1Id,
                character2Id,
                letterState,
                letterDirection,
                limit: PAGE_SIZE,
                offset,
            },
        })

        return res.admin.allLetters
    }
    const { isLoading, isFetching, data, fetchNextPage, hasNextPage } = useInfiniteQuery(
        [QueryKeys.adminAllLetters, runId, character1Id, character2Id, letterState, letterDirection],
        fetchPage,
        {
            getNextPageParam: (lastPage, allPages) => {
                if (lastPage.length < PAGE_SIZE) {
                    // No next page
                    return undefined
                }
                return allPages.length * PAGE_SIZE
            },
        },
    )

    if (isLoading) {
        return <CircularProgress />
    }

    const p = data?.pages || []
    if (p.length === 0 || p[0].length === 0) {
        const isFilterOn = Boolean(character1Id) || Boolean(character2Id) || letterState !== LetterState.All
        return <Alert severity="info">{t(isFilterOn ? 'Letters.noLettersFilter' : 'Letters.noLetters')}</Alert>
    }

    return (
        <TableContainer component={Paper} sx={{ width: 'auto', textAlign: 'center' }}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{t('Letter.sender')}</TableCell>
                        <TableCell>{t('Letter.recipient')}</TableCell>
                        <TableCell>{t('Letter.serialNumberShort')}</TableCell>
                        <TableCell>{t('Letter.sentDateShort')}</TableCell>
                        <TableCell>{t('Letter.receivedDateShort')}</TableCell>
                        <TableCell>{t('Letter.isEndOfLine')}</TableCell>
                        <TableCell />
                    </TableRow>
                </TableHead>
                <AdminLettersTableBody letters={data?.pages || []} runId={runId} />
            </Table>
            {hasNextPage ? (
                <Button
                    variant="contained"
                    onClick={() => fetchNextPage()}
                    disabled={isFetching}
                    sx={{ margin: '10px auto' }}
                >
                    {isFetching ? <CircularProgress size={10} sx={{ marginRight: '10px' }} /> : null}
                    {t('Letters.nextPage')}
                </Button>
            ) : null}
        </TableContainer>
    )
}

export default AdminLettersList
