import React, { useState } from 'react'
import {
    AdminDeleteLetterMutation,
    AdminDeleteLetterMutationVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import { IconButton, TableBody, TableCell, TableRow, Tooltip } from '@mui/material'
import { formatDate } from '../../../utils/dateUtils'
import {
    CancelScheduleSend as CancelScheduleSendIcon,
    Delete as DeleteIcon,
    Edit as EditIcon,
} from '@mui/icons-material'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { useTranslation } from 'next-i18next'
import { useShowConfirmDialog } from '../../../context/ConfirmDialogContext'
import { useMutation, useQueryClient } from 'react-query'
import { QueryKeys } from '../../../utils/queryKeys'
import AdminLetterFormDialog, { LetterToEdit } from '../AdminLetterFormDialog/AdminLetterFormDialog'
import DownloadPagesPopupMenu from '../../common/DownloadPagesPopupMenu/DownloadPagesPopupMenu'
import { useAdminGetDownloadTokens } from '../../../hooks/useAdminGetDownloadTokens'
import LetterSenderCell from '../LetterSenderCell/LetterSenderCell'
import { useFeedback } from '../../../hooks/useFeedback'
import { useGraphqlRequest } from 'src/client/hooks/useGraphqlRequest'

interface Props {
    readonly runId: string
    readonly letters: LetterToEdit[][]
}

const adminDeleteLetterMutation = require('./adminDeleteLetterMutation.graphql')

const AdminLettersTableBody = ({ runId, letters }: Props) => {
    const characters = useCharactersAdmin()
    const { t } = useTranslation('admin')
    const [editingLetter, setEditingLetter] = useState<LetterToEdit>()
    const showConfirmDialog = useShowConfirmDialog()
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()
    const feedback = useFeedback()
    const { mutate: mutateDelete } = useMutation(async (letterId: string) => {
        await request<AdminDeleteLetterMutation, AdminDeleteLetterMutationVariables>(adminDeleteLetterMutation, {
            runId,
            letterId,
        })
        await queryClient.invalidateQueries(QueryKeys.adminDashboard)
        await queryClient.invalidateQueries(QueryKeys.adminAllLetters)
    })
    const adminGetTokens = useAdminGetDownloadTokens()

    const deleteLetter = async (letterId: string) => {
        const result = await showConfirmDialog({
            text: t('Letters.confirmDelete'),
            confirmText: t('Letters.confirmDeleteButton'),
        })

        if (result === 'confirm') {
            mutateDelete(letterId)
        }
    }

    const handleDialogClose = (message?: string) => {
        feedback(message || '')
        setEditingLetter(undefined)
    }

    return (
        <TableBody>
            {letters.map((group, i) => (
                <React.Fragment key={i}>
                    {group.map(letter => {
                        const sx = letter.orgComment ? { border: 'none' } : undefined
                        return (
                            <React.Fragment key={letter.id}>
                                <TableRow>
                                    <LetterSenderCell sx={sx} letter={letter} characters={characters.map} />
                                    <TableCell sx={sx}>{characters.map[letter.recipientId || '']?.name}</TableCell>
                                    <TableCell sx={sx}>{letter.serialNumber}</TableCell>
                                    <TableCell sx={sx}>{formatDate(letter.sentDate)}</TableCell>
                                    <TableCell sx={sx}>{formatDate(letter.receivedDate)}</TableCell>
                                    <TableCell sx={sx}>
                                        {letter.isEndOfLine ? (
                                            <Tooltip title={t('Letter.isEndOfLineTooltip') as string}>
                                                <CancelScheduleSendIcon />
                                            </Tooltip>
                                        ) : null}
                                    </TableCell>
                                    <TableCell sx={{ ...sx, textAlign: 'right' }}>
                                        {letter.numberOfFiles > 0 ? (
                                            <DownloadPagesPopupMenu
                                                onGetTokens={() => adminGetTokens(runId, letter.id)}
                                            />
                                        ) : null}
                                        <IconButton onClick={() => deleteLetter(letter.id)}>
                                            <DeleteIcon />
                                        </IconButton>
                                        <IconButton onClick={() => setEditingLetter(letter)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                {letter.orgComment ? (
                                    <TableRow>
                                        <TableCell
                                            colSpan={7}
                                            sx={{
                                                paddingTop: 0,
                                                opacity: 0.6,
                                                maxWidth: '90vw',
                                                overflow: 'hidden',
                                                textOverflow: 'ellipsis',
                                            }}
                                        >
                                            {letter.orgComment}
                                        </TableCell>
                                    </TableRow>
                                ) : null}
                            </React.Fragment>
                        )
                    })}
                </React.Fragment>
            ))}
            {editingLetter ? (
                <AdminLetterFormDialog runId={runId} editLetter={editingLetter} onClose={handleDialogClose} />
            ) : null}
        </TableBody>
    )
}

export default AdminLettersTableBody
