import React, { useState } from 'react'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import AdminRunSelect from '../AdminRunSelect/AdminRunSelect'
import { CharacterAC } from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { Form } from 'react-final-form'
import AdminLettersList from './AdminLettersList'
import AdminLettersFilterPaper from './AdminLettersFilterPaper'
import { Button } from '@mui/material'
import { useTranslation } from 'next-i18next'
import { AddCircle as AddCircleIcon } from '@mui/icons-material'
import AdminLetterFormDialog from '../AdminLetterFormDialog/AdminLetterFormDialog'
import { useFeedback } from 'src/client/hooks/useFeedback'
import { LetterDirection, LetterState } from 'src/graphql/__generated__/typescript-operations'

interface FormShape {
    readonly character1?: CharacterAC
    readonly character2?: CharacterAC
    readonly letterDirection: LetterDirection
    readonly letterState: LetterState
}

const initialValues: FormShape = {
    letterState: LetterState.All,
    letterDirection: LetterDirection.Both,
}

const AdminLettersContent = () => {
    const [runId, setRunId] = useState('')
    const { t } = useTranslation('admin')
    const [creatingLetter, setCreatingLetter] = useState(false)
    const feedback = useFeedback()

    const handleDialogClose = (message?: string) => {
        feedback(message || '')
        setCreatingLetter(false)
    }

    const handleStartCreatingLetter = () => {
        setCreatingLetter(true)
    }

    return (
        <PageContentBox>
            <AdminRunSelect runId={runId} setRunId={setRunId} />

            <Form<FormShape>
                onSubmit={() => {}}
                initialValues={initialValues}
                render={({ values }) => (
                    <>
                        <Button
                            variant="contained"
                            sx={{ margin: '20px 0' }}
                            onClick={handleStartCreatingLetter}
                            startIcon={<AddCircleIcon />}
                        >
                            {t('Letters.createLetter')}
                        </Button>
                        <AdminLettersFilterPaper />
                        <AdminLettersList
                            runId={runId}
                            character1Id={values.character1?.id}
                            character2Id={values.character2?.id}
                            letterState={values.letterState}
                            letterDirection={values.letterDirection}
                        />
                    </>
                )}
            />
            {creatingLetter ? <AdminLetterFormDialog runId={runId} onClose={handleDialogClose} /> : null}
        </PageContentBox>
    )
}

export default AdminLettersContent
