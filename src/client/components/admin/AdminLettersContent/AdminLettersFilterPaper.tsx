import React from 'react'
import { useCharactersAdmin } from '../../../hooks/useCharactersAdmin'
import { Grid, MenuItem, Paper } from '@mui/material'
import FormFieldCharacterAutocomplete from '../../common/FormFieldCharacterAutocomplete/FormFieldCharacterAutocomplete'
import { useTranslation } from 'next-i18next'
import FormFieldSelect from '../../common/FormFieldSelect/FormFieldSelect'
import { LetterDirection, LetterState } from 'src/graphql/__generated__/typescript-operations'

const AdminLettersFilterPaper = () => {
    const characters = useCharactersAdmin()
    const { t } = useTranslation('admin')

    return (
        <Paper sx={{ padding: '10px', marginBottom: '10px', minWidth: '600px' }}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <FormFieldCharacterAutocomplete
                        fullWidth
                        characters={characters.list}
                        name="character1"
                        label={t('Letters.character1')}
                        sx={{ flex: 1 }}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormFieldCharacterAutocomplete
                        fullWidth
                        characters={characters.list}
                        name="character2"
                        label={t('Letters.character2')}
                        sx={{ flex: 1 }}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormFieldSelect<string>
                        fullWidth
                        name="letterDirection"
                        label={t('Letters.letterDirection.label')}
                    >
                        <MenuItem value={LetterDirection.Both}>{t('Letters.letterDirection.both')}</MenuItem>
                        <MenuItem value={LetterDirection.C1C2}>{t('Letters.letterDirection.c1c2')}</MenuItem>
                        <MenuItem value={LetterDirection.C2C1}>{t('Letters.letterDirection.c2c1')}</MenuItem>
                    </FormFieldSelect>
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormFieldSelect<string> fullWidth name="letterState" label={t('Letters.letterState.label')}>
                        <MenuItem value={LetterState.All}>{t('Letters.letterState.all')}</MenuItem>
                        <MenuItem value={LetterState.Unconfirmed}>{t('Letters.letterState.undelivered')}</MenuItem>
                        <MenuItem value={LetterState.Unanswered}>{t('Letters.letterState.unanswered')}</MenuItem>
                    </FormFieldSelect>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default AdminLettersFilterPaper
