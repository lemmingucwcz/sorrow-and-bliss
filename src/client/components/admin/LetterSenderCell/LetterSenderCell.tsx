import React from 'react'
import { Character, Letter } from '../../../../graphql/__generated__/typescript-operations'
import { TableCell, Theme } from '@mui/material'
import { SxProps } from '@mui/system'

interface Props {
    readonly letter: Pick<Letter, 'creatorId' | 'senderId'>
    readonly characters: Record<string, Character>
    readonly sx?: SxProps<Theme>
}

const LetterSenderCell = ({ letter, characters, sx }: Props) => (
    <TableCell sx={{ opacity: letter.senderId ? 1 : 0.4, ...sx }}>{characters[letter.creatorId || '']?.name}</TableCell>
)

export default LetterSenderCell
