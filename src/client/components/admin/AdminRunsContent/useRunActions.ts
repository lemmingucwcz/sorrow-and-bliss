import { QueryKeys } from 'src/client/utils/queryKeys'
import { useConfirmedAction } from '../../../hooks/useConfirmedAction'
import { useTranslation } from 'next-i18next'
import {
    ActivateRunMutation,
    ActivateRunMutationVariables,
    DeactivateRunMutation,
    DeactivateRunMutationVariables,
    DeleteRunMutation,
    DeleteRunMutationVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

const activateRunMutation = require('./graphql/activateRunMutation.graphql')
const deactivateRunMutation = require('./graphql/deactivateRunMutation.graphql')
const deleteRunMutation = require('./graphql/deleteRunMutation.graphql')

const invalidateKeys = [QueryKeys.adminRuns]

export const useRunActions = () => {
    const request = useGraphqlRequest()
    const { t } = useTranslation('admin')

    const activateRun = useConfirmedAction({
        question: t('Runs.activateRunConfirm'),
        message: t('Runs.activateRunSuccess'),
        callServer: (id: string) =>
            request<ActivateRunMutation, ActivateRunMutationVariables>(activateRunMutation, { id }).then(
                res => res.admin.activateRun,
            ),
        invalidateKeys,
    })

    const deactivateRun = useConfirmedAction({
        question: t('Runs.deactivateRunConfirm'),
        message: t('Runs.deactivateRunSuccess'),
        callServer: (id: string) =>
            request<DeactivateRunMutation, DeactivateRunMutationVariables>(deactivateRunMutation, { id }).then(
                res => res.admin.deactivateRun,
            ),
        invalidateKeys,
    })

    const deleteRun = useConfirmedAction({
        question: t('Runs.deleteRunConfirm'),
        message: t('Runs.deleteRunSuccess'),
        callServer: (id: string) =>
            request<DeleteRunMutation, DeleteRunMutationVariables>(deleteRunMutation, { id }).then(
                res => res.admin.deleteRun,
            ),
        invalidateKeys,
    })

    return { activateRun, deactivateRun, deleteRun }
}
