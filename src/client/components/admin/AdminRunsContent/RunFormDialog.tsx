import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material'
import React from 'react'
import { Form } from 'react-final-form'
import FormFieldTextInput from '../../common/FormFieldTextInput/FormFieldTextInput'
import { useTranslation } from 'next-i18next'
import { fieldValidator, validateRequired } from '../../../../utils/validationUtils'
import { useMutation, useQueryClient } from 'react-query'
import {
    CreateRunMutation,
    CreateRunMutationVariables,
    GameRun,
    UpdateRunMutation,
    UpdateRunMutationVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import { QueryKeys } from '../../../utils/queryKeys'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

interface Props {
    readonly run?: GameRun
    readonly onClose: (message?: string) => void
}

interface FormShape {
    readonly name: string
}

const createRunMutation = require('./graphql/createRunMutation.graphql')
const updateRunMutation = require('./graphql/updateRunMutation.graphql')

const RunFormDialog = ({ run, onClose }: Props) => {
    const initialValues: FormShape = run ? { name: run.name } : { name: '' }
    const { t } = useTranslation('admin')
    const { t: ct } = useTranslation('common')
    const request = useGraphqlRequest()
    const queryClient = useQueryClient()

    const { isLoading: isRunningC, mutate: mutateC } = useMutation(async (name: string) => {
        const res = await request<CreateRunMutation, CreateRunMutationVariables>(createRunMutation, {
            name,
        })
        if (res.admin.createRun) {
            await queryClient.invalidateQueries(QueryKeys.adminRuns)
            onClose(t('RunFormDialog.doneNew'))
        }
    })

    const { isLoading: isRunningU, mutate: mutateU } = useMutation(
        async ({ id, name }: { id: string; name: string }) => {
            const res = await request<UpdateRunMutation, UpdateRunMutationVariables>(updateRunMutation, {
                id,
                name,
            })
            if (res.admin.updateRun) {
                await queryClient.invalidateQueries(QueryKeys.adminRuns)
                onClose(t('RunFormDialog.doneEdit'))
            }
        },
    )

    const handleFormSubmit = ({ name }: FormShape) => {
        if (run) {
            mutateU({ id: run.id, name })
        } else {
            mutateC(name)
        }
    }

    const isRunning = isRunningC || isRunningU

    return (
        <Form onSubmit={handleFormSubmit} initialValues={initialValues}>
            {({ handleSubmit }) => (
                <Dialog open>
                    <DialogTitle>{t(run ? 'RunFormDialog.titleEdit' : 'RunFormDialog.titleNew')}</DialogTitle>
                    <DialogContent>
                        <FormFieldTextInput
                            name="name"
                            label={t('RunFormDialog.name')}
                            validate={fieldValidator(ct, validateRequired)}
                            sx={{ marginTop: '5px' }}
                            autoFocus
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => onClose()} disabled={isRunning}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={handleSubmit} variant="contained" disabled={isRunning}>
                            {t(run ? 'RunFormDialog.submitEdit' : 'RunFormDialog.submitNew')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </Form>
    )
}

export default RunFormDialog
