import {
    Button,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
} from '@mui/material'
import React, { useState } from 'react'
import { useAdminRuns } from '../../../hooks/useAdminRuns'
import PageContentBox from '../../common/PageContentBox/PageContentBox'
import { useTranslation } from 'next-i18next'
import { GameRun } from '../../../../graphql/__generated__/typescript-operations'
import RunPasswordsDialog from './RunPasswordsDialog'
import {
    RunCircle as RunCircleIcon,
    VpnKey as VpnKeyIcon,
    Edit as EditIcon,
    Delete as DeleteIcon,
    PlayCircleOutline as PlayCircleOutlineIcon,
    StopCircleOutlined as StopCircleOutlinedIcon,
    AddCircle as AddCircleIcon,
} from '@mui/icons-material'
import { useFeedback } from '../../../hooks/useFeedback'
import RunFormDialog from './RunFormDialog'
import { useRunActions } from './useRunActions'

const AdminRunsContent = () => {
    const [passwordsRun, setPasswordsRun] = useState<GameRun>()
    const [editRun, setEditRun] = useState<GameRun>()
    const [creatingNew, setCreatingNew] = useState(false)
    const feedback = useFeedback()
    const runs = useAdminRuns()
    const { t } = useTranslation('admin')
    const runActions = useRunActions()

    const handleClose = (message?: string) => {
        setPasswordsRun(undefined)
        setEditRun(undefined)
        setCreatingNew(false)
        if (message) {
            feedback(message)
        }
    }

    return (
        <PageContentBox>
            <Button
                onClick={() => setCreatingNew(true)}
                startIcon={<AddCircleIcon />}
                variant="contained"
                sx={{ marginBottom: '10px' }}
            >
                {t('Runs.createNew')}
            </Button>
            <TableContainer component={Paper} sx={{ width: 'auto' }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Runs.name')}</TableCell>
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {runs.map(run => (
                            <TableRow key={run.id}>
                                <TableCell>
                                    <Typography
                                        sx={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            gap: '10px',
                                            opacity: run.isActive ? 1 : 0.4,
                                        }}
                                    >
                                        {run.name}
                                        {run.isActive ? (
                                            <Tooltip title={t('Runs.isActive') as string}>
                                                <RunCircleIcon />
                                            </Tooltip>
                                        ) : null}
                                    </Typography>
                                </TableCell>
                                <TableCell>
                                    <IconButton onClick={() => setEditRun(run)}>
                                        <EditIcon />
                                    </IconButton>
                                    {run.isActive ? (
                                        <IconButton onClick={() => runActions.deactivateRun(run.id)}>
                                            <StopCircleOutlinedIcon />
                                        </IconButton>
                                    ) : (
                                        <IconButton onClick={() => runActions.activateRun(run.id)}>
                                            <PlayCircleOutlineIcon />
                                        </IconButton>
                                    )}
                                    <IconButton onClick={() => setPasswordsRun(run)}>
                                        <VpnKeyIcon />
                                    </IconButton>
                                    <IconButton onClick={() => runActions.deleteRun(run.id)}>
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            {passwordsRun ? <RunPasswordsDialog run={passwordsRun} onClose={() => handleClose()} /> : null}
            {editRun ? <RunFormDialog onClose={handleClose} run={editRun} /> : null}
            {creatingNew ? <RunFormDialog onClose={handleClose} /> : null}
        </PageContentBox>
    )
}

export default AdminRunsContent
