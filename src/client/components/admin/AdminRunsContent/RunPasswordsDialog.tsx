import React from 'react'
import { useQuery } from 'react-query'
import {
    GameRun,
    RunPasswordsQuery,
    RunPasswordsQueryVariables,
} from '../../../../graphql/__generated__/typescript-operations'
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@mui/material'
import { useTranslation } from 'next-i18next'
import { useGraphqlRequest } from '../../../hooks/useGraphqlRequest'

interface Props {
    readonly run: GameRun
    readonly onClose: () => void
}

const runPasswordsQuery = require('./graphql/runPasswordsQuery.graphql')

const RunPasswordsDialog = ({ run, onClose }: Props) => {
    const request = useGraphqlRequest()
    const { data } = useQuery(['runPasswords', run.id], () =>
        request<RunPasswordsQuery, RunPasswordsQueryVariables>(runPasswordsQuery, {
            id: run.id,
        }),
    )
    const { t } = useTranslation('admin')

    return (
        <Dialog open={true}>
            <DialogTitle>{t('RunPasswords.title', { runName: run.name })}</DialogTitle>
            <DialogContent>
                {data ? (
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>{t('RunPasswords.character')}</TableCell>
                                <TableCell>{t('RunPasswords.password')}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.admin.runPasswords.map(row => (
                                <TableRow key={row.id}>
                                    <TableCell>{row.name}</TableCell>
                                    <TableCell>{row.password}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                ) : null}
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} variant="contained">
                    {t('RunPasswords.close')}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default RunPasswordsDialog
