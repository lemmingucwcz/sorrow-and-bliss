export interface PlayerCredentials {
    readonly type: 'player'
    readonly token: string
    readonly runName: string
    readonly characterName: string
}

export interface AdminCredentials {
    readonly type: 'admin'
    readonly token: string
    readonly email: string
}

// interface DecodedToken {
//     readonly exp: number
// }

export type Credentials = PlayerCredentials | AdminCredentials

const playerBase = 'PLAYER'
const playerToken = `${playerBase}_TOKEN`
const playerExpiration = `${playerBase}_EXPIRATION`
const playerRun = `${playerBase}_RUN`
const playerCharacter = `${playerBase}_CHARACTER`

const adminBase = 'ADMIN'
const adminToken = `${adminBase}_TOKEN`
const adminExpiration = `${adminBase}_EXPIRATION`
const adminEmail = `${adminBase}_EMAIL`

export const dummyPlayerCredentials: PlayerCredentials = {
    type: 'player',
    token: '',
    characterName: '',
    runName: '',
}

export const dummyAdminCredentials: AdminCredentials = {
    type: 'admin',
    token: '',
    email: '',
}

export const CredentialsService = {
    storePlayerCredentials: (credentials: PlayerCredentials, expiration: number) => {
        localStorage.setItem(playerToken, credentials.token)
        localStorage.setItem(playerExpiration, expiration.toString())
        localStorage.setItem(playerRun, credentials.runName)
        localStorage.setItem(playerCharacter, credentials.characterName)
    },

    storeAdminCredentials: (credentials: AdminCredentials, expiration: number) => {
        localStorage.setItem(adminToken, credentials.token)
        localStorage.setItem(adminExpiration, expiration.toString())
        localStorage.setItem(adminEmail, credentials.email)
    },

    logoutPlayer: (clearInfo?: boolean) => {
        if (clearInfo) {
            localStorage.removeItem(playerToken)
            localStorage.removeItem(playerExpiration)
            localStorage.removeItem(playerRun)
            localStorage.removeItem(playerCharacter)
        } else {
            localStorage.setItem(playerExpiration, '0')
        }
    },

    logoutAdmin: () => {
        localStorage.setItem(adminExpiration, '0')
    },

    getPlayerCredentials: (): PlayerCredentials | undefined => {
        if (!global.localStorage) {
            // On server
            return undefined
        }

        // Considered token expired when it has less than one day
        const limitExp = new Date().getTime() / 1000 + 86400

        const pToken = localStorage.getItem(playerToken)
        if (pToken) {
            const expiration = parseInt(localStorage.getItem(playerExpiration) || '0')
            if (expiration < limitExp) {
                // Expired player token
                return dummyPlayerCredentials
            }

            // Valid player token
            return {
                type: 'player',
                token: pToken,
                characterName: localStorage.getItem(playerCharacter) || '',
                runName: localStorage.getItem(playerRun) || '',
            }
        }
    },

    getAdminCredentials: (): AdminCredentials | undefined => {
        if (!global.localStorage) {
            // On server
            return undefined
        }

        // Considered token expired when it has less than one day
        const limitExp = new Date().getTime() / 1000 + 86400

        const aToken = localStorage.getItem(adminToken)
        if (aToken) {
            const expiration = parseInt(localStorage.getItem(adminExpiration) || '0')
            if (expiration < limitExp) {
                // Expired admin token
                return dummyAdminCredentials
            }

            // Valid admin token
            return { type: 'admin', token: aToken, email: localStorage.getItem(adminEmail) || '' }
        }
    },
}
