import React, { useContext } from 'react'
import { dummyPlayerCredentials, PlayerCredentials } from '../services/CredentialsService'

export const PlayerCredentialsContext = React.createContext<PlayerCredentials>(dummyPlayerCredentials)

export const usePlayerCredentials = () => useContext(PlayerCredentialsContext)
