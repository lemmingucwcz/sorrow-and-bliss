import React, { useContext } from 'react'
import { GraphQLClient } from 'graphql-request'

const client = new GraphQLClient('/api/graphql')

export const GraphqlClientContext = React.createContext(client)

export const useGraphqlClient = () => useContext(GraphqlClientContext)
