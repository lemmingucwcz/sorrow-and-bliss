import React, { useContext } from 'react'

export type ConfirmDialogButtons = 'confirm' | 'cancel'

export interface ConfirmDialogRequest {
    readonly title?: string
    readonly text: string
    readonly confirmText?: string
    readonly cancelText?: string
}

export const ConfirmDialogContext = React.createContext<
    (request: ConfirmDialogRequest) => Promise<ConfirmDialogButtons>
>(() => Promise.resolve('cancel'))

export const useShowConfirmDialog = () => useContext(ConfirmDialogContext)
