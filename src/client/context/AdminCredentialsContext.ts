import React, { useContext } from 'react'
import { AdminCredentials, dummyAdminCredentials } from '../services/CredentialsService'

export const AdminCredentialsContext = React.createContext<AdminCredentials>(dummyAdminCredentials)

export const useAdminCredentials = () => useContext(AdminCredentialsContext)
