import React from 'react'
import { AlertColor } from '@mui/material/Alert/Alert'

type Shape = (message?: string, severity?: AlertColor) => void

export const FeedbackContext = React.createContext<Shape>(() => {})
