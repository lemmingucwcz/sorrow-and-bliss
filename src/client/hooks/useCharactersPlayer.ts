import { useQuery } from 'react-query'
import { CharactersPlayerQuery } from 'src/graphql/__generated__/typescript-operations'
import { doCharacterMapping, noCharacterData } from '../utils/characterMapUtils'
import { QueryKeys } from '../utils/queryKeys'
import { useGraphqlRequest } from './useGraphqlRequest'

const charactersPlayer = require('./graphql/charactersPlayerQuery.graphql')

export const useCharactersPlayer = () => {
    const request = useGraphqlRequest()

    const { data } = useQuery(
        QueryKeys.playerCharacters,
        () => {
            return request<CharactersPlayerQuery>(charactersPlayer).then(res => {
                return doCharacterMapping(res.player.characters)
            })
        },
        { staleTime: 60000 },
    )

    return data || noCharacterData
}
