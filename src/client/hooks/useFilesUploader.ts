import { useState } from 'react'

const initialState = [0, 0] as [number, number]

/**
 * Files uploader providing progress (how many files are already uploaded)
 */
export const useFilesUploader = () => {
    const [state, setState] = useState<[number, number]>(initialState)

    const onBeforeUpdate = (files: File[]) => {
        if (files.length > 0) {
            // We will start uploding after the update - show loader
            setState([0, files.length + 1])
        }
    }

    const onUploadFiles = async ({
        token,
        runId,
        letterId,
        files,
    }: {
        token: string
        runId: string
        letterId: string
        files: File[]
    }) => {
        for (let i = 0; i < files.length; i++) {
            setState([i + 1, files.length + 1])

            const body = new FormData()
            body.append('file', files[i])
            body.append('letterId', letterId)
            body.append('fileNo', `${i + 1}`)
            body.append('runId', runId)
            await fetch(`/api/file-upload`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                body,
            })
        }

        // Finished
        setState([1, 1])
    }

    const onFinish = () => setState(initialState)

    return {
        // Progress 0 - 100. null when upload is not in progress.
        progress: state[1] > 0 ? (100 * state[0]) / state[1] : null,
        // Call before update to show progress when file update is executed
        onBeforeUpdate,
        // Call to actualy upload files
        onUploadFiles,
        // Call when operation was finished
        onFinish,
    }
}
