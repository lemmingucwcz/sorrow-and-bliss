import { useQuery } from 'react-query'
import { QueryKeys } from '../utils/queryKeys'
import { AdminRunsQuery, GameRun } from '../../graphql/__generated__/typescript-operations'
import { useGraphqlRequest } from './useGraphqlRequest'

const adminRunsQuery = require('./graphql/adminRunsQuery.graphql')

export const useAdminRuns = () => {
    const request = useGraphqlRequest()
    const { data } = useQuery(
        QueryKeys.adminRuns,
        async () => {
            const res = await request<AdminRunsQuery>(adminRunsQuery)
            return res.admin.runs
        },
        { staleTime: 60000 },
    )

    return data || ([] as GameRun[])
}
