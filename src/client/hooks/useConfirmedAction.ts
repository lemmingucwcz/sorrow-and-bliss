import { useMutation, useQueryClient } from 'react-query'
import { useFeedback } from './useFeedback'
import { useShowConfirmDialog } from '../context/ConfirmDialogContext'

interface Args {
    readonly callServer: (id: string) => Promise<number>
    readonly question: string
    readonly message: string
    readonly invalidateKeys?: Array<string | string[]>
}

/**
 * Ask for confirmation. If user confirms, call callServer() function. When it returns thurty value,
 * confirm operation success, invalidate given keys and show message.
 *
 * @param callServer Does server call, returns 1 when operation is successful, 0 when not
 * @param question Question to ask
 * @param message Message to show on success
 * @param invalidateKeys Keys to invalidate on success
 */
export const useConfirmedAction = ({ callServer, question, message, invalidateKeys = [] }: Args) => {
    const queryClient = useQueryClient()
    const feedback = useFeedback()
    const showConfirmDialog = useShowConfirmDialog()
    const { mutate } = useMutation(async (id: string) => {
        const res = await callServer(id)
        if (res) {
            if (invalidateKeys) {
                for (let i = 0; i < invalidateKeys.length; i++) {
                    await queryClient.invalidateQueries(invalidateKeys[i])
                }
            }
            feedback(message)
        }
    })

    return async (id: string) => {
        const response = await showConfirmDialog({
            text: question,
        })
        if (response === 'confirm') {
            mutate(id)
        }
    }
}
