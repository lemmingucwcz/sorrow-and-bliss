import { useQuery } from 'react-query'
import { CharactersAdminQuery } from '../../graphql/__generated__/typescript-operations'
import { doCharacterMapping, noCharacterData } from '../utils/characterMapUtils'
import { QueryKeys } from '../utils/queryKeys'
import { useGraphqlRequest } from './useGraphqlRequest'

const charactersAdmin = require('./graphql/charactersAdminQuery.graphql')

export const useCharactersAdmin = () => {
    const request = useGraphqlRequest()

    const { data } = useQuery(
        QueryKeys.adminCharacters,
        () => {
            return request<CharactersAdminQuery>(charactersAdmin).then(res => {
                return doCharacterMapping(res.admin.characters)
            })
        },
        {
            staleTime: 60000,
        },
    )

    return data || noCharacterData
}
