import {
    PlayerDownloadTokensQuery,
    PlayerDownloadTokensQueryVariables,
} from '../../graphql/__generated__/typescript-operations'
import { useGraphqlRequest } from './useGraphqlRequest'

const playerDownloadTokensQuery = require('./graphql/playerDownloadTokensQuery.graphql')

export const usePlayerGetDownloadTokens = () => {
    const request = useGraphqlRequest()

    return async (letterId: string) => {
        const res = await request<PlayerDownloadTokensQuery, PlayerDownloadTokensQueryVariables>(
            playerDownloadTokensQuery,
            {
                letterId,
            },
        )
        return res.player.letterPagesDownloadTokens || []
    }
}
