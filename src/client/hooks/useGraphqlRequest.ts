import { useGraphqlClient } from '../context/GraphqlClientContext'
import { useFeedback } from './useFeedback'
import { useTranslation } from 'next-i18next'
import { RequestDocument, Variables } from 'graphql-request/dist/types'

export const useGraphqlRequest = () => {
    const graphqlClient = useGraphqlClient()
    const feedback = useFeedback()
    const { t } = useTranslation('common')

    return async <T, V = Variables>(document: RequestDocument, variables?: V) => {
        try {
            return await graphqlClient.request<T, V>(document, variables)
        } catch (e) {
            feedback(t('communicationError'), 'error')
            throw e
        }
    }
}
