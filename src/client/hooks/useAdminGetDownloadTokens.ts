import {
    AdminDownloadTokensQuery,
    AdminDownloadTokensQueryVariables,
} from '../../graphql/__generated__/typescript-operations'
import { useGraphqlRequest } from './useGraphqlRequest'

const adminDownloadTokensQuery = require('./graphql/adminDownloadTokensQuery.graphql')

export const useAdminGetDownloadTokens = () => {
    const request = useGraphqlRequest()

    return async (runId: string, letterId: string) => {
        const res = await request<AdminDownloadTokensQuery, AdminDownloadTokensQueryVariables>(
            adminDownloadTokensQuery,
            {
                runId,
                letterId,
            },
        )
        return res.admin.letterPagesDownloadTokens || []
    }
}
