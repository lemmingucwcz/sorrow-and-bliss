import { Character } from '../../graphql/__generated__/typescript-operations'

export type CharacterGQL = Pick<Character, 'id' | 'name' | 'isNPC'>

export type ResMap = Record<string, CharacterGQL>

export const noCharacterData = {
    map: {} as ResMap,
    list: [] as CharacterGQL[],
}

export const doCharacterMapping = (list: CharacterGQL[]) => {
    const map = list.reduce((acc, c) => {
        acc[c.id] = c
        return acc
    }, {} as ResMap)

    return {
        map,
        list,
    }
}
