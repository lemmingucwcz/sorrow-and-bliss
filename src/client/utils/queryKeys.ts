export const QueryKeys = {
    playerInfoQuery: 'playerInfoQuery',
    playerSentLettersQuery: 'playerSentLettersQuery',
    playerCharacters: 'playerCharacters',
    playerNextSerial: 'playerNextSerial',
    adminRuns: 'adminRuns',
    adminDashboard: 'adminDashboard',
    adminAllLetters: 'adminAllLetters',
    adminCharacters: 'adminCharacters',
    adminNextSerial: 'adminNextSerial',
    adminAccounts: 'adminAccounts',
}
