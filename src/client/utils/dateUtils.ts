import getConfig from 'next/config'

const dateRe = /^([0-9]+)-([0-9]+)-([0-9]+)$/

export const formatDate = (date: string | null | undefined) => {
    const d = date ? dateRe.exec(date) : undefined

    return d ? `${d[3]}.${d[2]}.` : '---'
}

export const letterAge = (receivedDate?: string | null | undefined) =>
    Math.floor((new Date().getTime() - new Date(receivedDate || '').getTime()) / 86400000)

const { publicRuntimeConfig } = getConfig()
const pendingError = parseInt(publicRuntimeConfig.LETTER_AGE_PENDING_ERROR)
const pendingWarn = parseInt(publicRuntimeConfig.LETTER_AGE_PENDING_WARN)

export const getAgeColor = (age: number): 'error' | 'warning' | 'info' => {
    if (age > pendingError) {
        return 'error'
    }
    if (age > pendingWarn) {
        return 'warning'
    }
    return 'info'
}
